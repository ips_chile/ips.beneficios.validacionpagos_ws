﻿using System;
using System.Collections.Generic;
using System.Linq;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Helpers = IPS.Beneficios.CommonRoles;
using Models = IPS.Beneficios.CommonRoles.Models;

namespace IPS.Beneficios.CommonRoles.DataAccess
{
    public class LOGIN_USUARIO
    {
        public static List<Models.LOGIN_USUARIO> Get(int APLICACION_ID)
        {
            var login_usuarioList = new List<Models.LOGIN_USUARIO>();

            using (OracleConnection connection = new OracleConnection(Helpers.Config.GetConnectionString()))
            {
                OracleCommand command = new OracleCommand(string.Format(
                    Properties.Resources.ResourceManager.GetString("LOGIN_USUARIO_GetBy_APLICACION_ID")
                    , APLICACION_ID)
                    , connection);


                OracleDataReader readerOra = null;

                try
                {
                    connection.Open();

                    readerOra = command.ExecuteReader();

                    while (readerOra.Read())
                    {
                        login_usuarioList.Add(GetObjectFromDataReader(readerOra));
                    }
                }
                finally
                {
                    readerOra.Close();
                }
            }

            return

                login_usuarioList;
        }


        private static Models.LOGIN_USUARIO GetObjectFromDataReader(OracleDataReader reader)
        {
            var objectToReturn = default(Models.LOGIN_USUARIO);

            try
            {
                objectToReturn = new Models.LOGIN_USUARIO();
                objectToReturn.LOGIN_USUARIO_ID = reader["LOGIN_USUARIO_ID"] is DBNull ? default(int) : Convert.ToInt32(reader["LOGIN_USUARIO_ID"]);
                objectToReturn.LOGIN_USUARIO_PASSWORD = reader["LOGIN_USUARIO_PASSWORD"] is DBNull ? null : (string)reader["LOGIN_USUARIO_PASSWORD"];
                objectToReturn.LOGIN_USUARIO_NAME = reader["LOGIN_USUARIO_NAME"] is DBNull ? null : (string)reader["LOGIN_USUARIO_NAME"];
                objectToReturn.LOGIN_USUARIO_CHANGE_PASSWORD = reader["LOGIN_USUARIO_CHANGE_PASSWORD"] is DBNull ? default(char) : Convert.ToChar(reader["LOGIN_USUARIO_CHANGE_PASSWORD"]);
                objectToReturn.LOGIN_USUARIO_DESCRIPCION = reader["LOGIN_USUARIO_DESCRIPCION"] is DBNull ? null : (string)reader["LOGIN_USUARIO_DESCRIPCION"];
                objectToReturn.LOGIN_USUARIO_ISADMIN = reader["LOGIN_USUARIO_ISADMIN"] is DBNull ? default(char) : Convert.ToChar(reader["LOGIN_USUARIO_ISADMIN"]);

            }
            catch (Exception ex)
            {
                //Library.WriteErrorLog_E(string.Format("Msg:{0}  Pila:{1}", ex.Message, ex.StackTrace));

                objectToReturn = new Models.LOGIN_USUARIO();
            }

            return
                objectToReturn;
        }
    }
}
