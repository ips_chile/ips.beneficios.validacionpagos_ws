﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Helpers = IPS.Beneficios.CommonRoles;
using Models = IPS.Beneficios.CommonRoles.Models;

namespace IPS.Beneficios.CommonRoles.DataAccess
{
    public class LOGIN_USUARIO_MAIL
    {
        public static List<Models.LOGIN_USUARIO_MAIL> Get(int ROL_ID, bool withBCC = false)
        {
            var login_usuario_mailList = new List<Models.LOGIN_USUARIO_MAIL>();

            using (OracleConnection connection = new OracleConnection(Helpers.Config.GetConnectionString()))
            {
                var query = withBCC
                    ? string.Format(Properties.Resources.ResourceManager.GetString("LOGIN_USUARIO_MAIL_GetBy_ROL_ID_BCC"), ROL_ID)
                    : string.Format(Properties.Resources.ResourceManager.GetString("LOGIN_USUARIO_MAIL_GetBy_ROL_ID"), ROL_ID);

                OracleCommand command = new OracleCommand(query, connection);

                OracleDataReader readerOra = null;

                try
                {
                    connection.Open();

                    readerOra = command.ExecuteReader();

                    while (readerOra.Read())
                    {
                        login_usuario_mailList.Add(GetObjectFromDataReader(readerOra));
                    }
                }
                finally
                {
                    readerOra.Close();
                }
            }

            return

                login_usuario_mailList;
        }

        private static Models.LOGIN_USUARIO_MAIL GetObjectFromDataReader(OracleDataReader reader)
        {
            var objectToReturn = default(Models.LOGIN_USUARIO_MAIL);

            try
            {
                objectToReturn = new Models.LOGIN_USUARIO_MAIL();

                objectToReturn.LOGIN_USUARIO_ID = reader["LOGIN_USUARIO_ID"] is DBNull ? default(int) : Convert.ToInt32(reader["LOGIN_USUARIO_ID"]);
                objectToReturn.LOGIN_USUARIO_MAIL_ID = reader["LOGIN_USUARIO_MAIL_ID"] is DBNull ? default(int) : Convert.ToInt32(reader["LOGIN_USUARIO_MAIL_ID"]);
                objectToReturn.LOGIN_USUARIO_MAIL_NAME = reader["LOGIN_USUARIO_MAIL_NAME"] is DBNull ? null : (string)reader["LOGIN_USUARIO_MAIL_NAME"];
            }
            catch (Exception ex)
            {
                //Library.WriteErrorLog_E(string.Format("Msg:{0}  Pila:{1}", ex.Message, ex.StackTrace));
                objectToReturn = new Models.LOGIN_USUARIO_MAIL();
            }

            return
                objectToReturn;
        }

    }
}
