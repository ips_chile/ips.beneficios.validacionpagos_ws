﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Helpers = IPS.Beneficios.CommonRoles;
using Models = IPS.Beneficios.CommonRoles.Models;

namespace IPS.Beneficios.CommonRoles.DataAccess
{
    public class LOGIN_ROL
    {
        public static List<Models.LOGIN_ROL> Get(int APLICACION_ID)
        {
            var login_rolList = new List<Models.LOGIN_ROL>();

            using (OracleConnection connection = new OracleConnection(Helpers.Config.GetConnectionString()))
            {
                OracleCommand command = new OracleCommand(string.Format(
                    Properties.Resources.ResourceManager.GetString("LOGIN_ROL_GetByAPLICACION_ID")
                    , APLICACION_ID)
                    , connection);


                OracleDataReader readerOra = null;

                try
                {
                    connection.Open();

                    readerOra = command.ExecuteReader();

                    while (readerOra.Read())
                    {
                        login_rolList.Add(GetObjectFromDataReader(readerOra));
                    }
                }
                finally
                {
                    readerOra.Close();
                }
            }

            return

                login_rolList;
        }

        private static Models.LOGIN_ROL GetObjectFromDataReader(OracleDataReader reader)
        {
            var objectToReturn = default(Models.LOGIN_ROL);

            try
            {
                objectToReturn = new Models.LOGIN_ROL();
                objectToReturn.LOGIN_ROL_ID = reader["LOGIN_ROL_ID"] is DBNull ? default(int) : Convert.ToInt32(reader["LOGIN_ROL_ID"]);
                objectToReturn.LOGIN_APLICACION_ID = reader["LOGIN_APLICACION_ID"] is DBNull ? default(int) : Convert.ToInt32(reader["LOGIN_APLICACION_ID"]);
                objectToReturn.LOGIN_ROL_NAME = reader["LOGIN_ROL_NAME"] is DBNull ? null : (string)reader["LOGIN_ROL_NAME"];
                objectToReturn.LOGIN_ROL_DESCRIPCION = reader["LOGIN_ROL_DESCRIPCION"] is DBNull ? null : (string)reader["LOGIN_ROL_DESCRIPCION"];
                objectToReturn.LOGIN_ROL_NIVEL_ID = reader["LOGIN_ROL_NIVEL_ID"] is DBNull ? default(int) : Convert.ToInt32(reader["LOGIN_ROL_NIVEL_ID"]);
                objectToReturn.LOGIN_ROL_IS_ADMIN = reader["LOGIN_ROL_IS_ADMIN"] is DBNull ? default(char) : Convert.ToChar(reader["LOGIN_ROL_IS_ADMIN"]);
                objectToReturn.LOGIN_ROL_IS_SUB_ROL = reader["LOGIN_ROL_IS_SUB_ROL"] is DBNull ? default(char) : Convert.ToChar(reader["LOGIN_ROL_IS_SUB_ROL"]);
                objectToReturn.LOGIN_ROL_ISACTIVE = reader["LOGIN_ROL_ISACTIVE"] is DBNull ? default(char) : Convert.ToChar(reader["LOGIN_ROL_ISACTIVE"]);

            }
            catch (Exception ex)
            {
                //Library.WriteErrorLog_E(string.Format("Msg:{0}  Pila:{1}", ex.Message, ex.StackTrace));

                objectToReturn = new Models.LOGIN_ROL();
            }

            return
                objectToReturn;
        }

    }
}
