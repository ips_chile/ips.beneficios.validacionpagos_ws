﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPS.Beneficios.CommonRoles.Models
{
    public class LOGIN_ROL_NIVEL
    {
        public int LOGIN_ROL_NIVEL_ID { get; set; }
        public int LOGIN_ROL_NIVEL_PADRE { get; set; }
        public string LOGIN_ROL_NIVEL_NAME { get; set; }
    }
}
