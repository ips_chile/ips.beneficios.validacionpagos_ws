﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPS.Beneficios.CommonRoles.Models
{
    public class LOGIN_ROL
    {
        public int LOGIN_ROL_ID { get; set; }
        public int LOGIN_APLICACION_ID { get; set; }
        public string LOGIN_ROL_NAME { get; set; }
        public string LOGIN_ROL_DESCRIPCION { get; set; }
        public int LOGIN_ROL_NIVEL_ID { get; set; }
        public char LOGIN_ROL_IS_ADMIN { get; set; }
        public char LOGIN_ROL_IS_SUB_ROL { get; set; }
        public char LOGIN_ROL_ISACTIVE { get; set; }
    }
}
