﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPS.Beneficios.CommonRoles.Models
{
    public class LOGIN_USUARIO_MAIL
    {
        public int LOGIN_USUARIO_MAIL_ID { get; set; }
        public int LOGIN_USUARIO_ID { get; set; }
        public string LOGIN_USUARIO_MAIL_NAME { get; set; }
    }
}
