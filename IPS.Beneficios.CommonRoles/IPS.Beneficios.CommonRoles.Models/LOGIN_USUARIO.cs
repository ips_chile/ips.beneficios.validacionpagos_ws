﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPS.Beneficios.CommonRoles.Models
{
    public class LOGIN_USUARIO
    {
        public int LOGIN_USUARIO_ID { get; set; }
        public string LOGIN_USUARIO_PASSWORD { get; set; }
        public string LOGIN_USUARIO_NAME { get; set; }
        public char LOGIN_USUARIO_CHANGE_PASSWORD { get; set; }
        public string LOGIN_USUARIO_DESCRIPCION { get; set; }
        public char LOGIN_USUARIO_ISADMIN { get; set; }
    }
}
