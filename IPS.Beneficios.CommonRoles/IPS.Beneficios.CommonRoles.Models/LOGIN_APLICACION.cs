﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPS.Beneficios.CommonRoles.Models
{
    public class LOGIN_APLICACION
    {
        public int LOGIN_APLICACION_ID { get; set; }
        public string LOGIN_APLICACION_NOMBRE { get; set; }
        public string LOGIN_APLICACION_DESCRIPCION { get; set; }
        public string LOGIN_APLICACION_URL { get; set; }
        public char LOGIN_APLICACION_ALL_SHARE { get; set; }
        public char LOGIN_APLICACION_PRIVATE_LOGIN { get; set; }
    }
}
