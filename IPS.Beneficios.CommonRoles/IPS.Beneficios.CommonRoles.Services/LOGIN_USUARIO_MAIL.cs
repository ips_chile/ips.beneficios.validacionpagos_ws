﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = IPS.Beneficios.CommonRoles.Models;
using DataAccess = IPS.Beneficios.CommonRoles.DataAccess;

namespace IPS.Beneficios.CommonRoles.Services
{
    public class LOGIN_USUARIO_MAIL
    {
        public List<Models.LOGIN_USUARIO_MAIL> Get(int ROL_ID)
        {
            return
                DataAccess.LOGIN_USUARIO_MAIL.Get(ROL_ID);
        }

        /// <summary>
        /// if withBCC is specified the, it will return all matches wit BCC == 'S' on database
        /// </summary>
        /// <param name="ROL_ID"></param>
        /// <param name="withBCC"></param>
        /// <returns></returns>
        public string Get_PlainText(int ROL_ID, bool withBCC = false)
        {
            var emailList = string.Empty;

            try
            {
                if (withBCC)
                    emailList = string.Join(",", new List<string>(DataAccess.LOGIN_USUARIO_MAIL.Get(ROL_ID, true).Select(o => o.LOGIN_USUARIO_MAIL_NAME)).ToArray());
                else
                    emailList = string.Join(",", new List<string>(DataAccess.LOGIN_USUARIO_MAIL.Get(ROL_ID).Select(o => o.LOGIN_USUARIO_MAIL_NAME)).ToArray());
            }
            catch
            {
                //Common.Config.IPS_Config.CaptureException(ex);
            }

            return
                emailList;
        }
    }
}
