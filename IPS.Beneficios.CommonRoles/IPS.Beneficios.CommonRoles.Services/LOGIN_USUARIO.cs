﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = IPS.Beneficios.CommonRoles.Models;
using DataAccess = IPS.Beneficios.CommonRoles.DataAccess;

namespace IPS.Beneficios.CommonRoles.Services
{
    public class LOGIN_USUARIO
    {
        public List<Models.LOGIN_USUARIO> Get(int APLICACION_ID)
        {
            return
                DataAccess.LOGIN_USUARIO.Get(APLICACION_ID);
        }
    }
}
