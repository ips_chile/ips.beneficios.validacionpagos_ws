﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = IPS.Beneficios.CommonRoles.Models;
using DataAccess = IPS.Beneficios.CommonRoles.DataAccess;

namespace IPS.Beneficios.CommonRoles.Services
{
    public class LOGIN_ROL
    {
        public List<Models.LOGIN_ROL> Get(int APLICACION_ID)
        {
            return
                DataAccess.LOGIN_ROL.Get(APLICACION_ID);
        }
    }
}
