﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPS.Beneficios.CommonRoles.Helpers
{
    public static class Config
    {
        public static string GetConnectionString()
        {
            return
                Properties.Settings.Default.ConnectionStringApp;
        }
    }
}
