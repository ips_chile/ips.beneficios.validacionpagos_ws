﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using Common = IPS.Beneficios.Common;
using System.Data.Common;
using IPS.Beneficios.Common;
using IPS.Beneficios.Common.Config;

namespace IPS.Beneficios.ValiacionPagos.Repository
{
    public class REPORT_PARAMS : Common.Repository.IPS_Repository
    {
        public REPORT_PARAMS() : base(false) { }


        public Model.REPORT_PARAMS GET(int _reportId)
        {
            //DbConnection _conn = null;

            //try
            //{
            //    var _params = new IPS.Beneficios.Common.OracleDynamicParameters();
            //    _params.Add("p_reportId", _reportId, dbType: DbType.Int16, direction: ParameterDirection.Input);
            //    _params.Add("p_Report_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

            //    using (_conn = CreateConnection(IPS_Config.ConnString.ORA_DESAB))
            //    {
            //        return
            //            _conn.Query<Model.REPORT_PARAMS>("CGUTIERREZC.PKG_REPORTS.GET_PARAM",_params,commandType:CommandType.StoredProcedure)
            //            .SingleOrDefault();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (_conn.State == ConnectionState.Open)
            //        _conn.Close();
            //}

            return
                default(Model.REPORT_PARAMS);

        }

        #region //PRIVATE

        //private static Model.REPORT_PARAMS Get_FromDataReader(OracleDataReader reader)
        //{
        //    Model.REPORT_PARAMS Parametros = null;

        //    try
        //    {
        //        Parametros = new Model.REPORT_PARAMS();

        //        Parametros.REP_ID = reader["REP_ID"] is DBNull ? default(int) : Convert.ToInt32(reader["REP_ID"]);

        //        Parametros.REP_GLOSA = reader["REP_GLOSA"] is DBNull ? null : (string)reader["REP_GLOSA"];

        //        Parametros.REP_PAUSED = string.IsNullOrEmpty(reader["REP_PAUSED"].ToString()) ? default(char) : char.Parse(reader["REP_PAUSED"].ToString());

        //        Parametros.REP_FECDESDE = string.IsNullOrEmpty(reader["REP_FECDESDE"].ToString()) ? default(DateTime) : DateTime.Parse(reader["REP_FECDESDE"].ToString());

        //        Parametros.REP_FECHASTA = string.IsNullOrEmpty(reader["REP_FECHASTA"].ToString()) ? default(DateTime) : DateTime.Parse(reader["REP_FECHASTA"].ToString());

        //        Parametros.REP_REPEAT_HRS = reader["REP_REPEAT_HRS"] is DBNull ? default(int) : Convert.ToInt32(reader["REP_REPEAT_HRS"]);

        //        Parametros.REP_CONSIDER_WEENKD = string.IsNullOrEmpty(reader["REP_CONSIDER_WEENKD"].ToString()) ? default(char) : char.Parse(reader["REP_CONSIDER_WEENKD"].ToString());

        //        Parametros.REP_DELAY_HRS = reader["REP_DELAY_HRS"] is DBNull ? default(int) : Convert.ToInt32(reader["REP_DELAY_HRS"]);

        //    }
        //    catch (Exception ex)
        //    {
        //        Parametros = new Model.REPORT_PARAMS();
        //    }

        //    return
        //        Parametros;
        //}
        #endregion
    }
}


/*
                //var eeee = this.GetData(IPS_Config.ConnString.ORA_DESAB, "CGUTIERREZC.PKG_REPORTS.GET_PARAM", _params).SingleOrDefault();

 
             //var conn = new MySql.Data.MySqlClient.MySqlConnection();//CONEXION SUSSCESS TO MYSQL
            //conn.ConnectionString = _connectionString;
            //conn.Open();


            //using (OracleConnection connection = new OracleConnection(_connectionString))
            //{
            //    OracleCommand command = new OracleCommand();
            //    OracleDataReader readerOra = null;

            //    var _REPORT_PARAMS = new Model.REPORT_PARAMS();

            //    try
            //    {
            //        command.Connection = connection;
            //        command.CommandText = "CGUTIERREZC.PKG_REPORTS.GET_PARAM";
            //        command.CommandType = CommandType.StoredProcedure;

            //        command.Parameters.Add("p_reportId", OracleDbType.Int16).Value = _reportId;
            //        command.Parameters.Add(new OracleParameter("p_Report_display", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;


            //        connection.Open();
            //        readerOra = command.ExecuteReader();


            //        _REPORT_PARAMS = DataReaderMapToList<Model.REPORT_PARAMS>(readerOra).SingleOrDefault();


            //        if (readerOra.Read())
            //        {
            //            _REPORT_PARAMS = Get_FromDataReader(readerOra);
            //        }



            //    }
            //    catch (Exception ex)
            //    {
            //        Common.Config.IPS_Config.CaptureException(ex);
            //    }
            //    finally
            //    {
            //        connection.Dispose();
            //        command.Dispose();
            //        readerOra.Close();
            //    }

            //    return
            //        _REPORT_PARAMS;
            //}
 * 
 * 
 * 
 //conn.Open();


                    //using (var query = conn.QueryMultiple("PKG_REPORTS.GET_PARAM", param: _params, commandType: CommandType.StoredProcedure ))
                    //{
                    //    var u = query.Read<Model.REPORT_PARAMS>().SingleOrDefault();
                    //}

                    //var a = conn.ExecuteReader("CGUTIERREZC.PKG_REPORTS.GET_PARAM", _params, commandType: CommandType.StoredProcedure);

                    var results = conn.Query<Model.REPORT_PARAMS>("CGUTIERREZC.PKG_REPORTS.GET_PARAM", _params, commandType: CommandType.StoredProcedure);

                   // conn.Close();
*/