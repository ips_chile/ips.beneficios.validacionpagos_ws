﻿using System;
using System.Data;
using System.Collections.Generic;
using Common = IPS.Beneficios.Common;

namespace IPS.Beneficios.ValiacionPagos.Repository
{
    public class REPORT_ACTIVITY : Common.Repository.IPS_Repository
    {
        public REPORT_ACTIVITY() : base(false) { }

        public Model.REPORT_ACTIVITY GET(int _reportId)
        {
            //using (oracleconnection connection = new oracleconnection(_connectionstring))
            //{
            //    oraclecommand command = new oraclecommand();
            //    oracledatareader readerora = null;

            //    var _report_activity = new model.report_activity();

            //    try
            //    {
            //        command.connection = connection;
            //        command.commandtext = "cgutierrezc.pkg_reports.get_last_activity";
            //        command.commandtype = commandtype.storedprocedure;

            //        command.parameters.add("p_reportid", oracledbtype.int16).value = _reportid;
            //        command.parameters.add(new oracleparameter("p_report_display", oracledbtype.refcursor)).direction = parameterdirection.output;


            //        connection.open();
            //        readerora = command.executereader();

            //        if (readerora.read())
            //            _report_activity = get_fromdatareader(readerora);

            //    }
            //    catch (exception ex)
            //    {
            //        common.config.ips_config.captureexception(ex);
            //    }
            //    finally
            //    {
            //        connection.dispose();
            //        command.dispose();
            //        readerora.close();
            //    }

            //    return
            //        _report_activity;
            //}

            return
              default(Model.REPORT_ACTIVITY);
        }

        public bool SET(Model.REPORT_ACTIVITY _REPORT_ACTIVITY)
        {
            bool returnedValue = false;
            //using (OracleConnection connection = new OracleConnection(_connectionString))
            //{
            //    OracleCommand command = new OracleCommand();

            //    try
            //    {
            //        command.Connection = connection;
            //        command.CommandText = "CGUTIERREZC.PKG_REPORTS.SET_REPORT_ACTIVITY";
            //        command.CommandType = CommandType.StoredProcedure;

            //        command.Parameters.Add("p_reportId", OracleDbType.Int16).Value = _REPORT_ACTIVITY.REP_ID;
            //        command.Parameters.Add(new OracleParameter("p_status", OracleDbType.Char, 1)).Direction = ParameterDirection.Output;

            //        connection.Open();
            //        command.ExecuteNonQuery();
            //        if (((OracleString)command.Parameters["p_status"].Value).IsNull)
            //        {
            //            returnedValue = false;
            //        }
            //        else
            //        {
            //            string returnedStr = Convert.ToString(command.Parameters["p_status"].Value);
            //            if (returnedStr.Equals("I"))
            //                returnedValue = true;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Common.Config.IPS_Config.CaptureException(ex);
            //    }
            //    finally
            //    {
            //        connection.Dispose();
            //        command.Dispose();
            //    }
                return returnedValue;
        }

        //#region //PRIVATE

        //private static Model.REPORT_ACTIVITY Get_FromDataReader(OracleDataReader reader)
        //{
        //    Model.REPORT_ACTIVITY Parametros = null;

        //    try
        //    {
        //        Parametros = new Model.REPORT_ACTIVITY();

        //        Parametros.REP_ID = reader["REP_ID"] is DBNull ? default(int) : Convert.ToInt32(reader["REP_ID"]);

        //        Parametros.FEC_NOITIFY = string.IsNullOrEmpty(reader["FEC_NOITIFY"].ToString()) ? default(DateTime) : DateTime.Parse(reader["FEC_NOITIFY"].ToString());
        //    }
        //    catch
        //    {
        //        Parametros = new Model.REPORT_ACTIVITY();
        //    }

        //    return
        //        Parametros;
        //}
        //#endregion
    }
}
