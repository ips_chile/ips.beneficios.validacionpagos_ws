﻿using System;
using Oracle.DataAccess.Client;
using System.Data;
using Oracle.DataAccess.Types;
using System.Collections.Generic;
using Common = IPS.Beneficios.Common;

namespace IPS.Beneficios.ValiacionPagos.Repository
{
    public class IPS_Worker_Report : Common.Repository.IPS_Repository
    {

        public IPS_Worker_Report() : base(Common.Config.IPS_Config.ConnString.ORA_DESAB, false) { }


        public Dictionary<string, int> ELEGIBILIDAD_GETBY_DIA(DateTime _fechaDesde)
        {
            using (OracleConnection connection = new OracleConnection(_connectionString))
            {
                OracleCommand command = new OracleCommand();
                OracleDataReader readerOra = null;

                var _IPS_Worker_Report_Dic = new Dictionary<string, int>();

                try
                {
                    command.Connection = connection;
                    command.CommandText = "CGUTIERREZC.PKG_LOG.ELEGIBILIDAD_GETBY_DIA";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("p_fecha", OracleDbType.Date).Value = _fechaDesde;
                    command.Parameters.Add(new OracleParameter("p_Elegibilidad_display", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;


                    connection.Open();
                    readerOra = command.ExecuteReader();

                    while (readerOra.Read())
                    {
                        _IPS_Worker_Report_Dic.Add(
                            readerOra["dia"] is DBNull ? null : (string)readerOra["dia"]
                            , readerOra["cant"] is DBNull ? default(int) : Convert.ToInt32(readerOra["cant"])
                            );
                    }

                }
                catch (Exception ex)
                {
                    Common.Config.IPS_Config.CaptureException(ex);
                }
                finally
                {
                    connection.Dispose();
                    command.Dispose();
                    readerOra.Close();
                }

                return
                    _IPS_Worker_Report_Dic;
            }
        }


        public Dictionary<string, int> ELEGIBILIDAD_GETBY_REGION(DateTime _fechaDesde)
        {
            using (OracleConnection connection = new OracleConnection(_connectionString))
            {
                OracleCommand command = new OracleCommand();
                OracleDataReader readerOra = null;

                var _IPS_Worker_Report_Dic = new Dictionary<string, int>();

                try
                {
                    command.Connection = connection;
                    command.CommandText = "CGUTIERREZC.PKG_LOG.ELEGIBILIDAD_GETBY_REGION";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("p_fecha", OracleDbType.Date).Value = _fechaDesde;
                    command.Parameters.Add(new OracleParameter("p_Elegibilidad_display", OracleDbType.RefCursor)).Direction = ParameterDirection.Output;


                    connection.Open();
                    readerOra = command.ExecuteReader();

                    while (readerOra.Read())
                    {
                        _IPS_Worker_Report_Dic.Add(
                            readerOra["region"] is DBNull ? null : (string)readerOra["region"]
                            , readerOra["cant"] is DBNull ? default(int) : Convert.ToInt32(readerOra["cant"])
                            );
                    }

                }
                catch (Exception ex)
                {
                    Common.Config.IPS_Config.CaptureException(ex);
                }
                finally
                {
                    connection.Dispose();
                    command.Dispose();
                    readerOra.Close();
                }

                return
                    _IPS_Worker_Report_Dic;
            }
        }





        #region //PRIVATE

        private static Model.IPS_Worker_Report Get_FromDataReader(OracleDataReader reader)
        {
            Model.IPS_Worker_Report Parametros = null;

            try
            {
                Parametros = new Model.IPS_Worker_Report();

                Parametros.RUT_CONSULTADO = reader["RUT_CONSULTADO"] is DBNull ? default(int) : Convert.ToInt32(reader["RUT_CONSULTADO"]);

                Parametros.DV_CONSULTADO = string.IsNullOrEmpty(reader["DV_CONSULTADO"].ToString()) ? default(char) : char.Parse(reader["DV_CONSULTADO"].ToString());

                Parametros.NOMBRE_PC = reader["NOMBRE_PC"] is DBNull ? null : (string)reader["NOMBRE_PC"];

                Parametros.USUARIO_PC = reader["USUARIO_PC"] is DBNull ? null : (string)reader["USUARIO_PC"];

                Parametros.ACCION_IP = reader["ACCION_IP"] is DBNull ? null : (string)reader["ACCION_IP"];

                Parametros.FECHA_CONSULTA = string.IsNullOrEmpty(reader["FECHA_CONSULTA"].ToString()) ? default(DateTime) : DateTime.Parse(reader["FECHA_CONSULTA"].ToString());

                Parametros.REGION = reader["REGION"] is DBNull ? default(int) : Convert.ToInt32(reader["REGION"]);

                Parametros.COMUNA = reader["COMUNA"] is DBNull ? null : (string)reader["COMUNA"];

                Parametros.CALIDAD = reader["CALIDAD"] is DBNull ? null : (string)reader["CALIDAD"];

                Parametros.VALORES = reader["VALORES"] is DBNull ? null : (string)reader["VALORES"];
            }
            catch (Exception ex)
            {
                Parametros = new Model.IPS_Worker_Report();
            }

            return
                Parametros;
        }
        #endregion

    }
}
