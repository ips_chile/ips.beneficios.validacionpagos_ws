﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using Common = IPS.Beneficios.Common;

namespace IPS.Beneficios.ValiacionPagos
{
    public class Program
    {
        public static ManualResetEvent[] doneEvents = new ManualResetEvent[20];

        public static Dictionary<short, string> WorkerPool = new Dictionary<short, string>();



        public static void Thread_IPS_Worker_LoadCausantesBase()
        {
            try
            {
                BackgroundWorker backgroundWorker = new System.ComponentModel.BackgroundWorker();
                short workerID = (int)IPS.Beneficios.Common.Config.IPS_Config.Workers.LoadCausantesBase;
                if (!WorkerPool.ContainsKey(workerID))
                {
                    Workers.IPS_Worker_LoadCausantesBase worker = new Workers.IPS_Worker_LoadCausantesBase();
                    if (!WorkerPool.ContainsKey(workerID))
                    {
                        WorkerPool.Add(workerID, worker.workerJobName);
                        object[] workerParams = new object[] { workerID, worker.workerJobName };
                        backgroundWorker.DoWork += new DoWorkEventHandler(worker.InitWork);
                        backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Thread_IPS_Worker_Completed);
                        backgroundWorker.RunWorkerAsync(workerParams);
                    }
                }
            }
            catch (ThreadAbortException ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }
        }

        public static void Thread_IPS_Worker_LoadCausantesMes()
        {
            try
            {
                BackgroundWorker backgroundWorker = new System.ComponentModel.BackgroundWorker();
                short workerID = (int)IPS.Beneficios.Common.Config.IPS_Config.Workers.LoadCausantesMes;
                if (!WorkerPool.ContainsKey(workerID))
                {
                    Workers.IPS_Worker_LoadCausantesMes worker = new Workers.IPS_Worker_LoadCausantesMes();
                    if (!WorkerPool.ContainsKey(workerID))
                    {
                        WorkerPool.Add(workerID, worker.workerJobName);
                        object[] workerParams = new object[] { workerID, worker.workerJobName };
                        backgroundWorker.DoWork += new DoWorkEventHandler(worker.InitWork);
                        backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Thread_IPS_Worker_Completed);
                        backgroundWorker.RunWorkerAsync(workerParams);
                    }
                }
            }
            catch (ThreadAbortException ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }
        }

        public static void Thread_IPS_Worker_LoadSolicitudesBase()
        {
            try
            {
                BackgroundWorker backgroundWorker = new System.ComponentModel.BackgroundWorker();
                short workerID = (int)IPS.Beneficios.Common.Config.IPS_Config.Workers.LoadSolicitudesBase;
                if (!WorkerPool.ContainsKey(workerID))
                {
                    Workers.IPS_Worker_LoadSolicitudesBase worker = new Workers.IPS_Worker_LoadSolicitudesBase();
                    if (!WorkerPool.ContainsKey(workerID))
                    {
                        WorkerPool.Add(workerID, worker.workerJobName);
                        object[] workerParams = new object[] { workerID, worker.workerJobName };
                        backgroundWorker.DoWork += new DoWorkEventHandler(worker.InitWork);
                        backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Thread_IPS_Worker_Completed);
                        backgroundWorker.RunWorkerAsync(workerParams);
                    }
                }
            }
            catch (ThreadAbortException ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }
        }

        public static void Thread_IPS_Worker_CalculoIngresoPromedio()
        {
            try
            {
                BackgroundWorker backgroundWorker = new System.ComponentModel.BackgroundWorker();
                short workerID = (int)IPS.Beneficios.Common.Config.IPS_Config.Workers.CalculoIngresoPromedio;
                if (!WorkerPool.ContainsKey(workerID))
                {
                    Workers.IPS_Worker_CalculoIngresoPromedio worker = new Workers.IPS_Worker_CalculoIngresoPromedio();
                    if (!WorkerPool.ContainsKey(workerID))
                    {
                        WorkerPool.Add(workerID, worker.workerJobName);
                        object[] workerParams = new object[] { workerID, worker.workerJobName };
                        backgroundWorker.DoWork += new DoWorkEventHandler(worker.InitWork);
                        backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Thread_IPS_Worker_Completed);
                        backgroundWorker.RunWorkerAsync(workerParams);
                    }
                }
            }
            catch (ThreadAbortException ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }
        }

        public static void Thread_IPS_Worker_DistribucionPagos()
        {
            try
            {
                BackgroundWorker backgroundWorker = new System.ComponentModel.BackgroundWorker();
                short workerID = (int)IPS.Beneficios.Common.Config.IPS_Config.Workers.DistribucionPagos;
                if (!WorkerPool.ContainsKey(workerID))
                {
                    Workers.IPS_Worker_DistribucionPagos worker = new Workers.IPS_Worker_DistribucionPagos();
                    if (!WorkerPool.ContainsKey(workerID))
                    {
                        WorkerPool.Add(workerID, worker.workerJobName);
                        object[] workerParams = new object[] { workerID, worker.workerJobName };
                        backgroundWorker.DoWork += new DoWorkEventHandler(worker.InitWork);
                        backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Thread_IPS_Worker_Completed);
                        backgroundWorker.RunWorkerAsync(workerParams);
                    }
                }
            }
            catch (ThreadAbortException ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }
        }

        public static void Thread_IPS_Worker_SolicitudesProcesadas()
        {
            try
            {
                BackgroundWorker backgroundWorker = new System.ComponentModel.BackgroundWorker();
                short workerID = (int)IPS.Beneficios.Common.Config.IPS_Config.Workers.SolicitudesProcesadas;
                if (!WorkerPool.ContainsKey(workerID))
                {
                    Workers.IPS_Worker_LoadSolicitudesProcesadas worker = new Workers.IPS_Worker_LoadSolicitudesProcesadas();
                    if (!WorkerPool.ContainsKey(workerID))
                    {
                        WorkerPool.Add(workerID, worker.workerJobName);
                        object[] workerParams = new object[] { workerID, worker.workerJobName };
                        backgroundWorker.DoWork += new DoWorkEventHandler(worker.InitWork);
                        backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Thread_IPS_Worker_Completed);
                        backgroundWorker.RunWorkerAsync(workerParams);
                    }
                }
            }
            catch (ThreadAbortException ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }
        }

        public static void Thread_IPS_Worker_GeneraArchivoMenoresMayores()
        {
            try
            {
                BackgroundWorker backgroundWorker = new System.ComponentModel.BackgroundWorker();
                short workerID = (int)IPS.Beneficios.Common.Config.IPS_Config.Workers.GeneraArchivoMenoresMayores;
                if (!WorkerPool.ContainsKey(workerID))
                {
                    Workers.IPS_Worker_GeneraArchivoMenoresMayores worker = new Workers.IPS_Worker_GeneraArchivoMenoresMayores();
                    if (!WorkerPool.ContainsKey(workerID))
                    {
                        WorkerPool.Add(workerID, worker.workerJobName);
                        object[] workerParams = new object[] { workerID, worker.workerJobName };
                        backgroundWorker.DoWork += new DoWorkEventHandler(worker.InitWork);
                        backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Thread_IPS_Worker_Completed);
                        backgroundWorker.RunWorkerAsync(workerParams);
                    }
                }
            }
            catch (ThreadAbortException ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }
        }

        public static void Thread_IPS_Worker_CheckPeriodosProcesos()
        {
            try
            {
                BackgroundWorker backgroundWorker = new System.ComponentModel.BackgroundWorker();
                short workerID = (int)IPS.Beneficios.Common.Config.IPS_Config.Workers.CheckPeriodosProcesos;
                if (!WorkerPool.ContainsKey(workerID))
                {
                    Workers.IPS_Worker_CheckPeriodosProcesos worker = new Workers.IPS_Worker_CheckPeriodosProcesos();
                    if (!WorkerPool.ContainsKey(workerID))
                    {
                        WorkerPool.Add(workerID, worker.workerJobName);
                        object[] workerParams = new object[] { workerID, worker.workerJobName };
                        backgroundWorker.DoWork += new DoWorkEventHandler(worker.InitWork);
                        backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Thread_IPS_Worker_Completed);
                        backgroundWorker.RunWorkerAsync(workerParams);
                    }
                }
            }
            catch (ThreadAbortException ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }
        }


        private static void Thread_IPS_Worker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            object[] parameters = e.Result as object[];
            short workerID = (short)parameters[0];
            string workerName = parameters[1].ToString();
            if (e.Error != null)
            {
                IPS.Beneficios.Common.Config.IPS_Config.CaptureException(e.Error);
            }
            else
            {
                #region //LOG REGISTRY
                Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                {
                    PROCESO_NAME = String.Format("Thread_{0}", workerName)
                    ,
                    API_MESSAGE = "Removiendo worker de la lista de Threads"
                    ,
                    TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
                });
                #endregion
                WorkerPool.Remove(workerID);
                #region //LOG REGISTRY
                Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                {
                    PROCESO_NAME = String.Format("Thread_{0}", workerName)
                    ,
                    API_MESSAGE = "Worker removido de la lista"
                    ,
                    TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
                });
                #endregion
            }
        }
    }
}