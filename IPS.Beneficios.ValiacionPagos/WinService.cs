﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Timers;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.ComponentModel;
using System.Threading;
using System.Diagnostics;
using System.Linq;
using Common = IPS.Beneficios.Common;
using System.Collections.Generic;
using log4net;

namespace IPS.Beneficios.ValiacionPagos
{
    class WinService : System.ServiceProcess.ServiceBase
    {
        private System.Timers.Timer timerWS = null;

        public static bool taskHasBeenDone = true;

        public static Dictionary<string, Thread> ThreadList = new Dictionary<string, Thread>();

        protected static readonly ILog log = LogManager.GetLogger(typeof(WinService));
        static void Main()
        {
            #if (!DEBUG)
                log.Info("Service-mode Enabled");
                System.ServiceProcess.ServiceBase.Run(new WinService());
            #else
                log.Info("Interactive-mode Enabled");
                WinService service = new WinService();
                service.OnStart(null);
                System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
            #endif

            //if (Environment.UserInteractive)
            //{
            //    log.Info("Interactive-mode Enabled");
            //    WinService service = new WinService();
            //    service.OnStart(null);
            //    System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
            //}
            //else
            //{
            //    log.Info("Service-mode Enabled");
            //    System.ServiceProcess.ServiceBase.Run(new WinService());
            //}
        }

        public WinService()
        {
            Common.Config.IPS_Config.InitializeEventViewerLog();
            Common.Config.IPS_Config.InicializaLog();
            #region //LOG REGISTRY
            Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
            {
                PROCESO_NAME = "WinService()"
                ,
                API_MESSAGE = "Log inicializado"
                ,
                TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
            });
            #endregion
        }

        protected override void OnStart(string[] args)
        {
            int interval = Properties.Settings.Default.TimeMinutesInterval;
            SetInterval(interval);

            #region //LOG REGISTRY
            Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
            {
                TIPO_OPERACION = Common.Model.EnumLog_Operation.SERVICE_STARTED
            });
            #endregion
        }

        protected override void OnStop()
        {
            timerWS.Stop();

            timerWS = null;

            #region //LOG REGISTRY
            Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
            {
                TIPO_OPERACION = Common.Model.EnumLog_Operation.SERVICE_STOPED
            });
            #endregion

        }

        private static void SetWorkerThread()
        {
            ////Call workers from here..
            IPS.Beneficios.ValiacionPagos.Program.Thread_IPS_Worker_LoadCausantesBase();// OK, test 1
            IPS.Beneficios.ValiacionPagos.Program.Thread_IPS_Worker_LoadSolicitudesBase();// OK, test 1
            IPS.Beneficios.ValiacionPagos.Program.Thread_IPS_Worker_LoadCausantesMes();// OK, test 1
            IPS.Beneficios.ValiacionPagos.Program.Thread_IPS_Worker_CalculoIngresoPromedio();// OK, test 1, OK test 2
            IPS.Beneficios.ValiacionPagos.Program.Thread_IPS_Worker_DistribucionPagos();// OK, test 1, OK test 2, Ok,test 3, Ok Test 4
            IPS.Beneficios.ValiacionPagos.Program.Thread_IPS_Worker_SolicitudesProcesadas();// OK, test 1, OK test 2, OK test 3
            IPS.Beneficios.ValiacionPagos.Program.Thread_IPS_Worker_GeneraArchivoMenoresMayores();// OK, test 1, OK test 2, OK test 3
            IPS.Beneficios.ValiacionPagos.Program.Thread_IPS_Worker_CheckPeriodosProcesos();// OK, test 1
        }

        private void SetInterval(int intervalMinutes)
        {
            #region //LOG REGISTRY
            Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
            {
                PROCESO_NAME = "SetInterval()"
                ,
                API_MESSAGE = String.Format("Intervalo de {0} min seteado", intervalMinutes.ToString())
                ,
                TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
            });
            #endregion

            timerWS = new System.Timers.Timer();
            timerWS.Interval = 1000 * 60 * intervalMinutes;
            timerWS.Elapsed += new System.Timers.ElapsedEventHandler(this.timerWS_Tick);
            timerWS.Enabled = true;
            timerWS.AutoReset = true;
            timerWS.Start();
        }

        private void timerWS_Tick(object sender, ElapsedEventArgs e)
        {
            #region //LOG REGISTRY
            Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
            {
                PROCESO_NAME = "timerWS_Tick()"
                ,
                API_MESSAGE = "El ciclo de procesos ha sido iniciado"
                ,
                TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
            });
            #endregion
            SetWorkerThread();

            #region //LOG REGISTRY
            Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
            {
                PROCESO_NAME = "timerWS_Tick()"
                ,
                API_MESSAGE = "El ciclo de procesos ha terminado"
                ,
                TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
            });
            #endregion

        }
    }

}
