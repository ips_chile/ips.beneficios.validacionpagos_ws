﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Workers
{
    public class IPS_Worker_LoadCausantesBase : Base.IPS_Worker
    {
        private Model.VAP_PERIODOS _VAP_PERIODO = null;
        private Model.VAP_PROCESOS_TASK _VAP_PROCESOS_TASK = null;

        public IPS_Worker_LoadCausantesBase()
        {
            SetWorkerJobName();
        }

        public override void InitWork(object sender = null, System.ComponentModel.DoWorkEventArgs e = null)
        {
            object[] parameters = e.Argument as object[];
            e.Result = parameters;
            bool workerCanRun = this.workerCanRun(ref _VAP_PERIODO, Common.Config.IPS_Config.ProcesoID.LOAD_CAUSANTES_BASE, "LOAD CAUSANTES BASE", ref _VAP_PROCESOS_TASK);

            if (workerCanRun)
            {
                var procesoEstado = _serviceVAP_CAUSANTES_BASE.ProccessFile(_VAP_PERIODO);

                _VAP_PROCESOS_TASK.ESTADO_ID = (int)procesoEstado;
                _VAP_PROCESOS_TASK.FECHA_FIN = DateTime.Now;

                _serviceVAP_PROCESOS_TASK.UPDATE(_VAP_PROCESOS_TASK);
            }
        }
    }
}
