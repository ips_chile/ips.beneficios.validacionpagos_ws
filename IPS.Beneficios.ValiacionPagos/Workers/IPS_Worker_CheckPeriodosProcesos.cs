﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Workers
{
    public class IPS_Worker_CheckPeriodosProcesos : Base.IPS_Worker
    {
        //private Model.VAP_PERIODOS _VAP_PERIODO = null;
        //private Model.VAP_PROCESOS_TASK _VAP_PROCESOS_TASK = null;

        public IPS_Worker_CheckPeriodosProcesos()
        {
            SetWorkerJobName();
        }

        public override void InitWork(object sender = null, System.ComponentModel.DoWorkEventArgs e = null)
        {
            object[] parameters = e.Argument as object[];
            e.Result = parameters;

            _serviceBLL_CHECK_PERIODOSPROCESOS.Main();

            //Service.Dummies.BLL_DISTRIBUCION_PAGOS _BLL_DISTRIBUCION_PAGOS = new Service.Dummies.BLL_DISTRIBUCION_PAGOS(_VAP_PERIODO);

            //var wasSuccess = _BLL_DISTRIBUCION_PAGOS.Main();

            //_VAP_PROCESOS_TASK.ESTADO_ID = (wasSuccess
            //    ? (int)Common.Config.IPS_Config.ProcesoEstado.TERMINADO_SUCCESS
            //    : (int)Common.Config.IPS_Config.ProcesoEstado.TERMINADO_ERROR);
            //_VAP_PROCESOS_TASK.FECHA_FIN = DateTime.Now;

            //_serviceVAP_PROCESOS_TASK.UPDATE(_VAP_PROCESOS_TASK);
        }
    }
}
