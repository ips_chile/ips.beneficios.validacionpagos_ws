﻿using IPS.Beneficios.ValiacionPagos.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Workers
{
    public class IPS_Worker_DistribucionPagos : Base.IPS_Worker
    {
        private Model.VAP_PERIODOS _VAP_PERIODO = null;
        private Model.VAP_PROCESOS_TASK _VAP_PROCESOS_TASK = null;

        public IPS_Worker_DistribucionPagos()
        {
            SetWorkerJobName();
        }

        public override void InitWork(object sender = null, System.ComponentModel.DoWorkEventArgs e = null)
        {
            object[] parameters = e.Argument as object[];
            e.Result = parameters;
            bool _workerCanRun = this.workerCanRun(ref _VAP_PERIODO, Common.Config.IPS_Config.ProcesoID.DISTRIBUCION_PAGOS, "LOAD DISTRIBUCION PAGOS", ref _VAP_PROCESOS_TASK);
                
            if (_workerCanRun)
            {
                Service.Dummies.BLL_DISTRIBUCION_PAGOS _BLL_DISTRIBUCION_PAGOS = new Service.Dummies.BLL_DISTRIBUCION_PAGOS(_VAP_PERIODO);

                var wasSuccess = _BLL_DISTRIBUCION_PAGOS.Main();

                _VAP_PROCESOS_TASK.ESTADO_ID = (wasSuccess
                    ? (int)Common.Config.IPS_Config.ProcesoEstado.TERMINADO_SUCCESS
                    : (int)Common.Config.IPS_Config.ProcesoEstado.TERMINADO_ERROR);
                _VAP_PROCESOS_TASK.FECHA_FIN = DateTime.Now;

                _serviceVAP_PROCESOS_TASK.UPDATE(_VAP_PROCESOS_TASK);
            }
        }

        //public new bool workerCanRun()
        //{
        //    var canRun = false;
        //    var errorMessage = string.Empty;

        //    var _periodoToUse = _serviceVAP_PERIODOS.GETALL().Where(o =>
        //        o.PERIODO_ESTADO == (char)Common.Config.IPS_Config.PeriodoEstado.ABIERTO
        //        || o.PERIODO_ESTADO == (char)Common.Config.IPS_Config.PeriodoEstado.EJECUTANDO).Select(o => o).FirstOrDefault();

        //    if (_periodoToUse != null)
        //    {
        //        var _procesoTask = _serviceVAP_PROCESOS_TASK
        //                            .GET((int)Common.Config.IPS_Config.ProcesoID.DISTRIBUCION_PAGOS, _periodoToUse.PERIODO_ID)
        //                            .Where(o => o.ESTADO_ID == (int)Common.Config.IPS_Config.ProcesoEstado.EN_ESPERA_PROCESAMIENTO)
        //                            .Select(o => o).FirstOrDefault();

        //        if (_procesoTask != null)
        //        {
        //            _VAP_PERIODO = _periodoToUse;

        //            if (_periodoToUse.PERIODO_ESTADO == (char)Common.Config.IPS_Config.PeriodoEstado.ABIERTO)
        //                _serviceVAP_PERIODOS.UPDATE(_VAP_PERIODO.PERIODO_ID, (char)Common.Config.IPS_Config.PeriodoEstado.EJECUTANDO);

        //            _procesoTask.ESTADO_ID = (int)Common.Config.IPS_Config.ProcesoEstado.PROCESANDO;
        //            _procesoTask.FECHA_INI = DateTime.Now;
        //            _procesoTask.FECHA_FIN = default(DateTime);

        //            var wasUpdated = _serviceVAP_PROCESOS_TASK.UPDATE(_procesoTask);

        //            if (wasUpdated)
        //                _VAP_PROCESOS_TASK = _procesoTask;

        //            canRun = true;
        //        }
        //        else
        //            errorMessage = string.Format("No existe procesoTask({0}) estado: \"EN ESPERA PROCESAMIENTO\" para ser ejecutado, periodo: {1}", "LOAD DISTRIBUCION PAGOS", _periodoToUse.PERIODO_DISPLAY);
        //    }
        //    else
        //        errorMessage = "No existe periodo valido para procesar";


        //    #region //LOG REGISTRY
        //    Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
        //    {
        //        PROCESO_NAME = "workerCanRun()"
        //        ,
        //        API_MESSAGE = String.Format("El worker: {0} {1} puede correr. {2}", workerJobName, (canRun ? "SI" : "NO"), (canRun ? string.Empty : errorMessage))
        //        ,
        //        TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
        //    });
        //    #endregion

        //    return
        //        canRun;
        //}
    }
}
