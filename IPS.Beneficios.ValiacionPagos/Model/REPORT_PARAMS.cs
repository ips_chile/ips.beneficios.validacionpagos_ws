﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class REPORT_PARAMS
    {
        public int REP_ID { get; set; }
        public string REP_GLOSA { get; set; }
        public char REP_PAUSED { get; set; }
        public DateTime REP_FECDESDE { get; set; }
        public DateTime REP_FECHASTA { get; set; }
        public int REP_REPEAT_HRS { get; set; }
        public char REP_CONSIDER_WEENKD { get; set; }
        public int REP_DELAY_HRS { get; set; }
    }
}
