﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class REPORT_ACTIVITY
    {
        public int REP_ID { get; set; }
        public DateTime FEC_NOITIFY { get; set; }
    }
}
