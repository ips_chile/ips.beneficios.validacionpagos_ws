﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class IPS_Worker_Report
    {
        public int RUT_CONSULTADO { get; set; }
        public char DV_CONSULTADO { get; set; }
        public string NOMBRE_PC { get; set; }
        public string USUARIO_PC { get; set; }
        public string ACCION_IP { get; set; }
        public DateTime FECHA_CONSULTA { get; set; }
        public int REGION { get; set; }
        public string COMUNA { get; set; }
        public string CALIDAD { get; set; }
        public string VALORES { get; set; }

    }
}

