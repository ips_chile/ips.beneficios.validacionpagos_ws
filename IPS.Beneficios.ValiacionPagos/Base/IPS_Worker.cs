﻿using System;
using System.IO;
using System.Globalization;
using System.Threading;
using Common = IPS.Beneficios.Common;
using Service = IPS.Beneficios.ValiacionPagos.Service.Dummies;

namespace IPS.Beneficios.ValiacionPagos.Base
{
    public abstract class IPS_Worker : IDisposable
    {
        public bool _threadShutdown = false;
        protected ManualResetEvent _doneEvent;
        //public short macroProcID;
        public string workerJobName;
        protected string workerName = "IPS_WORKER";
        public string workerProfileLocation = "C:\\IPS_WORKER";

        public string localFilename = null;
        public string localFilenameExtension = null;

        protected bool isFileDownloaded = false;

        protected Service.Dummies.BLL_BULKDATA _serviceVAP_BULKDATA;
        protected Service.Dummies.VAP_INGRESO_PROMEDIO _serviceBLL_CALCULO_INGRESOPROMEDIO;
        protected Service.Dummies.VAP_PERIODOS _serviceVAP_PERIODOS;
        protected Service.Dummies.VAP_CAUSANTES_BASE _serviceVAP_CAUSANTES_BASE;
        protected Service.Dummies.VAP_CAUSANTES_MES _serviceVAP_CAUSANTES_MES;
        protected Service.Dummies.VAP_SOLICITUDES_BASE _serviceVAP_SOLICITUDES_BASE;
        protected Service.Dummies.VAP_PROCESO_ESTADOS _serviceVAP_ESTADOS;
        protected Service.Dummies.VAP_PROCESOS _serviceVAP_PROCESOS;
        protected Service.Dummies.VAP_PROCESOS_TASK _serviceVAP_PROCESOS_TASK;
        protected Service.Dummies.VAP_PROCESOS_DEPENDENCY _serviceVAP_PROCESOS_DEPENDENCY;
        protected Service.Dummies.BLL_CHECK_PERIODOSPROCESOS _serviceBLL_CHECK_PERIODOSPROCESOS;

        //protected Model.LOADER_MACROPROCESO_DEADLINE _macroDeadLine;
         //public static readonly log = Common.Config.IPS_Config. LogManager.GetLogger(typeof(IPS_Worker));


        public IPS_Worker()
        {
            //this.SetWorkerPeriod();
            //_repositoryMacroProc = new Repository.LOADER_MACROPROCESO_TASK();
            //_repositoryProcPeriod = new Repository.LOADER_PROC_PERIOD();
            //_repositoryMixedProcPeriod = new Repository.LOADER_PROC_PERIOD_MIXED();
            //_repositoryDeadLine = new Repository.LOADER_MACROPROCESO_DEADLINE();
            //_repositoryLoaderParametros = new Repository.LOADER_PARAMETROS();
            //_repositoryEmail = new Repository.EMAIL();
            _serviceVAP_BULKDATA = new Service.Dummies.BLL_BULKDATA();
            _serviceVAP_PERIODOS = new Service.Dummies.VAP_PERIODOS();
            _serviceVAP_CAUSANTES_BASE = new Service.Dummies.VAP_CAUSANTES_BASE();
            _serviceVAP_SOLICITUDES_BASE = new Service.Dummies.VAP_SOLICITUDES_BASE();
            _serviceVAP_ESTADOS = new Service.Dummies.VAP_PROCESO_ESTADOS();
            _serviceVAP_PROCESOS = new Service.Dummies.VAP_PROCESOS();
            _serviceVAP_PROCESOS_TASK = new Service.Dummies.VAP_PROCESOS_TASK();
            _serviceVAP_PROCESOS_DEPENDENCY = new Service.Dummies.VAP_PROCESOS_DEPENDENCY();
            _serviceBLL_CHECK_PERIODOSPROCESOS = new Service.Dummies.BLL_CHECK_PERIODOSPROCESOS();
            
            Common.Config.IPS_Config.InicializaLog();
        }

        public IPS_Worker(Model.VAP_PERIODOS _VAP_PERIODO)
        {
            _serviceVAP_CAUSANTES_MES = new Service.Dummies.VAP_CAUSANTES_MES(_VAP_PERIODO);
            _serviceBLL_CALCULO_INGRESOPROMEDIO = new Service.Dummies.VAP_INGRESO_PROMEDIO(_VAP_PERIODO);
            _serviceVAP_PERIODOS = new Service.Dummies.VAP_PERIODOS();
            _serviceBLL_CHECK_PERIODOSPROCESOS = new Service.Dummies.BLL_CHECK_PERIODOSPROCESOS();


            Common.Config.IPS_Config.InicializaLog();
        }


        public void SetWorkerJobName(string jobName = null)
        {
            if (jobName != null)
            {
                this.workerJobName = jobName;
            }
        }

        public void SetWorkerJobName()
        {
            this.workerJobName = this.GetType().Name.Replace("IPS_Worker_","");
        }


        public abstract void InitWork(object sender, System.ComponentModel.DoWorkEventArgs e);
       
        public bool checkFileExists(string fileNameToCheck = null)
        {
            if (fileNameToCheck == null)
            {
                fileNameToCheck = localFilename;
            }
            if (File.Exists(localFilename))
            {
                isFileDownloaded = true;
            }
            return isFileDownloaded;
        }

        //public bool MacroProcBegin()
        //{
        //    return _repositoryMacroProc.ChangeMacroprocesoEstado(this._macroProcData.LOADER_MACROPROCESO_ID, period, (int)Common.Config.IPS_Config.MacroProcesoEstado.VERIFICANDO);
        //}

        //public bool MacroProcEnd()
        //{
        //    return _repositoryMacroProc.ChangeMacroprocesoEstado(this._macroProcData.LOADER_MACROPROCESO_ID, period, (int)Common.Config.IPS_Config.MacroProcesoEstado.CARGADO);
        //}

        //public bool MacroProcWarning()
        //{
        //    return _repositoryMacroProc.ChangeMacroprocesoEstado(this._macroProcData.LOADER_MACROPROCESO_ID, period, (int)Common.Config.IPS_Config.MacroProcesoEstado.WARNING);
        //}

        //public bool MacroProcError()
        //{
        //    return _repositoryMacroProc.ChangeMacroprocesoEstado(this._macroProcData.LOADER_MACROPROCESO_ID, period, (int)Common.Config.IPS_Config.MacroProcesoEstado.ERROR);
        //}

        //public bool MacroProcNoDisponible()
        //{
        //    return _repositoryMacroProc.ChangeMacroprocesoEstado(this._macroProcData.LOADER_MACROPROCESO_ID, period, (int)Common.Config.IPS_Config.MacroProcesoEstado.NO_DISPONIBLE, false);
        //}



        public virtual bool workerCanRun(ref Model.VAP_PERIODOS _VAP_PERIODO, Common.Config.IPS_Config.ProcesoID _procesoID, string _procesoNombre, ref Model.VAP_PROCESOS_TASK _VAP_PROCESOS_TASK, bool _cleanTableBeforeToFill = false, Model.SQL_LOADER _SQL_LOADER = default(Model.SQL_LOADER))
        {
            var canRun = false;
            var errorMessage = string.Empty;
            var _hasProcesoDependecyUnresolved = false;
            var wasCleanedTable = false;

            //var _periodoToUse = _serviceVAP_PERIODOS.GETALL().Where(o =>
            //    o.PERIODO_ESTADO == (char)Common.Config.IPS_Config.PeriodoEstado.ABIERTO
            //    || o.PERIODO_ESTADO == (char)Common.Config.IPS_Config.PeriodoEstado.EJECUTANDO).Select(o => o).FirstOrDefault();

            var _periodoToUse = _serviceVAP_PERIODOS.GETALL().Find(o =>
                o.ESTADO_ID == (int)Common.Config.IPS_Config.PeriodoEstado.LISTO_PARAPROCESAR || o.ESTADO_ID == (int)Common.Config.IPS_Config.PeriodoEstado.EN_EJECUCUION);


            if (_periodoToUse != null)
            {
                var _procesoTaskDependencyList = _serviceVAP_PROCESOS_DEPENDENCY.GET((int)_procesoID);

                foreach (var _procesoTaskDependency in _procesoTaskDependencyList)
                {
                    var processSuccess = _serviceVAP_PROCESOS_TASK
                                    .GET(_procesoTaskDependency.PROCESO_ID_DEPENDENCY, _periodoToUse.PERIODO_ID)
                                    .Find(o => o.ESTADO_ID == (int)Common.Config.IPS_Config.ProcesoEstado.TERMINADO_SUCCESS);

                    if (processSuccess == null)
                    {
                        _hasProcesoDependecyUnresolved = true;
                        break;
                    }
                }

                var _procesoTask = _serviceVAP_PROCESOS_TASK
                                    .GET((int)_procesoID, _periodoToUse.PERIODO_ID)
                                    .Find(o => o.ESTADO_ID == (int)Common.Config.IPS_Config.ProcesoEstado.EN_ESPERA_PROCESAMIENTO || o.ESTADO_ID == (int)Common.Config.IPS_Config.ProcesoEstado.REPROCESAR);

                if (_procesoTask != null && _hasProcesoDependecyUnresolved == false)
                {
                    _VAP_PERIODO = _periodoToUse;

                    #region //Clean table
                    if (_cleanTableBeforeToFill)
                    {
                        if (_SQL_LOADER.CLEANTABLE_QUERY.Contains("xxx"))
                            _SQL_LOADER.CLEANTABLE_QUERY = _SQL_LOADER.CLEANTABLE_QUERY.Replace("xxx", _VAP_PERIODO.PERIODO_DISPLAY.ToString());

                        wasCleanedTable = _serviceVAP_BULKDATA.CleanTable(_SQL_LOADER);
                    }
                     #endregion


                    if (_periodoToUse.ESTADO_ID == (int)Common.Config.IPS_Config.PeriodoEstado.LISTO_PARAPROCESAR)
                        _serviceVAP_PERIODOS.UPDATE(_VAP_PERIODO.PERIODO_ID, (int)Common.Config.IPS_Config.PeriodoEstado.EN_EJECUCUION);

                    _procesoTask.ESTADO_ID = (int)Common.Config.IPS_Config.ProcesoEstado.PROCESANDO;
                    _procesoTask.FECHA_INI = DateTime.Now;
                    _procesoTask.FECHA_FIN = default(DateTime);

                    var wasUpdated = _serviceVAP_PROCESOS_TASK.UPDATE(_procesoTask);

                    if (wasUpdated)
                        _VAP_PROCESOS_TASK = _procesoTask;

                    canRun = _cleanTableBeforeToFill ? wasCleanedTable : (_cleanTableBeforeToFill == false);
                }
                else
                    errorMessage = _hasProcesoDependecyUnresolved
                        ? string.Format("ProcesoTask({0}) tiene procesos previos terminados para iniciar su ejecucion!, periodo: {1}", _procesoNombre, _periodoToUse.PERIODO_DISPLAY)
                        : string.Format("No existe procesoTask({0}) estado: \"EN ESPERA PROCESAMIENTO\" para ser ejecutado, periodo: {1}", _procesoNombre, _periodoToUse.PERIODO_DISPLAY);
            }
            else
                errorMessage = "No existe periodo valido para procesar";


            #region //LOG REGISTRY
            Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
            {
                PROCESO_NAME = "workerCanRun()"
                ,
                API_MESSAGE = String.Format("El worker: {0} {1} puede correr. {2}", workerJobName, (canRun ? "SI" : "NO"), (canRun ? string.Empty : errorMessage))
                ,
                TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
            });
            #endregion

            return
                canRun;
        }

        public void webs()
        {
            //_repositoryMacroProc.GetAllMacroprocesos(period);
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
            _threadShutdown = true;
        }
    }
}
