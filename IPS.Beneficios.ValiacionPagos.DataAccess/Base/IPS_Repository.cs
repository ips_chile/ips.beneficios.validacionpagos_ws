﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Principal;
using System.Dynamic;
using Dapper;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using MySql.Data.MySqlClient;
using CommonConfig = IPS.Beneficios.Common.Config;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Base
{
    public abstract class IPS_Repository
    {
        protected DbConnection CreateConnection(CommonConfig.IPS_Config.ConnString _connString)
        {
            DbConnection _dbConnection = default(DbConnection);

            switch (_connString)
            {
                case CommonConfig.IPS_Config.ConnString.ORA_DESAB:

                    _dbConnection = new OracleConnection(Properties.Settings.Default.ConnectionStringAppOraDesab);

                    break;

                case CommonConfig.IPS_Config.ConnString.ORA_PROD:

                    _dbConnection = new OracleConnection(Properties.Settings.Default.ConnectionStringAppOraProd);

                    break;

                case CommonConfig.IPS_Config.ConnString.MYSQL_DESAB:

                    _dbConnection = new MySqlConnection(Properties.Settings.Default.ConnectionStringAppMySQL_Desab);

                    break;

                case CommonConfig.IPS_Config.ConnString.MYSQL_PROD_AFAMILIA_PROCESOS:

                    _dbConnection = new MySqlConnection(Properties.Settings.Default.ConnectionStringAppMySQL_Prod_AfamiliaProcesos);

                    break;

                case CommonConfig.IPS_Config.ConnString.MYSQL_PROD_ARCHIVOS22:

                    _dbConnection = new MySqlConnection(Properties.Settings.Default.ConnectionStringAppMySQL_Prod_Archivo22);

                    break;

                default:

                    _dbConnection = new MySqlConnection(Properties.Settings.Default.ConnectionStringAppMySQL_Desab);

                    break;
            }

            return
                _dbConnection;
        }

        public IPS_Repository()
        {

        }
    }
}
