﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class VAP_CAUSANTES_MES : Base.IPS_Repository
    {
        public List<Model.VAP_CAUSANTES_MES> GET(int anoVigencia)
        {
            DbConnection _conn = null;

            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_anoVigencia", anoVigencia);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (_conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                    _conn.Query<Model.VAP_CAUSANTES_MES>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_CAUSANTES_MES_GET", _params, commandType: CommandType.StoredProcedure).ToList();

                }
            }
            catch (Exception ex)
            {
                if (_conn.State == ConnectionState.Open)
                    _conn.Close();

                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.VAP_CAUSANTES_MES>);
            }
        }

        public List<Model.VAP_CAUSANTES_MES> GET(DateTime fechaReconocimiento, int periodo, Common.Config.IPS_Config.NotificaionID notificacionID)
        {
            DbConnection _conn = null;

            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_fechaReconocimiento", fechaReconocimiento);
                _params.Add("p_periodo", periodo);
                _params.Add("p_notificacionID", (int)notificacionID);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (_conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                    _conn.Query<Model.VAP_CAUSANTES_MES>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_CAUSANTES_MES_GET", _params, commandType: CommandType.StoredProcedure).ToList();

                }
            }
            catch (Exception ex)
            {
                if (_conn.State == ConnectionState.Open)
                    _conn.Close();

                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.VAP_CAUSANTES_MES>);
            }
        }

        public List<Model.VAP_CAUSANTES_MES> GET(Int32 rutCausante, int periodo)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_rutCausante", rutCausante);
                _params.Add("p_periodo", periodo);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                    _conn.Query<Model.VAP_CAUSANTES_MES>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_CAUSANTES_MES_GET", _params, commandType: CommandType.StoredProcedure).ToList();

                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.VAP_CAUSANTES_MES>);
            }
        }

        public int GET_AGE(Int32 rutCausante)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_rutCausante", rutCausante);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                    _conn.Query<int>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_CAUSANTES_MES_GET_AGE", _params, commandType: CommandType.StoredProcedure).SingleOrDefault();

                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(int);
            }
        }


    }
}
