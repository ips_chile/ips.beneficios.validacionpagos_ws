﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class BLL_PAGOS : Base.IPS_Repository
    {
        public Model.BLL_PAGO GET(Int32 rutTrabajador, int periodo)
        {
            try
            {
                var sqlQuery = string.Format(Properties.Resources.MYSQL_BLL_PAGO_getByBeneficiario
                                , rutTrabajador
                                , periodo);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.MYSQL_DESAB))
                {
                    var bll_Pago = _conn.Query<Model.BLL_PAGO>(sqlQuery).SingleOrDefault();

                    return
                        bll_Pago != default(Model.BLL_PAGO) ? bll_Pago : new Model.BLL_PAGO();
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    new Model.BLL_PAGO();
            }
        }
    }
}
