﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;


namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class VAP_SOLICITUDES_PROCESADOS : Base.IPS_Repository
    {
        public List<Model.VAP_SOLICITUDES_PROCESADOS> GET(int anoVigencia)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_periodoHasta", anoVigencia);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                    _conn.Query<Model.VAP_SOLICITUDES_PROCESADOS>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_SOLICITUDES_PROCESADOS_GET", _params, commandType: CommandType.StoredProcedure).ToList();

                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.VAP_SOLICITUDES_PROCESADOS>);
            }
        }
    }
}
