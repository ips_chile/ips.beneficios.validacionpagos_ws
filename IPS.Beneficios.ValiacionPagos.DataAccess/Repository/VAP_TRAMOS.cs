﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class VAP_TRAMOS : Base.IPS_Repository
    {
        public List<Model.VAP_TRAMOS> GET_ALL()
        {
            DbConnection _conn = null;

            try
            {
                var sqlQuery = Properties.Resources.MYSQL_TRAMOS_getAll;

                using (_conn = CreateConnection(Common.Config.IPS_Config.ConnString.MYSQL_PROD_AFAMILIA_PROCESOS))
                {
                    return
                        _conn.Query<Model.VAP_TRAMOS>(sqlQuery).ToList();
                }
            }
            catch (Exception ex)
            {
                if (_conn.State == ConnectionState.Open)
                    _conn.Close();

                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.VAP_TRAMOS>);
            }

        }
    }
}
