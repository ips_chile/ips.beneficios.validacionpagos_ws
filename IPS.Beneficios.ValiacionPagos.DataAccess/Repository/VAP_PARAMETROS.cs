﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class VAP_PARAMETROS : Base.IPS_Repository
    {
        public List<Model.VAP_PARAMETROS> GETALL()
        {
            DbConnection _conn = null;

            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (_conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                        _conn.Query<Model.VAP_PARAMETROS>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_PARAMETROS_GETALL", _params, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                if (_conn.State == ConnectionState.Open)
                    _conn.Close();

                Common.Config.IPS_Config.CaptureException(ex);

                return  default(List<Model.VAP_PARAMETROS>);
            }
        }
    }
}
