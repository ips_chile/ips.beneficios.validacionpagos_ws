﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;
using Oracle.DataAccess.Types;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class VAP_PERIODOS : Base.IPS_Repository
    {
        public Model.VAP_PERIODOS GETBY_PERIODO_DISPLAY(Int32 PERIODO_DISPLAY)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_periodoDisplay", PERIODO_DISPLAY);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                        _conn.Query<Model.VAP_PERIODOS>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_PERIODOS_GET", _params, commandType: CommandType.StoredProcedure).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(Model.VAP_PERIODOS);
            }
        }

        public Model.VAP_PERIODOS GETBY_ESTADO_ID(int ESTADO_ID)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_estado", ESTADO_ID);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                        _conn.Query<Model.VAP_PERIODOS>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_PERIODOS_GET", _params, commandType: CommandType.StoredProcedure).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(Model.VAP_PERIODOS);
            }
        }

        public List<Model.VAP_PERIODOS> GETALL()
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                        _conn.Query<Model.VAP_PERIODOS>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_PERIODOS_GETALL", _params, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.VAP_PERIODOS>);
            }
        }


        public bool UPDATE(int PERIODO_ID, int ESTADO_ID)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_id", PERIODO_ID);
                _params.Add("p_estado", (int)ESTADO_ID);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    _conn.Execute("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_PERIODOS_UPDATE", _params, commandType: CommandType.StoredProcedure);

                    return
                        true;
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return 
                    false;
            }
        }

        public bool INSERT(Model.VAP_PERIODOS _VAP_PERIODO)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_periodoDisplay", _VAP_PERIODO.PERIODO_DISPLAY);
                _params.Add("p_estadoId", _VAP_PERIODO.ESTADO_ID);
                _params.Add("p_estadoIsActive", _VAP_PERIODO.PERIODO_ISACTIVE);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    var executeValue = _conn.Execute("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_PERIODOS_INSERT", _params, commandType: CommandType.StoredProcedure);

                    return
                        executeValue < 0;
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return 
                    false;
            }
        }
    }
}
