﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class VAP_SOLICITUDES_BASE : Base.IPS_Repository
    {
        #region //No se usa
        //public List<Model.VAP_SOLICITUDES_BASE> GET(string archivoIdCargaLike)
        //{
        //    DbConnection _conn = null;

        //    try
        //    {
        //        var _params = new Base.OracleDynamicParameters();
        //        _params.Add("p_archivoIdCargaLike", archivoIdCargaLike);
        //        _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

        //        using (_conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
        //        {
        //            return
        //                _conn.Query<Model.VAP_SOLICITUDES_BASE>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_SOLICITUDES_BASE_GET", _params, commandType: CommandType.StoredProcedure).ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        if (_conn.State == ConnectionState.Open)
        //            _conn.Close();

        //        Common.Config.IPS_Config.CaptureException(ex);

        //        return
        //            default(List<Model.VAP_SOLICITUDES_BASE>);
        //    }

        //}
        #endregion

        public List<Model.VAP_SOLICITUDES_BASE> GET(int periodoProceso)
        {
            DbConnection _conn = null;

            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_periodo", periodoProceso);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (_conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                        _conn.Query<Model.VAP_SOLICITUDES_BASE>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_SOLICITUDES_BASE_GET", _params, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                if (_conn.State == ConnectionState.Open)
                    _conn.Close();

                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.VAP_SOLICITUDES_BASE>);
            }

        }
    }
}
