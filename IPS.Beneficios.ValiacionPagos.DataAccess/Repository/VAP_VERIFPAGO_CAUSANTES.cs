﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class VAP_VERIFPAGO_CAUSANTES : Base.IPS_Repository
    {
        public List<Model.VAP_VERIFPAGO_CAUSANTES> GET(int _CAUSANTE_PERIODO)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_periodo", _CAUSANTE_PERIODO);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                        _conn.Query<Model.VAP_VERIFPAGO_CAUSANTES>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_VERIFPAGOCAU_GETBY_PERIODO", _params, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.VAP_VERIFPAGO_CAUSANTES>);
            }
        }

        public List<Model.VAP_VERIFPAGO_CAUSANTES> GET(int _RUT_EMPLEADOR, Model.VAP_INGRESO_PROMEDIO _CAUSANTE, long _VALORPLANILLA_PERIODO)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_rutEmpleador", _RUT_EMPLEADOR);
                _params.Add("p_rutBeneficiario", _CAUSANTE.RUT_BENEFICIARIO);
                _params.Add("p_rutCausante", _CAUSANTE.RUT_CAUSANTE);
                _params.Add("p_periodo", _CAUSANTE.CAUSANTE_PERIODO);
                _params.Add("p_valorPlanillaPeriodo", _VALORPLANILLA_PERIODO);

                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                        _conn.Query<Model.VAP_VERIFPAGO_CAUSANTES>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_VERIFPAGOCAU_GETBY_PERIODO", _params, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.VAP_VERIFPAGO_CAUSANTES>);
            }
        }

        public List<Model.VAP_VERIFPAGO_CAUSANTES> GET(Int32 RUT_BENEFICIARIO, int _PERIODO)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_rutBeneficiario", RUT_BENEFICIARIO);
                _params.Add("p_periodo", _PERIODO);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                        _conn.Query<Model.VAP_VERIFPAGO_CAUSANTES>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_VERIFPAGOCAU_GETBY_PERIODO", _params, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.VAP_VERIFPAGO_CAUSANTES>);
            }
        }

    }
}
