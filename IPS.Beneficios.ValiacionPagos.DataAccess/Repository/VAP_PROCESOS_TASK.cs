﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class VAP_PROCESOS_TASK : Base.IPS_Repository
    {
        public List<Model.VAP_PROCESOS_TASK> GETALL()
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                        _conn.Query<Model.VAP_PROCESOS_TASK>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_PROCESOS_TASK_GETALL", _params, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }

            return
                default(List<Model.VAP_PROCESOS_TASK>);

        }

        public List<Model.VAP_PROCESOS_TASK> GET(int PROCESO_ID, int PERIODO_ID)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_proceso", PROCESO_ID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                _params.Add("p_periodo", PERIODO_ID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                        _conn.Query<Model.VAP_PROCESOS_TASK>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_PROC_TASK_GETBY_PROC_PER", _params, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }

            return
                default(List<Model.VAP_PROCESOS_TASK>);

        }

        public List<Model.VAP_PROCESOS_TASK> GETBY_PERIODO_ID(int PERIODO_ID)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_periodoId", PERIODO_ID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                _params.Add("p_validacionPagos_display", OracleDbType.RefCursor, direction: ParameterDirection.Output);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    return
                        _conn.Query<Model.VAP_PROCESOS_TASK>("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_PROC_TASK_GET", _params, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }

            return
                default(List<Model.VAP_PROCESOS_TASK>);

        }

        public bool UPDATE(Model.VAP_PROCESOS_TASK VAP_PROCESOS_TASK)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_procesoId", VAP_PROCESOS_TASK.PROCESO_ID);
                _params.Add("p_periodoId", VAP_PROCESOS_TASK.PERIODO_ID);
                _params.Add("p_estadoId", VAP_PROCESOS_TASK.ESTADO_ID);
                _params.Add("p_fechaIni", VAP_PROCESOS_TASK.FECHA_INI);
                _params.Add("p_fechaFin", VAP_PROCESOS_TASK.FECHA_FIN);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    _conn.Execute("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_PROC_TASK_UPDT", _params, commandType: CommandType.StoredProcedure);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return false;
            }
        }

        public bool INSERT(Model.VAP_PROCESOS_TASK VAP_PROCESOS_TASK)
        {
            try
            {
                var _params = new Base.OracleDynamicParameters();
                _params.Add("p_procesoId", VAP_PROCESOS_TASK.PROCESO_ID);
                _params.Add("p_periodoId", VAP_PROCESOS_TASK.PERIODO_ID);
                _params.Add("p_estadoId", VAP_PROCESOS_TASK.ESTADO_ID);
                _params.Add("p_fechaIni", VAP_PROCESOS_TASK.FECHA_INI);
                _params.Add("p_fechaFin", VAP_PROCESOS_TASK.FECHA_FIN);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                {
                    var executeValue = _conn.Execute("VALIDACION_PAGOS.PKG_VALIDACIONESPAGO.VAP_PROC_TASK_INSERT", _params, commandType: CommandType.StoredProcedure);

                    return
                        executeValue < 0;
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return false;
            }
        }

    }
}
