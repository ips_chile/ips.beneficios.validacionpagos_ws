﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO : Base.IPS_Repository
    {
        /// <summary>
        /// GET(), obtiene desde la tabla "RetroactivosArchPrevired_PAGOS_cibYcipab" todos los beneficiarios(Empleadores) agrupado por liquido a pagar.
        /// </summary>
        /// <returns></returns>
        public List<Model.BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO> GETALL()
        {
            try
            {
                var sqlQuery = Properties.Resources.MYSQL_RetroactivosArchPrevired_PAGOS_cibYcipab_getValoresAgrupados;

                //sqlQuery = "SELECT rutBenef as RUT_BENEFICIARIO FROM RetroactivosArchPrevired_PAGOS_cibYcipab";

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.MYSQL_PROD_ARCHIVOS22))
                {
                    return
                        _conn.Query<Model.BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO>(sqlQuery).ToList(); 
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO>);
            }
        }

        /// <summary>
        /// Borra todos los registros de la tabla
        /// </summary>
        /// <returns></returns>
        public bool DELETE() {

            try
            {
                var sqlQuery = Properties.Resources.MYSQL_BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO_truncateTable;

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.MYSQL_PROD_ARCHIVOS22))
                {
                        var result =_conn.Execute(sqlQuery);

                        return
                            result == 0;
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(bool);
            }
        }
    }
}
