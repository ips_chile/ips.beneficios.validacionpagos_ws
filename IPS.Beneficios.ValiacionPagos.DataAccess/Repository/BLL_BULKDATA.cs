﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;
using System.Text;
using System.Transactions;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class BLL_BULKDATA : Base.IPS_Repository
    {
        public bool BulkData(Model.SQL_LOADER SQL_LOADER)
        {
            bool result = false;
            bool cleanedTable = false;
            string ctlPathWithExternalValues = string.Empty;

            if (SQL_LOADER.CLEANTABLE)
                cleanedTable = CleanTable(SQL_LOADER);

            if (SQL_LOADER.DICTIONARY_SETCONTROLFILEVALUES != default(Dictionary<string, string>))
                ctlPathWithExternalValues = Set_ExternalValuesToControlFile(SQL_LOADER.CTLPATH, SQL_LOADER.DICTIONARY_SETCONTROLFILEVALUES);

            if ((SQL_LOADER.CLEANTABLE && cleanedTable) || SQL_LOADER.CLEANTABLE == false)
            {
                StringBuilder szBuffer = new StringBuilder();
                szBuffer.Append(string.Format("userid = '{0}'", Get_SQLLDRUser(Common.Config.IPS_Config.SQLLDRUser.TEST)));
                szBuffer.Append(string.Format(" CONTROL = '{0}' DATA = '{1}'", (string.IsNullOrEmpty(ctlPathWithExternalValues) == false ? ctlPathWithExternalValues : SQL_LOADER.CTLPATH), SQL_LOADER.FILEPATH));
                if (!string.IsNullOrEmpty(SQL_LOADER.LOGPATH)) szBuffer.Append(string.Format(" LOG = '{0}'", SQL_LOADER.LOGPATH));
                if (!string.IsNullOrEmpty(SQL_LOADER.BADPATH)) szBuffer.Append(string.Format(" BAD = '{0}'", SQL_LOADER.BADPATH));
                if (!string.IsNullOrEmpty(SQL_LOADER.DISCARDPATH)) szBuffer.Append(string.Format(" DISCARD = '{0}'", SQL_LOADER.DISCARDPATH));
                szBuffer.Append(string.Format(" PARALLEL = true READSIZE = {0}", 1000000));

                // Use ProcessStartInfo class.
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = Get_SQLLDRPath(Common.Config.IPS_Config.SQLLDRPath.TEST);
                startInfo.Arguments = szBuffer.ToString();
                startInfo.ErrorDialog = true;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = true;
                startInfo.CreateNoWindow = false;

                try
                {
                    using (Process exeProcess = Process.Start(startInfo))
                    {
                        exeProcess.WaitForExit();

                        result = (exeProcess.HasExited) && (exeProcess.ExitCode == 0/* || exeProcess.ExitCode == 2*/);

                        if (string.IsNullOrEmpty(ctlPathWithExternalValues) == false)
                            File.Delete(ctlPathWithExternalValues);

                        return
                            result;
                    }
                }
                catch (Exception ex)
                {
                    Common.Config.IPS_Config.CaptureException(ex);

                    return
                         false;
                }
            }
            else
            {
                #region //LOG REGISTRY
                Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                {
                    PROCESO_NAME = "BulkData(Model.SQL_LOADER SQL_LOADER)"
                    ,
                    API_MESSAGE = String.Format("[ERROR] BulkData no pudo ser ejecutado debido a que la tabla [{0}] no fue limpiada exitosamente", SQL_LOADER.TABLE_NAME)
                    ,
                    TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
                });
                #endregion

                return
                    result;
            }
        }

        public bool BulkDataMysql(string sqlStatement, List<Model.BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO> listToInsert, Common.Config.IPS_Config.ConnString connString)
        {
            bool result = false;

            try
            {
                using (var transactionScope = new TransactionScope())
                {
                    using (var _conn = CreateConnection(connString))
                    {
                        _conn.Open();
                        _conn.Execute(@sqlStatement, listToInsert, commandTimeout: 280);

                        _conn.Close();
                    }

                    transactionScope.Complete();

                    result = true;

                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }

            return
                result;
        }



        #region //PRIVATE
        public bool CleanTable(Model.SQL_LOADER SQL_LOADER)
        {
            if (SQL_LOADER != null && (SQL_LOADER.CLEANTABLE_QUERY.ToUpper().Contains("TRUNCATE") || SQL_LOADER.CLEANTABLE_QUERY.ToUpper().Contains("DELETE")))
            {
                var _wasCleaned = false;

                try
                {
                    var transactionScopeOptions = new TransactionOptions();
                    transactionScopeOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                    transactionScopeOptions.Timeout = TimeSpan.MaxValue;

                    using (var transactionScope = new TransactionScope(TransactionScopeOption.Required, transactionScopeOptions))
                    {
                        using (DbConnection _conn = CreateConnection(Common.Config.IPS_Config.ConnString.ORA_DESAB))
                        {
                            _conn.Open();
                            //or do other things with your table instead of deleting here
                            _conn.Execute(@SQL_LOADER.CLEANTABLE_QUERY, commandTimeout: 480);

                            _conn.Close();
                        }

                        transactionScope.Complete();

                        #region //LOG REGISTRY
                        Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                        {
                            PROCESO_NAME = "CleanTable(Model.SQL_LOADER SQL_LOADER)"
                            ,
                            API_MESSAGE = String.Format("[SUCCESS] La tabla [{0}] fue limpiada exitosamente", SQL_LOADER.TABLE_NAME)
                            ,
                            TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
                        });
                        #endregion

                        _wasCleaned = true;
                    }
                }
                catch (Exception ex)
                {
                    #region //LOG REGISTRY
                    Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                    {
                        PROCESO_NAME = "CleanTable(Model.SQL_LOADER SQL_LOADER)"
                        ,
                        API_MESSAGE = String.Format("[ERROR] La tabla [{0}] NO pudo ser limpiada", SQL_LOADER.TABLE_NAME)
                        ,
                        TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
                    });
                    #endregion

                    Common.Config.IPS_Config.CaptureException(ex);
                }

                return
                    _wasCleaned;
            }
            else
            {
                #region //LOG REGISTRY
                Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                {
                    PROCESO_NAME = "CleanTable(Model.SQL_LOADER SQL_LOADER)"
                    ,
                    API_MESSAGE = String.Format("[ERROR] La tabla [{0}] no fue limpiada. Existe un error de sintaxis en el objeto que contiene la query de borrado: <<{1}>>"
                    , SQL_LOADER.TABLE_NAME
                    , SQL_LOADER.CLEANTABLE_QUERY)
                    ,
                    TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
                });
                #endregion

                return
                     false;
            }
        }

        public bool CleanTable(Model.SQL_LOADER SQL_LOADER, Common.Config.IPS_Config.ConnString connString)
        {
            if (SQL_LOADER != null && (SQL_LOADER.CLEANTABLE_QUERY.ToUpper().Contains("TRUNCATE") || SQL_LOADER.CLEANTABLE_QUERY.ToUpper().Contains("DELETE")))
            {
                var _wasCleaned = false;

                try
                {
                    var transactionScopeOptions = new TransactionOptions();
                    transactionScopeOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                    transactionScopeOptions.Timeout = TimeSpan.MaxValue;

                    using (var transactionScope = new TransactionScope(TransactionScopeOption.Required, transactionScopeOptions))
                    {
                        using (DbConnection _conn = CreateConnection(connString))
                        {
                            _conn.Open();
                            //or do other things with your table instead of deleting here
                            _conn.Execute(@SQL_LOADER.CLEANTABLE_QUERY, commandTimeout: 480);

                            _conn.Close();
                        }

                        transactionScope.Complete();

                        #region //LOG REGISTRY
                        Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                        {
                            PROCESO_NAME = "CleanTable(Model.SQL_LOADER SQL_LOADER)"
                            ,
                            API_MESSAGE = String.Format("[SUCCESS] La tabla [{0}] fue limpiada exitosamente", SQL_LOADER.TABLE_NAME)
                            ,
                            TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
                        });
                        #endregion

                        _wasCleaned = true;
                    }
                }
                catch (Exception ex)
                {
                    #region //LOG REGISTRY
                    Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                    {
                        PROCESO_NAME = "CleanTable(Model.SQL_LOADER SQL_LOADER)"
                        ,
                        API_MESSAGE = String.Format("[ERROR] La tabla [{0}] NO pudo ser limpiada", SQL_LOADER.TABLE_NAME)
                        ,
                        TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
                    });
                    #endregion

                    Common.Config.IPS_Config.CaptureException(ex);
                }

                return
                    _wasCleaned;
            }
            else
            {
                #region //LOG REGISTRY
                Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                {
                    PROCESO_NAME = "CleanTable(Model.SQL_LOADER SQL_LOADER)"
                    ,
                    API_MESSAGE = String.Format("[ERROR] La tabla [{0}] no fue limpiada. Existe un error de sintaxis en el objeto que contiene la query de borrado: <<{1}>>"
                    , SQL_LOADER.TABLE_NAME
                    , SQL_LOADER.CLEANTABLE_QUERY)
                    ,
                    TIPO_OPERACION = Common.Model.EnumLog_Operation.INFO_METHOD
                });
                #endregion

                return
                     false;
            }
        }

        private string Get_SQLLDRUser(Common.Config.IPS_Config.SQLLDRUser SQLLDRUser)
        {
            var user = string.Empty;

            switch (SQLLDRUser)
            {

                case Common.Config.IPS_Config.SQLLDRUser.PROD:
                    user = Properties.Settings.Default.SQLLDR_USER_PROD;

                    break;

                case Common.Config.IPS_Config.SQLLDRUser.TEST:
                    user = Properties.Settings.Default.SQLLDR_USER_TEST;
                    break;
            }

            return user;
        }

        private string Get_SQLLDRPath(Common.Config.IPS_Config.SQLLDRPath SQLLDRPath)
        {
            var path = string.Empty;

            switch (SQLLDRPath)
            {

                case Common.Config.IPS_Config.SQLLDRPath.PROD:
                    path = string.Format(@"{0}", Properties.Settings.Default.SQLLDR_PATH_PROD);
                    break;

                case Common.Config.IPS_Config.SQLLDRPath.TEST:
                    path = string.Format(@"{0}", Properties.Settings.Default.SQLLDR_PATH_TEST);
                    break;
            }

            return path;
        }

        private string Set_ExternalValuesToControlFile(string filePath, Dictionary<string, string> DICTIONARY_SETCONTROLFILEVALUES)
        {
            try
            {
                var tempFilePath = string.Format(@"{0}\{1}{2}", Path.GetDirectoryName(@filePath), Path.GetFileNameWithoutExtension(@filePath) + "_temp", Path.GetExtension(@filePath));

                short count = 0;
                foreach (var item in DICTIONARY_SETCONTROLFILEVALUES)
                {
                    if (count == 0)
                        File.WriteAllText(tempFilePath, Regex.Replace(File.ReadAllText(@filePath), item.Key, item.Value));
                    else
                        File.WriteAllText(tempFilePath, Regex.Replace(File.ReadAllText(tempFilePath), item.Key, item.Value));

                    count++;
                }

                return
                    Common.Helper.FilesHelper.Exist(tempFilePath) ? tempFilePath : string.Empty;
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    string.Empty;
            }

        }
        #endregion
    }
}
