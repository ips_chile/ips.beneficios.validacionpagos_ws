﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;


namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class BLL_CALCULO_INGRESOPROMEDIO : Base.IPS_Repository
    {
        public Model.BLL_INGRESO_PROMEDIO[] GET(string tableName)
        {
            DbConnection _conn = null;

            try
            {
                var sqlQuery = string.Format(Properties.Resources.MYSQL_CALCULO_IMGRESOPROMEDIO_getAll
                                , tableName);

                using (_conn = CreateConnection(Common.Config.IPS_Config.ConnString.MYSQL_PROD_AFAMILIA_PROCESOS))
                {
                    return
                        _conn.Query<Model.BLL_INGRESO_PROMEDIO>(sqlQuery, commandTimeout: 120).ToArray();
                }
            }
            catch (Exception ex)
            {
                if (_conn.State == ConnectionState.Open)
                    _conn.Close();

                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(Model.BLL_INGRESO_PROMEDIO[]);
            }
        }

        public Model.BLL_INGRESO_PROMEDIO GET(string tableName, int rutTrabajador)
        {
            DbConnection _conn = null;

            try
            {
                var sqlQuery = string.Format(Properties.Resources.MYSQL_CALCULO_IMGRESOPROMEDIO_getByTrabajador
                                , tableName
                                , rutTrabajador);

                using (_conn = CreateConnection(Common.Config.IPS_Config.ConnString.MYSQL_PROD_AFAMILIA_PROCESOS))
                {
                    return
                        _conn.Query<Model.BLL_INGRESO_PROMEDIO>(sqlQuery).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                if (_conn.State == ConnectionState.Open)
                    _conn.Close();

                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(Model.BLL_INGRESO_PROMEDIO);
            }
        }
    }
}
