﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dapper;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.Common;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.DataAccess.Repository
{
    public class BLL_CAUSANTE_TRAMOS_HISTORICOS : Base.IPS_Repository
    {
        public List<Model.BLL_CAUSANTE_TRAMOS_HISTORICOS> GET(Model.VAP_CAUSANTES_MES causante)
        {
            try
            {
                var sqlQuery = string.Format(Properties.Resources.MYSQL_BLL_CAUSANTE_TRAMOS_HISTORICOS_getTramos
                                , causante.RUT_CAUSANTE);

                using (var _conn = CreateConnection(Common.Config.IPS_Config.ConnString.MYSQL_PROD_ARCHIVOS22))
                {
                    return
                        _conn.Query<Model.BLL_CAUSANTE_TRAMOS_HISTORICOS>(sqlQuery).ToList();
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    default(List<Model.BLL_CAUSANTE_TRAMOS_HISTORICOS>);
            }
        }
    }
}
