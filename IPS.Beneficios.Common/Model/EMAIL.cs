﻿using System;
using System.Collections.Generic;

namespace IPS.Beneficios.Common.Model
{
    public class EMAIL : IPS_Model
    {
        public string SUBJECT { get; set; }
        public string BODY { get; set; }
        public string EMAIL_LIST { get; set; }
        public string EMAIL_LIST_CC { get; set; }
        public string EMAIL_LIST_BCC { get; set; }
        public List<string> FILESPATH { get; set; }
    }
}
