﻿
namespace IPS.Beneficios.Common.Model
{
    public abstract class IPS_Model
    {
        string _tableName;

        public IPS_Model(string anotherName = null)
        {
            SetTableName(anotherName);
        }

        public void SetTableName(string anotherName = null)
        {
            anotherName = (anotherName == null) ? this.GetType().Name : anotherName;
            _tableName = anotherName.Replace("_MIXED", "");
        }
        public string GetTableName()
        {
            return _tableName;
        }
    }
}
