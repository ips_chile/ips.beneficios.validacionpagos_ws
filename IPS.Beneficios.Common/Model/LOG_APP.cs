﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPS.Beneficios.Common.Model
{
    public class LOG_APP
    {
        public string PERIODO { get; set; }
        public string PROCESO_NAME { get; set; }
        public string FILEPATH { get; set; }
        public string FILENAME { get; set; }
        public string API_MESSAGE { get; set; }
        public string EXTRA_FIELD { get; set; }
        public EnumLog_Operation TIPO_OPERACION { get; set; }
    }

    public enum EnumLog_Operation
    {
        FILE_NOT_PROCECED = 1,
        FILE_PROCECED = 2,
        EMAIL = 3,
        PROCESS_NOT_SUCCESS = 4,
        FALLA_API = 5,
        SERVICE_STARTED = 6,
        SERVICE_STOPED = 7,
        WARNING_API = 8,
        PROCESS_SUCCESS = 9,
        INFO = 10,
        EMPTY_TABLE = 11,
        INFO_METHOD = 12
    };
}
