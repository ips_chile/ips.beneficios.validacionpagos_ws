using System;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Web;
using System.Globalization;
using Ionic.Zip;
using IPS.Beneficios.Common.Repository;
using System.Data;
using Oracle.DataAccess.Types;
using System.Reflection;
using VapModel = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.Common.Helper
{
    public static class Generics
    {
        public static T NextOf<T>(this IList<T> list, T item)
        {
            return list[(list.IndexOf(item) + 1) == list.Count ? 0 : (list.IndexOf(item) + 1)];
        }

        public static List<List<T>> ChunkBy<T>(this List<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }

        public static int GetPeriodoFormatted(int periodoToSet, int monthsToAdd)
        {
            if (periodoToSet != default(int) && monthsToAdd != default(int))
            {
                var year = int.Parse(periodoToSet.ToString().Substring(0, 4));

                var month = int.Parse(periodoToSet.ToString().Substring(periodoToSet.ToString().Length - 2));

                var fechaFormated = DateTime.Parse(string.Format("01/{0}/{1}", month, year));

                fechaFormated = fechaFormated.AddMonths(monthsToAdd);

                return
                    int.Parse(fechaFormated.ToString("yyyyMM"));
            }
            else
                return
                    default(int);
        }

        public static DateTime GetPeriodoFormatted(int periodoToSet)
        {
            if (periodoToSet != default(int))
            {
                var year = int.Parse(periodoToSet.ToString().Substring(0, 4));

                var month = int.Parse(periodoToSet.ToString().Substring(periodoToSet.ToString().Length - 2));

                var fechaFormated = DateTime.Parse(string.Format("01/{0}/{1}", month, year));

                return
                    fechaFormated;
            }
            else
                return
                    default(DateTime);
        }



        public static DataTable ConfigureDataTable(object objectDataTableRefers)
        {

            DataTable dt = new DataTable();

            foreach (var property in objectDataTableRefers.GetType().GetProperties())
            {
                dt.Columns.Add(property.Name, property.PropertyType);
            }

            return
                dt;
        }

        /// <summary>
        /// GetperiodosPerMont(), Get an Int[] array wich includes periods formated as(yyyyMM)
        /// </summary>
        /// <param name="fechaDesde"></param>
        /// <param name="fechaHasta"></param>
        /// <returns>int[]</returns>
        public static int[] GetperiodosPerMont(DateTime fechaDesde, DateTime fechaHasta)
        {
            List<int> periodoList = new List<int>();

            try
            {
                if (fechaDesde <= fechaHasta)
                {
                    var fecDesdeFormated = DateTime.Parse(string.Format("01/{0}/{1}", fechaDesde.Month, fechaDesde.Year));
                    var fecHastaFormated = DateTime.Parse(string.Format("01/{0}/{1}", fechaHasta.Month, fechaHasta.Year));

                    for (DateTime date = fecDesdeFormated; date <= fecHastaFormated; date = date.AddMonths(1))
                    {
                        periodoList.Add(int.Parse(date.ToString("yyyyMM")));
                    }
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }

            return
                periodoList.ToArray();
        }

        /// <summary>
        /// GetIntArray() Cast a single String to int[]. The string must contain ("," , ";" or "|") as separator character.
        /// </summary>
        /// <param name="arrayCommaSeparated"></param>
        /// <returns>int[]</returns>
        public static int[] GetIntArray(string arrayCommaSeparated)
        {

            int[] listOfInts = null;

            try
            {
                if (arrayCommaSeparated.Contains(",") || arrayCommaSeparated.Contains(";") || arrayCommaSeparated.Contains("|"))
                {
                    var charToSplit = arrayCommaSeparated.Contains(",") ? ',' : (arrayCommaSeparated.Contains(";") ? ';' : '|');

                    var listOfStrings = arrayCommaSeparated.Split(charToSplit);

                    listOfInts = listOfStrings.Select<string, int>(q => Convert.ToInt32(q)).ToArray();
                }
            }
            catch (InvalidCastException ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }

            return
                listOfInts;
        }

        public static bool IsArrayOf<T>(Type type)
        {
            return type == typeof(T[]);
        }

        public static int GetAge(DateTime birthDay)
        {
            int age = DateTime.Now.Year - birthDay.Year;
            if (DateTime.Now.Month < birthDay.Month || (DateTime.Now.Month == birthDay.Month && DateTime.Now.Day < birthDay.Day)) age--;
            return age;
        }

        public static void RemoveTemporallyFile(string path)
        {
            if (MyFile.Exist(path))
                File.Delete(path);
        }

        public static bool CompressToZipFile(string filePath)
        {
            var success = false;

            try
            {
                var extentionFile = Path.GetExtension(filePath).Replace(".", string.Empty);

                Generics.RemoveTemporallyFile(filePath.Replace(extentionFile, "zip"));

                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(filePath, "");
                    zip.Save(filePath.Replace(extentionFile, "zip"));
                }

                success = MyFile.Exist(filePath.Replace(extentionFile, "zip"));
            }
            catch (Exception)
            {
                success = false;
            }

            return
             success;
        }


        //public static string GetPeriodoProcesamiento_Actual(string period = null)
        //{
        //    DateTime date;
        //    date = (period == null) ? DateTime.Now : DateTime.ParseExact(period, "yyyyMM", CultureInfo.InvariantCulture);

        //    return date.ToString("yyyyMM");
        //}

        //public static string GetPeriodoProcesamientoMesMas1(string period = null)
        //{
        //    DateTime date;
        //    date = (period == null) ? DateTime.Now : DateTime.ParseExact(period, "yyyyMM", CultureInfo.InvariantCulture);

        //    return date.AddMonths(1).ToString("yyyyMM");
        //}

        public static string GetVisitorIPAddress(bool GetLan = false)
        {
            string visitorIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (String.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = HttpContext.Current.Request.UserHostAddress;

            if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
            {
                GetLan = true;
                visitorIPAddress = string.Empty;
            }

            if (GetLan)
            {
                if (string.IsNullOrEmpty(visitorIPAddress))
                {
                    //This is for Local(LAN) Connected ID Address
                    string stringHostName = Dns.GetHostName();
                    //Get Ip Host Entry
                    IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
                    //Get Ip Address From The Ip Host Entry Address List
                    IPAddress[] arrIpAddress = ipHostEntries.AddressList;

                    try
                    {
                        visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                    }
                    catch
                    {
                        try
                        {
                            visitorIPAddress = arrIpAddress[0].ToString();
                        }
                        catch
                        {
                            try
                            {
                                arrIpAddress = Dns.GetHostAddresses(stringHostName);
                                visitorIPAddress = arrIpAddress[0].ToString();
                            }
                            catch
                            {
                                visitorIPAddress = "127.0.0.1";
                            }
                        }
                    }
                }
            }
            return visitorIPAddress;
        }

        /// <summary>
        /// GetMonthName, returns month name from string. It must to be setted like: yyyyMM or yyyyMMdd
        /// </summary>
        /// <param name="period"></param>
        /// <returns>string</returns>
        public static string GetMonthName(string period)
        {
            string monthName = null;

            try
            {
                if (period.Length == 6)
                {
                    var year = int.Parse(period.Substring(0, 4));

                    var month = int.Parse(period.ToString().Substring(period.ToString().Length - 2));

                    monthName = new DateTime(year, month, 1).ToString("MMM", CultureInfo.CreateSpecificCulture("es-CL"));

                }
                else if (period.Length > 6)
                {
                    var year = int.Parse(period.Substring(0, 4));

                    var month = int.Parse(period.Substring(4, 4));

                    var day = int.Parse(period.Substring(5));

                    monthName = new DateTime(year, month, day).ToString("MMM", CultureInfo.CreateSpecificCulture("es-CL"));
                }

                return
                    monthName.ToUpper();
            }
            catch
            {
                return
                    DateTime.Now.ToString("MMM", CultureInfo.CreateSpecificCulture("es-CL")).ToUpper(); ;//RETURN CURRENT MONTH NAME
            }
        }


        #region //COMMON VARS
        #region GetCommonConfig
        public static string GetCommonConfig(string constantName = null)
        {
            var common_Config = new Common.Model.COMMON_VARS();
            using (OracleConnection connection = new OracleConnection(Properties.Settings.Default.ConnectionStringAppOraDesab))
            {
                OracleCommand command = new OracleCommand();

                try
                {
                    command.Connection = connection;
                    command.CommandText = "VALIDACION_PAGOS.PKG_BeneficiosCommon.GET_COMMON_VAR";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("pVarName", OracleDbType.Varchar2, 4000).Value = constantName;
                    command.Parameters.Add(new OracleParameter("p_value", OracleDbType.Varchar2, 4000)).Direction = ParameterDirection.Output;

                    connection.Open();
                    command.ExecuteNonQuery();
                    common_Config.CONSTANT_NAME = constantName;
                    common_Config.CONSTANT_VALUE = ((OracleString)command.Parameters["p_value"].Value).IsNull ? String.Empty : Convert.ToString(command.Parameters["p_value"].Value);
                }
                catch (Exception ex)
                {
                    Common.Config.IPS_Config.CaptureException(ex);
                }
                finally
                {
                    connection.Dispose();
                    command.Dispose();
                }
                return common_Config.CONSTANT_VALUE;
            }
        }
        #endregion

        #region SetCommonConfig
        /// <summary>
        /// Query para setear una configuraci�n com�n con el nombre de constante dado y valor.
        /// La funci�n determina si es update o create
        /// </summary>
        /// <param name="constantName">Corresponde al nombre de la constante a crear/actualizar.</param>
        /// <param name="constantValue">Corresponde al valor de la constante a crear/actualizar.</param>
        /// <returns>Retorna el caracter C si la tupla no exist�a y tuvo que crearla, y U si tuvo que actualizarla.</returns>
        public static bool SetCommonConfig(string constantName = null, string constantValue = null)
        {
            bool returnedValue = false;
            using (OracleConnection connection = new OracleConnection(Properties.Settings.Default.ConnectionStringAppOraDesab))
            {
                OracleCommand command = new OracleCommand();

                try
                {
                    //constantValue = constantValue == null ? String.Empty : constantValue;
                    command.Connection = connection;
                    command.CommandText = "VALIDACION_PAGOS.PKG_BeneficiosCommon.SET_COMMON_VAR";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("pVarName", OracleDbType.Varchar2, 100).Value = constantName;
                    command.Parameters.Add("p_value", OracleDbType.Varchar2, 100).Value = constantValue;
                    command.Parameters.Add(new OracleParameter("p_status", OracleDbType.Char, 1)).Direction = ParameterDirection.Output;

                    connection.Open();
                    command.ExecuteNonQuery();
                    if (((OracleString)command.Parameters["p_status"].Value).IsNull)
                    {
                        returnedValue = false;
                    }
                    else
                    {
                        string returnedStr = Convert.ToString(command.Parameters["p_status"].Value);
                        if (returnedStr.Equals('U') || returnedStr.Equals('C'))
                            returnedValue = true;
                    }
                }
                catch (Exception ex)
                {
                    Common.Config.IPS_Config.CaptureException(ex);
                }
                finally
                {
                    connection.Dispose();
                    command.Dispose();
                }
                return returnedValue;
            }
        }
        #endregion
        #endregion
    }

    public static class MyFile
    {
        const string longPathSpecifier = @"\\?";

        public static bool Exist(string path)
        {
            return
                checkFile(path);
        }

        private static bool checkFile(string path)
        {
            // Add the long-path specifier if it's missing
            string longPath = string.Format(@"{0}", path);

            return
                File.Exists(longPath);
        }

    }


}
