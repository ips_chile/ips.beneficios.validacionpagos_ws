﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace IPS.Beneficios.Common.Helper
{
    public abstract class HtmlReader : FileReader
    {
        public HtmlReader(Service.IPS_Worker worker) : base(worker)
        {
            _cumulatedRows = new Dictionary<int, object>();

        }

        public int getDocumentStart()
        {
            int counter = 0;
            int trLocation = 0;
            string line;

            System.IO.StreamReader file = new System.IO.StreamReader(@_filenamePath);
            while ((line = file.ReadLine()) != null)
            {
                if (line.Contains("<tr>"))
                {
                    trLocation = counter;
                }
                if ((counter == trLocation + 1) && (line.Contains("<td>")))
                {
                    return trLocation;
                }
                counter++;
            }

            file.Close();
            return counter + 1;
        }

        public int getDistanceToNextRow(int beginIndex)
        {
            int counter = 0;
            string line;

            System.IO.StreamReader file = new System.IO.StreamReader(@_filenamePath);
            while ((line = file.ReadLine()) != null)
            {
                if (counter > beginIndex)
                {
                    if (line.Contains("<tr>"))
                    {
                        return counter - beginIndex;
                    }
                }

                counter++;
            }

            file.Close();
            return counter + 1;
        }

        public string removeHtmlTags(string taggedHtml)
        {
            return Regex.Replace(taggedHtml, "<.*?>", string.Empty);
        }

        //public bool DataFormat()
        //{
        //    this.SetFileContainerInfo();
            
        //    return true;
        //}

        public abstract bool readExcelData();
    }
}
