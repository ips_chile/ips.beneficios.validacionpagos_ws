using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPS.Beneficios.Common.Helper
{
    class StringUtils
    {
        #region Funciones de Cadena
        #region Left
        public static string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }
        #endregion
        #region Right
        public static string Right(string param, int length)
        {
            int value = param.Length - length;
            string result = param.Substring(value, length);
            return result;
        }
        #endregion
        #endregion
    }
}
