﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
//using ServiceStack.Text;
using VapModel = IPS.Beneficios.ValiacionPagos.Model;
using System.Collections;
using System.ComponentModel;

namespace IPS.Beneficios.Common.Helper
{
    public static class FilesHelper
    {
        public static bool CheckQuantityAndFillCSV(string _preAssembledFilePath, string extencion, string csvDelimiter, bool isEndofList, Int32 maxCantToFillCSV, ref DataTable dataTable, ref int _contadorArchivo)
        {
            var filePath = string.Format("{0}_{1}.{2}", _preAssembledFilePath, _contadorArchivo, extencion);

            var sizeMB = Common.Helper.FilesHelper.GetFileSize(filePath);

            if (sizeMB >= Common.Config.IPS_Config._maxValueMBToFillFile)
                filePath = filePath = string.Format("{0}_{1}.{2}", _preAssembledFilePath, _contadorArchivo++, extencion);

            var wasFilled = false;

            // Performanced setted value, a higher value could break procces, "out of Memory" lack
            if (dataTable.Rows.Count >= maxCantToFillCSV || isEndofList)
            {
                wasFilled = FillCSV(dataTable, filePath, csvDelimiter);

                dataTable.Clear();
            }

            return
                wasFilled;
        }

        public static bool CheckEndOfList(Int32 currentValue, Int32 maxValue, Int32 deadLineValue)
        {
            return
                (maxValue - currentValue) <= deadLineValue;
        }

        public static string[] GetFiles(string folder, string searchPattern) {

            return
                Directory.GetFiles(folder, searchPattern);
        }

        /// <summary>
        /// GetFileSize(), return in MB size of file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static double GetFileSize(string filePath)
        {
            double length = 0;

            if (Exist(filePath))
                length = (new System.IO.FileInfo(filePath).Length / 1024f) / 1024f;
                   
            return
                length;
        }

        public static bool FillCSV(DataTable dataTable, string filePath, string delimiter)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                foreach (DataRow row in dataTable.Rows)
                {
                    var rowAsString = string.Join(delimiter, row.ItemArray);

                    sb.AppendLine(rowAsString.TrimEnd(char.Parse(delimiter)));
                }

                File.AppendAllText(filePath, sb.ToString());
                sb.Clear();

                return
                    Exist(filePath);

            }
            catch (OutOfMemoryException ex)
            {
                Config.IPS_Config.CaptureException(ex);

                return
                    false;
            }
            catch (Exception ex)
            {
                Config.IPS_Config.CaptureException(ex);

                return
                    false;
            }

        }

        public static bool CheckAndCreateCSVFile(string filePath, string delimiter, string[] arrayColumnHeader = null)
        {
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }

            if (arrayColumnHeader != null)
            {
                string[][] output = new string[][] { arrayColumnHeader };
                int length = output.GetLength(0);
                StringBuilder sb = new StringBuilder();
                for (int index = 0; index < length; index++)
                    sb.AppendLine(string.Join(delimiter, output[index]));
                File.AppendAllText(filePath, sb.ToString());
            }

            return
                Exist(filePath);
        }

        public static bool checkAndCreateDir(string entirePath = null)
        {
            try
            {
                if (!Directory.Exists(entirePath))
                    Directory.CreateDirectory(entirePath);

                return
                    Directory.Exists(entirePath);
                
            }
            catch (Exception ex)
            {
                Config.IPS_Config.CaptureException(ex);

                return
                    false;
            }
        }

        public static bool CleanFolder( string searchPattern, string entirePath)
        {
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(entirePath);

                directory.Empty(searchPattern + "*");

                return
                    directory.GetFiles(searchPattern + "*").Count() == 0;
            }
            catch (Exception ex)
            {
                Config.IPS_Config.CaptureException(ex);

                return
                    false;
            }

        }

        public static bool Exist(string path)
        {
            // Add the long-path specifier if it's missing
            string longPath = string.Format(@"{0}", path);

            return
                File.Exists(longPath);
        }

        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        public static DataTable CsvToDataTable(string filePath, char separator, Dictionary<string, Type> dictionaryColumn = null)
        {
            DataTable dataTable = new DataTable();
            if (dictionaryColumn != null)
            {
                foreach (var column in dictionaryColumn)
                {

                    if (column.Value.FullName == "System.DateTime")
                        dataTable.Columns.Add(new DataColumn(column.Key, typeof(DateTime)));
                    else
                        dataTable.Columns.Add(new DataColumn(column.Key));
                       

                       // dataTable.Columns.Add(_header);
                        //dataTable.Columns.Add(new DataColumn("dsdsd", typeof(OracleDate)));
                }
            }

            string line = null;
            long i = dataTable.Columns.Count > 0 ? 1 : 0;

            using (StreamReader sr = File.OpenText(@filePath))
            {
                sr.DiscardBufferedData();

                if (dictionaryColumn != null) sr.ReadLine();

                while ((line = sr.ReadLine()) != null)
                {
                    string[] data = line.Split(separator);
                    if (data.Length > 0)
                    {
                        if (i == 0)
                        {
                            foreach (var item in data)
                            {
                                dataTable.Columns.Add(new DataColumn());
                            }
                            i++;
                        }
                        DataRow row = dataTable.NewRow();
                        row.ItemArray = data;
                        dataTable.Rows.Add(row);
                    }
                }
            }

            return
                dataTable;
        }

        private static void Empty(this System.IO.DirectoryInfo directory, string searchPattern)
        {
            foreach (System.IO.FileInfo file in directory.GetFiles(searchPattern)) file.Delete();
            //foreach (System.IO.DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
        }
    }
}
