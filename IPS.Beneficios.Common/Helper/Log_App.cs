﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPS.Beneficios.Common.Helper
{
    public static class Log_App
    {
        private static Repository.EMAIL _repositoryEmail = new Repository.EMAIL();

        public static System.Diagnostics.EventLog eventLogSimple;

        private static readonly ILog log = LogManager.GetLogger(typeof(Model.LOG_APP));
        //protected static readonly ILog log = LogManager.GetLogger(typeof(WinService));
        public static void WriteErrorLog_I(string ex)
        {
            try
            {
                eventLogSimple.WriteEntry(ex);
            }
            catch (Exception)
            {
            }
        }

        public static void WriteErrorLog_E(string ex)
        {
            try
            {
                eventLogSimple.WriteEntry(ex, EventLogEntryType.Error);

                #region //LOG REGISTRY
                AddLogRegistry(new Model.LOG_APP()
                {
                    API_MESSAGE = ex
                     ,
                    TIPO_OPERACION = Model.EnumLog_Operation.FALLA_API
                    ,
                    PROCESO_NAME = "LogAPP"
                });
                #endregion

                #region //EMAIL
                _repositoryEmail.SendEmail(new Common.Model.EMAIL()
                {
                    EMAIL_LIST = Properties.Settings.Default.AdminReplyToEmail_Single
                    ,
                    SUBJECT = "[ERROR] AUTOMATE TASK SERVICE..."
                    ,
                    BODY = ex
                });
                #endregion
              

            }
            catch (Exception)
            {
            }
        }

        public static void WriteErrorLog_W(string ex)
        {
            try
            {
                eventLogSimple.WriteEntry(ex, EventLogEntryType.Warning);

                #region //LOG REGISTRY
                AddLogRegistry(new Model.LOG_APP()
                {
                    API_MESSAGE = ex
                     ,
                    TIPO_OPERACION = Model.EnumLog_Operation.WARNING_API
                    , PROCESO_NAME = "LogAPP"
                });
                #endregion

            }
            catch (Exception)
            {
            }
        }

        public static void AddLogRegistry(Model.LOG_APP logInfo)
        {
            var logMessage = string.Empty;

            try
            {
                switch (logInfo.TIPO_OPERACION)
                {
                    case Model.EnumLog_Operation.INFO:

                        logMessage = string.Format(":::::: Periodo: {0} -- {2} -- Proceso:{1}"
                            , logInfo.PERIODO
                            , logInfo.PROCESO_NAME
                            , logInfo.API_MESSAGE);

                        log.Info(logMessage);

                        break;
                    case Model.EnumLog_Operation.INFO_METHOD:

                        logMessage = string.Format(":::::: Proceso: {0} -- INFO:{1}..."
                            , logInfo.PROCESO_NAME
                            , logInfo.API_MESSAGE);

                        log.Info(logMessage);

                        break;

                    case Model.EnumLog_Operation.FILE_NOT_PROCECED:

                        logMessage = string.Format(":::::: Periodo: {0} -- Operacion: FILE NOT PROCECED -- Ruta:{1}"
                            , logInfo.PERIODO
                            , logInfo.FILEPATH);

                        log.Info(logMessage);

                        break;

                    case Model.EnumLog_Operation.FILE_PROCECED:

                        logMessage = string.Format(":::::: Periodo: {0} -- Operacion: FILE PROCECED -- Ruta:{1}"
                            , logInfo.PERIODO
                            , logInfo.FILEPATH);

                        log.Info(logMessage);

                        break;

                    case Model.EnumLog_Operation.EMAIL:

                        logMessage = string.Format(":::::: Operacion: EMAIL SENT -- Nombre:[{0}]"
                            , logInfo.API_MESSAGE);

                        log.Info(logMessage);

                        break;


                    case Model.EnumLog_Operation.FALLA_API:

                        logMessage = string.Format(":::::: Operacion: FALLA EN PROCESO {0} -- Descripcion: [{1}...]"
                            , logInfo.PROCESO_NAME, logInfo.API_MESSAGE);

                        log.Error(logMessage);

                        break;

                    case Model.EnumLog_Operation.WARNING_API:

                        logMessage = string.Format(":::::: Operacion: WARNING EN PROCESO {0} -- Descripcion: [{1}...]"
                            , logInfo.PROCESO_NAME, logInfo.API_MESSAGE);

                        log.Warn(logMessage);

                        break;


                    case Model.EnumLog_Operation.SERVICE_STARTED:

                        logMessage = ":::::: Service has been Starded!";

                        log.Info(logMessage);

                        break;

                    case Model.EnumLog_Operation.SERVICE_STOPED:

                        logMessage = ":::::: Service has been Stoped!";

                        log.Info(logMessage);

                        break;

                    case Model.EnumLog_Operation.PROCESS_NOT_SUCCESS:

                        logMessage = string.Format(":::::: Periodo: {0} -- Operacion: WORKER NOT SUCCESS -- Nombre: {1}"
                            , logInfo.PERIODO
                            , logInfo.PROCESO_NAME);

                        log.Info(logMessage);

                        break;

                    case Model.EnumLog_Operation.PROCESS_SUCCESS:

                        logMessage = string.Format(":::::: Periodo: {0} -- Operacion: WORKER SUCCESS -- Nombre: {1}"
                            , logInfo.PERIODO
                            , logInfo.PROCESO_NAME);

                        log.Info(logMessage);

                        break;
                    case Model.EnumLog_Operation.EMPTY_TABLE:

                        logMessage = string.Format(":::::: Periodo: {0} -- Operacion: EMPTY TABLE -- Nombre: {1} OBS: La tabla requerida [{2}] para que el proceso funcione se encuentra vacía. "
                            , logInfo.PERIODO
                            , logInfo.PROCESO_NAME
                            , logInfo.EXTRA_FIELD);

                        log.Info(logMessage);

                        break;
                        
                }

            }
            catch (Exception ex)
            {
                WriteErrorLog_E(string.Format("Msg:{0}  Pila:{1}", ex.Message, ex.StackTrace));
            }
        }

    }
}
