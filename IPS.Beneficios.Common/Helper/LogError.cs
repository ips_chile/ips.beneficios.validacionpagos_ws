using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;

namespace IPS.Beneficios.Common.Helper
{
  public class LogError
  {
    #region Guarda_Log
    public void Guarda_Log(string Componente, string Clase, string Metodo, string Excepcion, string Parametros)
    {
      StreamWriter log;
      string sFile = AppDomain.CurrentDomain.BaseDirectory + @"Log\Log_" + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + ".txt";

      if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + @"Log\"))
      {
        Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"Log\");
      }
      if (!File.Exists(sFile))
      {
        log = new StreamWriter(sFile);
        log.WriteLine("Fecha|Assembly|Clase|Metodo|Error|Parametros");
      }
      else
      {
        log = File.AppendText(sFile);
      }

      log.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "|" + Componente + "|" + Clase + "|" + Metodo + "|" + Excepcion + "|" + Parametros);
      log.Close();
    }
    #endregion
  }
}
