﻿using System.Collections.Generic;

namespace IPS.Beneficios.Common.Helper
{
    public abstract class FileReader
    {
        protected Dictionary<int,object> _cumulatedRows;
        protected string _filenamePath;
        protected string localFileContainerName = null;
        protected string localFileContainerWithPath = null;

        protected string _workerJobName;
        protected string _workerProfileLocation;
        protected string _period;
        protected string _today;

        public FileReader(Service.IPS_Worker worker)
        {
            _cumulatedRows = new Dictionary<int, object>();
            
            _filenamePath = worker.localFilename;
            _workerProfileLocation = worker.workerProfileLocation;
            _workerJobName = worker.workerJobName;
            _period = worker.period;
            _today = worker.today;
        }

        public void SetFileContainerInfo()
        {
            localFileContainerName = _workerJobName + "_" + _period + "_" + _cumulatedRows.Keys.Count + ".TXT";
            localFileContainerWithPath = _workerProfileLocation + "\\Downloads\\" + localFileContainerName;
        }

        public virtual bool DataFormat()
        {
            this.SetFileContainerInfo();

            return true;
        }

    }
}
