﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Linq;
using FlagFtp;
using VAP_Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.Common.Helper
{
    public class FtpComponent
    {
        private string _localFileContainerName = null;
        private string _localFileContainerWithPath = null;
        private string _workerJobName;
        private static string _baseFtpPath;
        private static string _extraFolderToGet = null;

        public FtpComponent(string localFileContainerName, string localFileContainerWithPath, string workerJobName)
        {
            _localFileContainerName = localFileContainerName;
            _localFileContainerWithPath = localFileContainerWithPath;
            _workerJobName = workerJobName;
            _baseFtpPath = Properties.Settings.Default.FtpUrlToConnect + "/" + _workerJobName;
        }

        public FtpComponent(string workerJobName)
        {
            _workerJobName = workerJobName;
            _baseFtpPath = Properties.Settings.Default.FtpUrlToConnect + "/" + _workerJobName;
        }

        public FtpComponent(){}

        public void SetExtraFolder(string exFolder = null)
        {
            if (exFolder != null)
            {
                _extraFolderToGet = exFolder;
            }
        }

        public static string GetFtpPathToDownload(bool withExtraFolder = true)
        {
            if (withExtraFolder)
            {
                if (_extraFolderToGet != null)
                {
                    return _baseFtpPath + "/" + _extraFolderToGet;
                }
            }
            return _baseFtpPath;
        }

        public static List<FtpFileInfo> GetFilesList(string directoryPath = null)
        {
            if (directoryPath == null)
            {
                directoryPath = GetFtpPathToDownload();
            }
            var fileList = new List<FtpFileInfo>();
            var credential = new NetworkCredential(Properties.Settings.Default.FtpUsername, Properties.Settings.Default.FtpPasswd);

            var client = new FtpClient(credential);
            try
            {
                fileList = client.GetFiles(new Uri(directoryPath)).ToList();
            }
            catch (Exception ex)
            {
                Config.IPS_Config.CaptureException(ex);
            }

            return fileList;
        }

        public List<FtpFileInfo> GetFilesListByExtension(string extension = null)
        {
            var allFiles = GetFilesList();
            
            return allFiles.Where(x => x.Name.Contains(extension)).ToList();
        }

        public bool DownloadFile(Uri filePath = null, string localPath = null)
        {
            bool fileWasDownloaded = false;
            if (filePath != null)
            {
                FtpStream fileStream;
                var credential = new NetworkCredential(Properties.Settings.Default.FtpUsername, Properties.Settings.Default.FtpPasswd);

                var client = new FtpClient(credential);
                try
                {
                    fileStream = client.OpenRead(filePath);
                    using (var localStream = File.Create(localPath))
                    {
                        fileStream.CopyTo(localStream, 49152);
                        fileWasDownloaded = true;
                    }
                }
                catch (Exception ex)
                {
                    Config.IPS_Config.CaptureException(ex);
                    fileWasDownloaded = false;
                }
            }
            return fileWasDownloaded;
        }

        public static bool DownloadFile(string filePath, string filePathToDownload)
        {
            //QUIZA AQUI HALLA QUE VALDIDAR SI SE ESTA REPORCESANDO, SI FUERA ASI ENTONCES QUIZA NO SE DEBERIA VOLVER A DESCARGAR...
            try
            {
                var fileName = Path.GetFileName(filePath);

                bool isDownloaded = FilesHelper.Exist(filePathToDownload + "\\" + fileName);
                
                if (isDownloaded == false)
                {
                    // Get the object used to communicate with the server.
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(filePath);

                    request.Proxy = null;

                    request.ReadWriteTimeout = 1000 * 6 * Properties.Settings.Default.TimeOutMinutesToDownloadFileInfo;

                    request.Method = WebRequestMethods.Ftp.DownloadFile;

                    request.Credentials = new NetworkCredential(Properties.Settings.Default.FtpUsername, Properties.Settings.Default.FtpPasswd);

                    byte[] buffer = new byte[16*4096];

                    using (var response = (FtpWebResponse)request.GetResponse())
                    {
                        using (var fileStream = File.Create(filePathToDownload + "\\" + fileName))
                        {
                            response.GetResponseStream().CopyTo(fileStream, buffer.Length);
                        }
                    }
                }

                return
                    FilesHelper.Exist(filePathToDownload + "\\" + fileName);

            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return
                    false;
            }

        }

        public FtpFileInfo CheckForOldPeriod(List<FtpFileInfo> fileList, string extension = null)
        {
            string sPattern = "_\\d{6}." + extension + "$";

            return fileList.FirstOrDefault(x => System.Text.RegularExpressions.Regex.IsMatch(x.Name, sPattern));
        }

        public bool UploadFile(bool withExtraFolder = false)
        {
            bool returnedValue = true;
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(GetFtpPathToDownload(withExtraFolder) + "/" + _localFileContainerName);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                request.Credentials = new NetworkCredential(Properties.Settings.Default.FtpUsername, Properties.Settings.Default.FtpPasswd);

                StreamReader sourceStream = new StreamReader(@_localFileContainerWithPath);
                byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                sourceStream.Close();
                request.ContentLength = fileContents.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);

                response.Close();
            }

            catch (Exception ex)
            {
                Config.IPS_Config.CaptureException(ex);
                returnedValue = false;
            }

            return returnedValue;
        }

        public static bool UploadFile(string fromPath, string toPath)
        {
            var success = false;

            try
            {
                string filename = Path.GetFileName(fromPath);
                string ftpfullpath = toPath;
                FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(ftpfullpath);
                ftp.Credentials = new NetworkCredential(Properties.Settings.Default.FtpUsername, Properties.Settings.Default.FtpPasswd);

                ftp.KeepAlive = true;
                ftp.UseBinary = true;
                ftp.Method = WebRequestMethods.Ftp.UploadFile;

                FileStream fs = File.OpenRead(fromPath);
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();

                Stream ftpstream = ftp.GetRequestStream();
                ftpstream.Write(buffer, 0, buffer.Length);
                ftpstream.Close();

                success = GetFileInfo(toPath) != default(FtpFileInfo);
            }
            catch (Exception ex)
            {
                Config.IPS_Config.CaptureException(ex);
                success = false;
            }

            return
                success;
        }

        public static bool MoveFile(Uri oldFilenameLocation, string folderToMove = "")
        {
            Uri uriSource = new Uri(oldFilenameLocation.ToString(), UriKind.Absolute);
            var filename = Path.GetFileName(oldFilenameLocation.ToString());
            Uri uriDestination = new Uri(GetFtpPathToDownload(withExtraFolder: true) + "/" + folderToMove + "/" + filename, UriKind.Absolute);
            
            Uri targetUriRelative = uriSource.MakeRelativeUri(uriDestination);
            
            FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(new Uri(uriSource.AbsoluteUri));
            ftp.Method = WebRequestMethods.Ftp.Rename;
            ftp.Credentials = new NetworkCredential(Properties.Settings.Default.FtpUsername, Properties.Settings.Default.FtpPasswd);
            ftp.UsePassive = true;
            ftp.RenameTo = Uri.UnescapeDataString(targetUriRelative.OriginalString);
            
            FtpWebResponse ftpresponse = (FtpWebResponse)ftp.GetResponse();
            
            return (ftpresponse.StatusCode.Equals("FileActionOK")) ? true : false;
        }

        public static FtpFileInfo GetFileInfo(string filePath)
        {

            try
            {
                var credential = new NetworkCredential(Properties.Settings.Default.FtpUsername, Properties.Settings.Default.FtpPasswd);

                var client = new FtpClient(credential);

                return
                    client.GetFileInfo(new Uri(filePath));
            }
            catch (Exception ex)
            {
                Config.IPS_Config.CaptureException(ex);
                return
                    default(FtpFileInfo);
            }

        }

        public static FtpFileInfo GetFileInfo(VAP_Model.VAP_FILE vap_FtpFile)
        {
            var file = default(FlagFtp.FtpFileInfo);

            try
            {
                var fileInfo_FilterByRuta = GetFilesList(vap_FtpFile.ARCHIVORUTA);

                var fileInfo_FilterByNameAndPeriod = fileInfo_FilterByRuta.Select(o => o).Where(o => o.Name.Contains(vap_FtpFile.ARCHIVONOMBRE)).ToList();

                var hasError = false;

                if (fileInfo_FilterByNameAndPeriod.Count > 1)
                    hasError = true;

                else if (fileInfo_FilterByNameAndPeriod.Count == 0)
                    hasError = true;

                else if (fileInfo_FilterByNameAndPeriod.Count == 1)
                {
                    file = fileInfo_FilterByNameAndPeriod.FirstOrDefault();

                    if (file.Length < 5 && IsFileEmpty(file.FullName) || Path.GetExtension(file.FullName).ToUpper() != vap_FtpFile.EXTENSION)
                        hasError = true;
                }

                else if (fileInfo_FilterByRuta.Count > 0)
                    hasError = true;

                if (hasError)
                    file = default(FlagFtp.FtpFileInfo);
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                file = default(FlagFtp.FtpFileInfo);
            }

            return
                file;
        }

        public static Dictionary<FtpFileInfo, bool> GetFilesInfo(VAP_Model.VAP_FILE vap_FtpFile)
        {
            Dictionary<FtpFileInfo, bool> fileDictionary = new Dictionary<FtpFileInfo, bool>();

            try
            {
                var fileInfo_FilterByRuta = GetFilesList(vap_FtpFile.ARCHIVORUTA);

                var fileInfo_FilterByNameAndPeriod = fileInfo_FilterByRuta.Select(o => o).Where(o => o.Name.Contains(vap_FtpFile.ARCHIVONOMBRE)).ToList();


                foreach (FtpFileInfo file in fileInfo_FilterByNameAndPeriod)
                {
                    if (file.Length < 5 && IsFileEmpty(file.FullName) || Path.GetExtension(file.FullName).ToUpper() != vap_FtpFile.EXTENSION)
                        fileDictionary.Add(file, false);
                    else
                        fileDictionary.Add(file, true);
                }

                return
                    fileDictionary;
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);

                return null;
            }

           
        }

        public static List<string> GetFileNames(string _directory)
        {
            try
            {
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(new Uri(_directory));
                ftpRequest.Credentials = new NetworkCredential(Properties.Settings.Default.FtpUsername, Properties.Settings.Default.FtpPasswd);
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                StreamReader streamReader = new StreamReader(response.GetResponseStream());

                List<string> directories = new List<string>();

                string line = streamReader.ReadLine();
                while (!string.IsNullOrEmpty(line))
                {
                    directories.Add(line);
                    line = streamReader.ReadLine();
                }

                streamReader.Close();

                return
                    directories;
            }
            catch (Exception ex)
            {
                Config.IPS_Config.CaptureException(ex);
                return
                    new List<string>();
            }

        }

        public bool EmptyFolder(string folderPath= null)
        {
            bool folderWasEmpty = true;
            try
            {
                Uri uriDestination = new Uri(folderPath, UriKind.Absolute);
                var credential = new NetworkCredential(Properties.Settings.Default.FtpUsername, Properties.Settings.Default.FtpPasswd);

                var client = new FtpClient(credential);
                List<FtpFileInfo> allFiles = client.GetFiles(uriDestination).ToList();
                foreach (var file in allFiles)
                {
                    Console.WriteLine("Borrando: " + file.Name + "\n");
                    client.DeleteFile(file);
                }
                folderWasEmpty = true;
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
                folderWasEmpty = false;
            }

            return folderWasEmpty;
        }

        public static bool IsFileEmpty(string filePath)
        {
            var linesFillCant = 0;

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(filePath);

                request.Proxy = null;

                request.Method = WebRequestMethods.Ftp.DownloadFile;

                request.Credentials = new NetworkCredential(Properties.Settings.Default.FtpUsername, Properties.Settings.Default.FtpPasswd);

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                Stream responseStream = response.GetResponseStream();


                using (StreamReader reader = new StreamReader(responseStream))
                {
                    while (!string.IsNullOrEmpty(reader.ReadLine()))
                    {
                        linesFillCant += 1;

                        break;

                    }
                }

                responseStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }

            return
                linesFillCant == 0;
        }
    }
}
