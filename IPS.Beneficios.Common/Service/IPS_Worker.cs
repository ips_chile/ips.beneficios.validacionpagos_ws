﻿using System;
using System.IO;
using System.Globalization;
using log4net;
using System.Threading;

namespace IPS.Beneficios.Common.Service
{
    public abstract class IPS_Worker : IDisposable
    {
        public bool _threadShutdown = false;
        protected ManualResetEvent _doneEvent;
        public short macroProcID;
        public string workerJobName;
        protected string workerName = "IPS_WORKER";
        public string workerProfileLocation = "C:\\IPS_WORKER";

        public string localFilename = null;
        public string localFilenameExtension = null;

        protected bool isFileDownloaded = false;
        //protected Repository.LOADER_MACROPROCESO_TASK _repositoryMacroProc;
        //protected Repository.LOADER_PROC_PERIOD _repositoryProcPeriod;
        //protected Repository.LOADER_PROC_PERIOD_MIXED _repositoryMixedProcPeriod;
        //protected Repository.LOADER_MACROPROCESO_DEADLINE _repositoryDeadLine;
        //protected Repository.LOADER_PARAMETROS _repositoryLoaderParametros;
        protected Repository.EMAIL _repositoryEmail;
        //protected Model.LOADER_MACROPROCESO_TASK _macroProcData;
        //protected Model.LOADER_MACROPROCESO_DEADLINE _macroDeadLine;
        protected static readonly ILog log = LogManager.GetLogger(typeof(IPS_Worker));


        public string period;
        public string today;
        public DateTime _runTimeDate;
        public IPS_Worker()
        {
            //this.SetWorkerPeriod();
            //_repositoryMacroProc = new Repository.LOADER_MACROPROCESO_TASK();
            //_repositoryProcPeriod = new Repository.LOADER_PROC_PERIOD();
            //_repositoryMixedProcPeriod = new Repository.LOADER_PROC_PERIOD_MIXED();
            //_repositoryDeadLine = new Repository.LOADER_MACROPROCESO_DEADLINE();
            //_repositoryLoaderParametros = new Repository.LOADER_PARAMETROS();
            _repositoryEmail = new Repository.EMAIL();
            Config.IPS_Config.InicializaLog();
        }

        //public void SetWorkerPeriod(string periodo = null, bool forceLastDayOfMonth = true)
        //{
        //    int year, month, day, hour, min, sec;
        //    if(periodo != null)
        //    {
        //        DateTime date = DateTime.Now;
        //        hour = date.Hour;
        //        min = date.Minute;
        //        sec = date.Second;
        //        day = date.Day;
        //        CultureInfo provider = CultureInfo.InvariantCulture;
        //        date = DateTime.ParseExact(periodo, "yyyyMM", provider);
                
        //        this.period = periodo;
        //        year = date.Year;
        //        month = date.Month;
        //        today = date.ToString("yyyy_MM_") + DateTime.DaysInMonth(date.Year, date.Month).ToString();
        //        day = (forceLastDayOfMonth) ? DateTime.DaysInMonth(date.Year, date.Month) : day;
                
        //    }
        //    else{
        //        this.period = DateTime.Now.ToString("yyyyMM");
        //        this.today = DateTime.Now.ToString("yyyy_MM_d");
        //        year = DateTime.Now.Year;
        //        month = DateTime.Now.Month;
        //        hour = DateTime.Now.Hour;
        //        min = DateTime.Now.Minute;
        //        sec = DateTime.Now.Second;
        //        day = DateTime.Now.Day;
        //    }
        //    _runTimeDate = new DateTime(year, month, day, hour, min, sec);
        //}

        public void SetWorkerJobName(string jobName = null)
        {
            if (jobName != null)
            {
                this.workerJobName = jobName;
            }
        }

        public void SetWorkerJobName()
        {
            this.workerJobName = this.GetType().Name.Replace("IPS_Worker_","");
        }

        public void SetWorkerInputFileName(string fileExtension = null)
        {
            if (fileExtension != null)
            {
                this.localFilenameExtension = fileExtension;
                this.localFilename = workerProfileLocation + "\\Downloads\\" + today + "_" + workerJobName + "." + fileExtension;
            }
        }

        public abstract void InitWork(object sender, System.ComponentModel.DoWorkEventArgs e);
       
        public bool checkFileExists(string fileNameToCheck = null)
        {
            if (fileNameToCheck == null)
            {
                fileNameToCheck = localFilename;
            }
            if (File.Exists(localFilename))
            {
                isFileDownloaded = true;
            }
            return isFileDownloaded;
        }

        //public bool MacroProcBegin()
        //{
        //    return _repositoryMacroProc.ChangeMacroprocesoEstado(this._macroProcData.LOADER_MACROPROCESO_ID, period, (int)Common.Config.IPS_Config.MacroProcesoEstado.VERIFICANDO);
        //}

        //public bool MacroProcEnd()
        //{
        //    return _repositoryMacroProc.ChangeMacroprocesoEstado(this._macroProcData.LOADER_MACROPROCESO_ID, period, (int)Common.Config.IPS_Config.MacroProcesoEstado.CARGADO);
        //}

        //public bool MacroProcWarning()
        //{
        //    return _repositoryMacroProc.ChangeMacroprocesoEstado(this._macroProcData.LOADER_MACROPROCESO_ID, period, (int)Common.Config.IPS_Config.MacroProcesoEstado.WARNING);
        //}

        //public bool MacroProcError()
        //{
        //    return _repositoryMacroProc.ChangeMacroprocesoEstado(this._macroProcData.LOADER_MACROPROCESO_ID, period, (int)Common.Config.IPS_Config.MacroProcesoEstado.ERROR);
        //}

        //public bool MacroProcNoDisponible()
        //{
        //    return _repositoryMacroProc.ChangeMacroprocesoEstado(this._macroProcData.LOADER_MACROPROCESO_ID, period, (int)Common.Config.IPS_Config.MacroProcesoEstado.NO_DISPONIBLE, false);
        //}

        //public void SetMacroProcData(Model.LOADER_MACROPROCESO_TASK macroProc)
        //{
        //    string logMsg = "Seteando datos macropoceso ";
        //    if (macroProc != null)
        //    {
        //        logMsg += String.Format("ID: {0}, ", macroProc.LOADER_MACROPROCESO_ID);
        //    }
        //    log.Info(logMsg);
        //    _macroProcData = macroProc;
        //}

        //public void SetMacroProcData(Model.LOADER_MACROPROCESO_DEADLINE macroDeadLine)
        //{
        //    _macroDeadLine = macroDeadLine;
        //}

        //public virtual bool workerCanRun()
        //{
        //    var macroProcesoConfig = this._macroProcData;

        //    var fechaProg = Convert.ToDateTime(macroProcesoConfig.LOADER_MACROPROCESO_FECHA_PROG).ToString("yyyy_MM_d");
        //    fechaProg = (fechaProg.Contains("0001_01_1")) ? null : fechaProg;
        //    var estadoProc = macroProcesoConfig.LOADER_MACROPROCESO_ESTADO_ID;

        //    if ((macroProcesoConfig != null) && (fechaProg == null || fechaProg.Equals(today)) && macroProcesoConfig.LOADER_MACROPROCESO_CAN_RUN.Equals(1) && !estadoProc.Equals((int)Config.IPS_Config.MacroProcesoEstado.CARGADO) && !estadoProc.Equals((int)Config.IPS_Config.MacroProcesoEstado.VERIFICANDO))
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        public void webs()
        {
            //_repositoryMacroProc.GetAllMacroprocesos(period);
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
            _threadShutdown = true;
        }
    }
}
