﻿using System;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using System.Web;
using Roles = IPS.Beneficios.CommonRoles.Services;
using System.Text.RegularExpressions;

namespace IPS.Beneficios.Common.Repository
{
    public class EMAIL : IPS_Repository
    {
        Roles.LOGIN_USUARIO_MAIL _usuario_mail_service = null;

        public string _smtpServer_MailFrom = null;

        public string _smtpServer_Pswd = null;

        public bool _smtpSend_MailTo_RealUsers = default(bool);


        public EMAIL() : base(true) 
        {
            _usuario_mail_service = new Roles.LOGIN_USUARIO_MAIL();

            _smtpServer_MailFrom = Common.Helper.Generics.GetCommonConfig("SMTP_MAIL");

            _smtpServer_Pswd = Common.Helper.Generics.GetCommonConfig("SMTP_PSWD");

            _smtpSend_MailTo_RealUsers = Common.Helper.Generics.GetCommonConfig("SEND_MAILS_TO_REAL_USERS").ToUpper().Equals("S");
        }


        public List<string> GetEmailByRol(int login_rol_id)
        {
            var emailList = new List<string>();

            using (OracleConnection connection = new OracleConnection(_connectionString))
            {
                OracleCommand command = new OracleCommand(string.Format(
                    Properties.Resources.ResourceManager.GetString("GetEmailByRol")
                    , login_rol_id)
                    , connection);


                OracleDataReader readerOra = null;

                try
                {
                    connection.Open();

                    readerOra = command.ExecuteReader();

                    while (readerOra.Read())
                    {
                        emailList.Add(readerOra["LOGIN_USUARIO_MAIL_NAME"] is DBNull ? string.Empty : (string)readerOra["LOGIN_USUARIO_MAIL_NAME"]);
                    }
                }
                finally
                {
                    readerOra.Close();
                }
            }

            return

                emailList;
        }

        public string GetEmailByUserID(int userID)
        {
            var email = string.Empty;

            using (OracleConnection connection = new OracleConnection(_connectionString))
            {
                OracleCommand command = new OracleCommand(string.Format(
                    Properties.Resources.ResourceManager.GetString("GetEmailByUserID")
                    , userID)
                    , connection);


                OracleDataReader readerOra = null;

                try
                {
                    connection.Open();

                    readerOra = command.ExecuteReader();

                    if (readerOra.Read())
                    {
                        email = readerOra["LOGIN_USUARIO_MAIL_NAME"] is DBNull ? string.Empty : (string)readerOra["LOGIN_USUARIO_MAIL_NAME"];
                    }
                }
                finally
                {
                    readerOra.Close();
                }
            }

            return

                email;
        }

        public void SendEmail(Common.Model.EMAIL email)
        {
            try
            {
                using (MailMessage mm = new MailMessage())
                {
                    mm.From = new MailAddress(_smtpServer_MailFrom);

                    if (_smtpSend_MailTo_RealUsers)
                    {
                        #region //SEND TO
                        if (!string.IsNullOrEmpty(email.EMAIL_LIST))
                        {
                            var emailToArray = email.EMAIL_LIST.Split(',');

                            var emailToWrong = new List<string>();

                            foreach (var emailTo in emailToArray)
                            {
                                if (IsValidMail(emailTo))
                                    mm.To.Add(emailTo);
                                else
                                    emailToWrong.Add(emailTo);
                            }

                            if (emailToWrong.Count > 0)
                                #region //LOG REGISTRY
                                Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                                {
                                    PROCESO_NAME = "SendEmail()"
                                    ,
                                    API_MESSAGE = string.Format(" Email: ({0})  [Contiene destinatarios (TO) No validos]: ({1})", email.SUBJECT, string.Join(",", emailToWrong))
                                    ,
                                    TIPO_OPERACION = Common.Model.EnumLog_Operation.WARNING_API
                                });
                                #endregion
                        }
                        else
                            #region //LOG REGISTRY
                            Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                            {
                                PROCESO_NAME = "SendEmail()"
                                ,
                                API_MESSAGE = string.Format(" Email: ({0})  [No contiene destinatarios (TO) para enviar]", email.SUBJECT)
                                ,
                                TIPO_OPERACION = Common.Model.EnumLog_Operation.WARNING_API
                            });
                            #endregion
                        #endregion

                        #region //SEND CC
                        if (!string.IsNullOrEmpty(email.EMAIL_LIST_CC))
                        {
                            var emailCcArray = email.EMAIL_LIST_CC.Split(',');

                            var emailCcWrong = new List<string>();

                            foreach (var emailCc in emailCcArray)
                            {
                                if (IsValidMail(emailCc))
                                    mm.CC.Add(emailCc);
                                else
                                    emailCcWrong.Add(emailCc);
                            }

                            if (emailCcWrong.Count > 0)
                                #region //LOG REGISTRY
                                Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                                {
                                    PROCESO_NAME = "SendEmail()"
                                    ,
                                    API_MESSAGE = string.Format(" Email: ({0})  [Contiene destinatarios (CC) No validos]: ({1})", email.SUBJECT, string.Join(",", emailCcWrong))
                                    ,
                                    TIPO_OPERACION = Common.Model.EnumLog_Operation.WARNING_API
                                });
                                #endregion
                        }
                        #endregion

                        #region //SEND BCC
                        if (!string.IsNullOrEmpty(email.EMAIL_LIST_BCC))
                        {
                            var emailBccArray = email.EMAIL_LIST_BCC.Split(',');

                            var emailBccWrong = new List<string>();

                            foreach (var emailBcc in emailBccArray)
                            {
                                if (IsValidMail(emailBcc))
                                    mm.Bcc.Add(emailBcc);
                                else
                                    emailBccWrong.Add(emailBcc);
                            }

                            if (emailBccWrong.Count > 0)
                                #region //LOG REGISTRY
                                Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                                {
                                    PROCESO_NAME = "SendEmail()"
                                    ,
                                    API_MESSAGE = string.Format(" Email: ({0})  [Contiene destinatarios (BCC) No validos]: ({1})", email.SUBJECT, string.Join(",", emailBccWrong))
                                    ,
                                    TIPO_OPERACION = Common.Model.EnumLog_Operation.WARNING_API
                                });
                                #endregion
                        }
                        //else
                        //    #region //LOG REGISTRY
                        //    Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                        //    {
                        //        PROCESO_NAME = "SendEmail()"
                        //        ,
                        //        API_MESSAGE = string.Format(" Email: ({0})  [No contiene destinatarios (BCC) para enviar]", email.SUBJECT)
                        //        ,
                        //        TIPO_OPERACION = Common.Model.EnumLog_Operation.WARNING_API
                        //    });
                        //    #endregion
                        #endregion
                    }
                    else
                    {
                        #region //SEND DEBUG MODE
                        var admin_Debugmails = _usuario_mail_service.Get_PlainText((int)Common.Config.IPS_Config.Roles.ADM, true);

                        if (!string.IsNullOrEmpty(admin_Debugmails))
                        {
                            var emailToArray = admin_Debugmails.Split(',');

                            var emailToWrong = new List<string>();

                            foreach (var emailTo in emailToArray)
                            {
                                if (IsValidMail(emailTo))
                                    mm.To.Add(emailTo);
                                else
                                    emailToWrong.Add(emailTo);
                            }

                            if (emailToWrong.Count > 0)
                                #region //LOG REGISTRY
                                Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                                {
                                    PROCESO_NAME = "SendEmail()"
                                    ,
                                    API_MESSAGE = string.Format(" Email: ({0})  [Contiene destinatarios (TO) Modo Debug No validos]: ({1})", email.SUBJECT, string.Join(",", emailToWrong))
                                    ,
                                    TIPO_OPERACION = Common.Model.EnumLog_Operation.WARNING_API
                                });
                                #endregion
                        }
                        else
                            #region //LOG REGISTRY
                            Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                            {
                                PROCESO_NAME = "SendEmail()"
                                ,
                                API_MESSAGE = string.Format(" Email: ({0})  [No contiene destinatarios (TO) Modo Debug para enviar]", email.SUBJECT)
                                ,
                                TIPO_OPERACION = Common.Model.EnumLog_Operation.WARNING_API
                            });
                            #endregion
                        #endregion
                    }



                    if (!string.IsNullOrEmpty(email.EMAIL_LIST) || !string.IsNullOrEmpty(email.EMAIL_LIST_CC) || !string.IsNullOrEmpty(email.EMAIL_LIST_BCC))
                    {
                        if (email.FILESPATH != null && email.FILESPATH.Count > 0)
                            foreach (var file in email.FILESPATH)
                            {
                                mm.Attachments.Add(new Attachment(file, MimeMapping.GetMimeMapping(file)));
                            }

                        mm.From = new MailAddress(_smtpServer_MailFrom, "noreply IPS", System.Text.Encoding.UTF8);
                        mm.IsBodyHtml = true;
                        mm.Subject = email.SUBJECT;
                        mm.Body = email.BODY;
                        mm.IsBodyHtml = true;
                        mm.Priority = MailPriority.High;

                        SmtpClient smtp = new SmtpClient();
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential(_smtpServer_MailFrom, _smtpServer_Pswd);
                        smtp.Port = 587; // You can use Port 25 if 587 is blocked (mine is!)
                        smtp.Host = "smtp.office365.com";
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.EnableSsl = true;
                        //smtp.Send(mm);
                        mm.Dispose();

                        #region //LOG REGISTRY
                        Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                        {
                            API_MESSAGE = email.SUBJECT
                            ,
                            TIPO_OPERACION = Common.Model.EnumLog_Operation.EMAIL
                        });
                        #endregion
                    }
                    else
                        #region //LOG REGISTRY
                        Common.Helper.Log_App.AddLogRegistry(new Common.Model.LOG_APP()
                        {
                            PROCESO_NAME = "SendEmail()"
                            ,
                            API_MESSAGE = string.Format(" Email: ({0})  [No Contiene destinatarios (TO, CC o BCC) para enviar])", email.SUBJECT)
                            ,
                            TIPO_OPERACION = Common.Model.EnumLog_Operation.WARNING_API
                        });
                        #endregion
                }
            }
            catch (Exception ex)
            {
                Common.Config.IPS_Config.CaptureException(ex);
            }

        }

        private bool IsValidMail(string emailAddress)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            Match match = regex.Match(emailAddress);

            return
                match.Success;
        }


    }
}
