﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Reflection;
using System.Security.Principal;
using System.Dynamic;

namespace IPS.Beneficios.Common.Repository
{
    public abstract class IPS_Repository
    {
        protected string _connectionString;
        protected string _classModelName;

        protected string GetConnectionString()
        {
            string _connectionString = Properties.Settings.Default.ConnectionStringAppOraDesab;

            return
                _connectionString;
        }


        protected void SetClassModel()
        {
            Model.IPS_Model modelObj = (Model.IPS_Model)Activator.CreateInstance(Type.GetType(this.GetType().Namespace.Replace("Repository", "Model.") + this.GetType().Name), new object[] { });
            _classModelName = modelObj.GetTableName();
        }

        public IPS_Repository(bool _setClassModel = true)
        {
            _connectionString = this.GetConnectionString();

            if (_setClassModel)
                SetClassModel();
        }
    }
}
