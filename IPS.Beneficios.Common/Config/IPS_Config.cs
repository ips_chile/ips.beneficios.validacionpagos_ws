﻿using SharpRaven;
using System;
using System.Collections.Generic;
using System.Linq;
using log4net.Config;
using log4net.Appender;
using log4net;
using System.IO;
using System.ComponentModel;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace IPS.Beneficios.Common.Config
{
    public static class IPS_Config
    {
        public static bool isProd;
        public static bool DEBUG;
        private static RavenClient ravenClient;
        public static string _adminReplyToEmails;
        public static string mailFooterMsg;
        public static readonly ILog log = LogManager.GetLogger(typeof(IPS_Config));
        public static readonly string _ftpUsername = Properties.Settings.Default.FtpUsername;
        public static readonly string _ftpPassword = Properties.Settings.Default.FtpPasswd;
        public static readonly string _ftpPath = Properties.Settings.Default.FtpUrlToConnect;
        public static readonly string installDirApp = GetInstallDirApp();
        public static readonly string archivosCargaFolder = string.Format("{0}DOWNLOADS\\ARCHIVOS_CARGA_PORMES", installDirApp);
        public static readonly string desagragacionMesFolder = string.Format("{0}DOWNLOADS\\CAUSANTES_PORMES", installDirApp);
        public static readonly string calculoIngresoPromedioMesFolder = string.Format("{0}DOWNLOADS\\INGRESO_PROMEDIO_PORMES", installDirApp);
        public static readonly string distribucionPagosMesFolder = string.Format("{0}DOWNLOADS\\DISTRIBUCION_PAGOS_PORMES", installDirApp);
        public static readonly string solicitudesProcesadasMesFolder = string.Format("{0}DOWNLOADS\\SOLICITUDES_PROCESADAS_PORMES", installDirApp);
        public static readonly string mayoresMenoresFolder = string.Format("{0}DOWNLOADS\\MAYORESyMENORES", installDirApp);
        public static readonly Int32 _maxValueMBToFillFile = 1000;
        //public static readonly Int32 _maxCantToFillCSV = 100000;//No se usa

        static IPS_Config()
        {
            mailFooterMsg = "<br/><br/><br/><br/> Saludos,<br/>Unidad Operaciones Especiales y Procesos Masivos.";
            _adminReplyToEmails = Properties.Resources.AdminReplyToEmails;
            isProd = true;
            DEBUG = false;
            ravenClient = new RavenClient("https://2afe593f3e6447e1b30800ea9ce1be15:e7a398c55e4448d58ed69ecfeb6e0d19@app.getsentry.com/32094");
        }

        public static void CaptureException(Exception e)
        {
            ravenClient.CaptureException(e);

            Common.Helper.Log_App.WriteErrorLog_E(string.Format("Msg:{0}  Pila:{1}", e.Message, e.StackTrace));
        }

        #region //Enums
        public enum ConnString { MYSQL_DESAB = 1, ORA_DESAB = 2, ORA_PROD = 3, MYSQL_PROD_AFAMILIA_PROCESOS = 4, MYSQL_PROD_ARCHIVOS22 = 5 };

        public enum Roles { ELEGIBILIDAD_TO = 26, ELEGIBILIDAD_CC = 27, ADM = 13 };//CONFIGURACION PARA EL ACCESS MANAGER, MODIFICAR DESPUES.

        public enum LogReports { ELEGIBILIDAD = 1, POTENCIAL_BENEFICIOS = 2 };//ELIMINAR DEPENDENCIAS DESPUES...



        public enum Workers { LoadCausantesBase = 1, LoadSolicitudesBase = 2, LoadCausantesMes = 3, CalculoIngresoPromedio = 4, DistribucionPagos = 5, SolicitudesProcesadas = 6, GeneraArchivoMenoresMayores = 7, CheckPeriodosProcesos = 8 };

        public enum PeriodoEstado { LISTO_PARAPROCESAR = 1, PENDIENTE = 2, EN_EJECUCUION = 3, TERMINADO = 4 };


        public enum ProcesoEstado { EN_ESPERA_PROCESAMIENTO = 1, PROCESANDO = 2, REPROCESAR = 3, PAUSADO = 4, VERIFICANDO_ARCHIVOS = 5,  TERMINADO_ERROR = 6, TERMINADO_SUCCESS = 7, ENESPERA_ARCHIVOPERIODO = 8 };

        public enum ProcesoID { LOAD_CAUSANTES_BASE = 1, LOAD_CAUSANTES_MES = 2, LOAD_SOLICITUDES_BASE = 3, LOAD_SOLICITUDES_PROCESADAS = 4, DISTRIBUCION_PAGOS = 5, VERIFICACION_PAGO_SOLICITUDES = 6, VERIFICACION_PAGO_CONSOLIDADO = 7, CALCULO_INGRESO_PROMEDIO = 8, GENERA_ARCHIVO_MENORESMAYORES = 9 };

        public enum ParametroID { FECHA_RECONOCIMIENTO = 1, TIPO_CAUSANTE_NORMAL = 2, TIPO_CAUSANTE_ESTUDIANTE = 3, FILTRO_EDAD_CAUSANTE_NORMAL = 4, FILTRO_EDAD_CAUSANTE_ESTUDIANTE = 5, TIPO_CAUSANTE_DUPLO = 6, AÑO_DESDE_VIGENCIA_INGRESO_PROMEDIO = 7, DIAS_FORMULA_CALCULO_PROPORCIONAL = 8, MESES_NOPROCESAMIENTO_ARCHIVO_MENORESMAYORES = 9, PERIODO_INICIO_SOLICITUDES_INGRESADAS = 10 };

        public enum NotificaionID { CAUSANTE_MENOR18 = 1, CAUSANTE_18Y24 = 2, CAUSANTE_VIGENTE = 3, CAUSANTE_EXTINGUIDO = 4, SIN_INGRESO_PROMEDIO = 5, SIN_TRAMO = 6, CAUSANTE_PAGO_PENDIENTE_CON_RELACION_LABORAL = 7, CAUSANTE_PAGADO_EXITOSAMENTE = 8, TRABAJBENEF_DIFERENCIA_MONTOS = 9, TRABAJBENEF_MONTOS_IGUALES = 10, CAUSANTE_NO_CONSIDERADO_EN_PERIODO = 11, CAUSANTE_CONSIDERADO_EN_PERIODO = 12, TRABAJADOR_PERIODO_NOEXISTE_EN_PLANILLA = 13, CAUSANTE_DUPLICADO_ENPERIODO = 14, CAUSANTE_UNICO_ENPERIODO = 15, TRABAJADOR_NOTIENE_CAUSANTES_ENPERIODO = 16, CAUSANTE_PAGO_PENDIENTE_SIN_RELACION_LABORAL = 17,
        GROUP_NOTIFICACION_12_7 = 18, GROUP_NOTIFICACION_12_17 = 19, GROUP_NOTIFICACION_11_7 = 20, GROUP_NOTIFICACION_11_8 = 21, GROUP_NOTIFICACION_11_17 = 22 };

        public enum RulePriorizationToUse { MAYOR_PERMANENCIA = 1, FECHA_INSERT = 2, MENOR_RUT = 3 };

        public enum TipoReconocido { VIGENTE = 'V', EXTINGUIDO = 'E' };

        public enum SQLLDRPath { PROD = 1, TEST = 2 };

        public enum SQLLDRUser { PROD = 1, TEST = 2 };

        #endregion


        public static void InitializeEventViewerLog()
        {
            Common.Helper.Log_App.eventLogSimple = new System.Diagnostics.EventLog();
            Common.Helper.Log_App.eventLogSimple.Source = ServiceDisplayName();
            Common.Helper.Log_App.eventLogSimple.Log = ServiceDisplayName();

            ((ISupportInitialize)(Common.Helper.Log_App.eventLogSimple)).BeginInit();
            if (!System.Diagnostics.EventLog.SourceExists(Common.Helper.Log_App.eventLogSimple.Source))
            {
                System.Diagnostics.EventLog.CreateEventSource(Common.Helper.Log_App.eventLogSimple.Source, Common.Helper.Log_App.eventLogSimple.Log);
            }
            ((ISupportInitialize)(Common.Helper.Log_App.eventLogSimple)).EndInit();
        }

        public static void InicializaLog(string suffix = "")
        {
            string logPath = GetInstallDirApp() + String.Format("APILOG\\{0}.log", ServiceDisplayName());

            if (!File.Exists(logPath))
            {
                Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();
                hierarchy.Root.RemoveAllAppenders(); /*Remove any other appenders*/

                FileAppender fileAppender = new FileAppender();
                fileAppender.AppendToFile = true;
                fileAppender.LockingModel = new FileAppender.MinimalLock();
                fileAppender.File = logPath;
                PatternLayout pl = new PatternLayout();
                pl.ConversionPattern = "%date %-5level [%thread] %logger - %message%newline";
                pl.ActivateOptions();
                fileAppender.Layout = pl;
                fileAppender.ActivateOptions();

                log4net.Config.BasicConfigurator.Configure(fileAppender);
            }
        }

        public static string GetInstallDirApp()
        {
            return ProgramFilesx86() + "\\IPS\\" + ServiceDisplayName() + "\\";
        }
        static string ServiceDisplayName()
        {
            return "SrvValidacionDAF";//CAMBIAR DESPUES...
        }
        public static string ProgramFilesx86()
        {
            if (8 == IntPtr.Size
                || (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
            {
                return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
            }

            return Environment.GetEnvironmentVariable("ProgramFiles");
        }
    }
}