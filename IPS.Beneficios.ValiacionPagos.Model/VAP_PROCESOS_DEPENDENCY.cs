﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_PROCESOS_DEPENDENCY
    {
        public int PROCESO_ID { get; set; }
        public int PROCESO_ID_DEPENDENCY { get; set; }
        public int PROCESO_ORDER { get; set; }
        public char ISACTIVE { get; set; }
    }
}
