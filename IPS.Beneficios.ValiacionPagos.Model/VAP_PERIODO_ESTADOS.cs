﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_PERIODO_ESTADOS
    {
        public int ESTADO_ID { get; set; }
        public string ESTADO_GLOSA { get; set; }
    }
}
