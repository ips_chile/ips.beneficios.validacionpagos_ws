﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_NOTIFICACION
    {
        public Int32 NOTIFICAION_ID { get; set; }

        public string NOTIFICACION_GLOSA { get; set; }
    }

}
