﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_TRAMOS
    {
        public Int32 PERIODO { get; set; }

        public Int32 PERIODOD { get; set; }

        public Int32 PERIODOH { get; set; }

        public Int64 ING_DESDE { get; set; }

        public Int64 ING_HASTA { get; set; }

        public int TRAMOA { get; set; }

        public Int32 VALOR_ASIGSIMPLE { get; set; }

        public Int32 VALOR_ASIGDUPLO { get; set; }

    }
}
