﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_PARAMETROS
    {
        public int PARAMETRO_ID { get; set; }
        public string PARAMETRO_GLOSA { get; set; }
        public string PARAMETRO_VALOR { get; set; }
        public string PARAMETRO_CONSTRAIN { get; set; }
    }
}
