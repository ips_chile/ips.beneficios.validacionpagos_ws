﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public partial class VAP_LOG_BASE
    {




        public Int32 RUT_BENEFICIARIO { get; set; }






        public string DV_BENEFICIARIO { get; set; }






        public Int32 RUT_CAUSANTE { get; set; }






        public string DV_CAUSANTE { get; set; }






        public Int32 CAUSANTE_PERIODO { get; set; }






        public string ARCHIVO_IDCARGA { get; set; }






        public DateTime FECHA_INCORPORACION { get; set; }






        public Int32 INCORPORACION_MOTIVO_ID { get; set; }






        public Int32 ESTADO_PROCESAMIENTO { get; set; }






        public string TIPO_CAUSANTE { get; set; }



    }

}
