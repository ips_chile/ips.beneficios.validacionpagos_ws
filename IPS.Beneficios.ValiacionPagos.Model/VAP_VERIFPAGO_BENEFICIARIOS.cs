﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_VERIFPAGO_BENEFICIARIOS
    {
        public long FOLIO_SOLICITUD { get; set; }

        public Int32 RUT_EMPLEADOR { get; set; }

        public string DV_EMPLEADOR { get; set; }

        public Int32 RUT_BENEFICIARIO { get; set; }

        public string DV_BENEFICIARIO { get; set; }

        public long VALORAF_DEFAULT { get; set; }

        public long VALORAF_PAGADO { get; set; }

        public long VALORAF_PENDIENTE { get; set; }
        
        public int BENEFICIARIO_PERIODO { get; set; }

        public int CAUSANTES_PENDIENTEPAGO { get; set; }

        public int NOTIFICACION_ID { get; set; }

        public DateTime SISTEMA_FEC_PROCESO { get; set; }

        public string ARCHIVO_IDCARGA { get; set; }

        public int PERIODO_PROCESO { get; set; }

    }
}
