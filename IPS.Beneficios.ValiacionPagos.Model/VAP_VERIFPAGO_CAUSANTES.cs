﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_VERIFPAGO_CAUSANTES
    {
        public long FOLIO_SOLICITUD { get; set; }

        public Int32 RUT_EMPLEADOR { get; set; }

        public string DV_EMPLEADOR { get; set; }

        public Int32 RUT_BENEFICIARIO { get; set; }

        public string DV_BENEFICIARIO { get; set; }

        public Int32 RUT_CAUSANTE { get; set; }

        public string DV_CAUSANTE { get; set; }

        public string CAUSANTE_PENDIENTEPAGO { get; set; }

        public int CAUSANTE_PERIODO { get; set; }

        public long VALORAF_DEFAULT { get; set; }

        public long VALOR_ARCHIVO12 { get; set; }

        public long RETROACTIVO_ACUMULADO { get; set; }

        public long VALORAF_PAGADO { get; set; }

        public long VALORAF_PENDIENTE { get; set; }

        public DateTime FECHA_INSERT { get; set; }

        public DateTime CAUSANTE_FEC_REC { get; set; }

        public DateTime CAUSANTE_FEC_EXT_CALC { get; set; }

        public int CAUSANTE_DIAS_SIAGF { get; set; }

        public int CAUSANTE_DIAS_SOLICITADOS { get; set; }

        public int CAUSANTE_DIAS_TRABAJADOS { get; set; }

        public int DIAS_A_PROCESAR { get; set; }

        public int NOTIFICACION_ID { get; set; }

        public int TRAMO { get; set; }

        public DateTime SISTEMA_FEC_PROCESO { get; set; }

        public string ARCHIVO_IDCARGA { get; set; }

        public int PERIODO_PROCESO { get; set; }
    }
}
