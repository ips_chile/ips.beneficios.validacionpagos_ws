﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_PERIODOS
    {
        public int PERIODO_ID { get; set; }
        public Int32 PERIODO_DISPLAY { get; set; }
        public int ESTADO_ID { get; set; }
        public char PERIODO_ISACTIVE  { get; set; }

    }
}
