﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class BLL_INGRESO_PROMEDIO
    {
        public Int32 RUT_TRABAJADOR { get; set; }

        public string DV_TRABAJADOR { get; set; }

        public long INGRESO_PROM { get; set; }
    }
}
