﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO
    {
        public long RUT_BENEFICIARIO { get; set; }

        public string DV_BENEFICIARIO { get; set; }

        public long LIQUIDOAPAGAR_AGRUPADO { get; set; }

        public long LIQUIDOAPAGAR_SISTEMA { get; set; }

        public char PAGOCORRECTO { get; set; }

        public DateTime SISTEMA_FEC_PROCESO { get; set; }
    }
}
