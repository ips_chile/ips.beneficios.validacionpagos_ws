﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_PROCESOS
    {
        public int PROCESO_ID { get; set; }
        public string PROCESO_GLOSA { get; set; }
        public char PROCESO_ISACTIVE { get; set; }
    }
}
