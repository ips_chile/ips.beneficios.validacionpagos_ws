﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class BLL_PAGO
    {
        public Int32 RUT { get; set; }

        public string DV { get; set; }

        public int MESREM { get; set; }

        public long MONTO { get; set; }
    }
}
