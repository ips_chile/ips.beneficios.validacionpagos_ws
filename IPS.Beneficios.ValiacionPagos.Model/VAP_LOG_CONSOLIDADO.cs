﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public partial class VAP_LOG_CONSOLIDADO
    {




        public Int32 TUPLA_ID { get; set; }






        public Int32 RUT_BENEFICIARIO_TRABAJADOR { get; set; }






        public string DV_BENEFICIARIO_TRABAJADOR { get; set; }






        public Int32 RUT_CAUSANTE { get; set; }






        public string DV_CAUSANTE { get; set; }






        public Int32 PERIODO { get; set; }






        public Int32 MONTO { get; set; }






        public Int32 INCORPORACION_MOTIVO_ID { get; set; }






        public Int32 TIPO_CAUSANTE { get; set; }






        public DateTime FECHA_INCORPORACION { get; set; }



    }

}
