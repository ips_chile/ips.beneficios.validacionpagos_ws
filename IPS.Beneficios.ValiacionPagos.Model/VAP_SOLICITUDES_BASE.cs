﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public partial class VAP_SOLICITUDES_BASE
    {

        public long FOLIO { get; set; }

        public DateTime FECHA_DECLARACION_CARGAS { get; set; }

        public Int32 RUT_EMPLEADOR { get; set; }

        public string DV_EMPLEADOR { get; set; }

        public string RAZSOC_EMPLEADOR { get; set; }

        public string DIRECCION_EMPLEADOR { get; set; }

        public string EMAIL_EMPLEADOR { get; set; }

        public string COMUNA_EMPLEADOR { get; set; }

        public string CIUDAD_EMPLEADOR { get; set; }

        public string REGION_EMPLEADOR { get; set; }

        public Int32 RUT_TRABAJADOR { get; set; }

        public string DV_TRABAJADOR { get; set; }

        public string A_PATERNO_TRABAJADOR { get; set; }

        public string A_MATERNO_TRABAJADOR { get; set; }

        public string NOMBRES_TRABAJADOR { get; set; }

        public Int32 NUMERO_CARGAS { get; set; }

        public string TIPO_CARGA { get; set; }

        public DateTime FECHA_INICIO_COMPENSACION { get; set; }

        public DateTime FECHA_FIN_COMPENSACION { get; set; }

        public DateTime FECHA_SISTEMA_PROCESO { get; set; }

        public string ARCHIVO_IDCARGA { get; set; }

        public int PERIODO_PROCESO { get; set; }
        //public Int32 NUMERO_CARGAS_SIAGF { get; set; }

        //public Int32 NUMERO_CARGAS_SIAGF_VALIDAS { get; set; }
    }

}
