﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class SQL_LOADER
    {
        public string CTLPATH { get; set; }
        public string LOGPATH { get; set; }
        public string BADPATH { get; set; }
        public string DISCARDPATH { get; set; }
        public string FILEPATH { get; set; }
        public string TABLE_NAME { get; set; }
        public bool CLEANTABLE { get; set; }
        public string CLEANTABLE_QUERY { get; set; }
        public Dictionary<string, string> DICTIONARY_SETCONTROLFILEVALUES { get; set; }
 
    }
}
