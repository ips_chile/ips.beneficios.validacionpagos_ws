﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_INGRESO_PROMEDIO
    {
        public Int32 RUT_BENEFICIARIO { get; set; }

        public string DV_BENEFICIARIO { get; set; }

        public Int32 RUT_CAUSANTE { get; set; }

        public string DV_CAUSANTE { get; set; }

        public DateTime FECHA_INSERT { get; set; }

        public DateTime CAUSANTE_FEC_REC { get; set; }

        public DateTime CAUSANTE_FEC_EXT_CALC { get; set; }

        public long CAUSANTE_INGPROMEDIO { get; set; }

        public int CAUSANTE_PERIODO { get; set; }
        
        public string CAUSANTE_TIENE_INGPROMEDIO { get; set; }

        public int TRAMO { get; set; }

        public DateTime SISTEMA_FEC_PROCESO { get; set; }

        public int PERIODO_PROCESO { get; set; }

        public string ARCHIVO_IDCARGA { get; set; }
    }
}
