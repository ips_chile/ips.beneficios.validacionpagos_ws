﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public partial class VAP_CAUSANTES_BASE
    {
        public int ID_TUPLA { get; set; }

        public DateTime FECHA_INSERT { get; set; }

        public int RUT_CAUSANTE { get; set; }

        public string DV_CAUSANTE { get; set; }

        public string NOMBRE_CAUSANTE { get; set; }

        public int COD_TIPO_CAUSANTE { get; set; }

        public string NOMBRE_TIPO_CAUSANTE { get; set; }

        public int RUT_BENEFICIARIO { get; set; }

        public string DV_BENEFICIARIO { get; set; }

        public string NOMBRE_BENEFICIARIO { get; set; }

        public int COD_TIPO_BENEFICIARIO { get; set; }

        public string NOMBRE_TIPO_BENEFICIARIO { get; set; }

        public int COD_TIPO_BENEFICIO { get; set; }

        public string NOMBRE_BENEFICIO { get; set; }

        public int COD_ENTIDAD_INTERNO { get; set; }

        public int COD_ENTIDAD_SUSESO { get; set; }

        public string NOMBRE_ENTIDAD_ADMINISTRADORA { get; set; }

        public int COD_TRAMO { get; set; }

        public DateTime FECHA_RECONOCIMIENTO { get; set; }

        public DateTime FECHA_EXTINCION { get; set; }

        public DateTime FECHA_EXTINCION_CALCULADA { get; set; }

        public DateTime FECHA_NACIMIENTO_CAUSANTE { get; set; }

        public int ESTADO { get; set; }

        public int COD_CAUSAL_EXT { get; set; }

        public DateTime FECHA_SISTEMA_PROCESO { get; set; }

        public string TIPO_RECONOCIDO { get; set; }

        public string ARCHIVO_IDCARGA { get; set; }

        public int PERIODO_PROCESO { get; set; }


        #region //METHODS
        public static string GetTableName()
        {
            return
                typeof(VAP_CAUSANTES_BASE).Name;
        }
        #endregion

    }


    /*
     * 
   FECHA_INSERT                   DATE,
   RUT_CAUSANTE                   INTEGER,
   DV_CAUSANTE                    VARCHAR2(1 BYTE),
   NOMBRE_CAUSANTE                VARCHAR2(50 BYTE),
   COD_TIPO_CAUSANTE              INTEGER,
   NOMBRE_TIPO_CAUSANTE           VARCHAR2(100 BYTE),
   RUT_BENEFICIARIO               INTEGER,
   DV_BENEFICIARIO                VARCHAR2(1 BYTE),
   NOMBRE_BENEFICIARIO            VARCHAR2(50 BYTE),
   COD_TIPO_BENEFICIARIO          INTEGER,
   NOMBRE_TIPO_BENEFICIARIO       VARCHAR2(50 BYTE),
   COD_TIPO_BENEFICIO             INTEGER,
   NOMBRE_BENEFICIO               VARCHAR2(50 BYTE),
   COD_ENTIDAD_INTERNO            INTEGER,
   COD_ENTIDAD_SUSESO             INTEGER,
   NOMBRE_ENTIDAD_ADMINISTRADORA  VARCHAR2(100 BYTE),
   COD_TRAMO                      INTEGER,
   FECHA_RECONOCIMIENTO           DATE,
   FECHA_EXTINCION                DATE,
   FECHA_NACIMIENTO_CAUSANTE      DATE,
   ESTADO                         INTEGER,
   COD_CAUSAL_EXT                 INTEGER,
   FECHA_SISTEMA_PROCESO          DATE,
   TIPO_RECONOCIDO                CHAR(1 BYTE),
   ARCHIVO_IDCARGA                VARCHAR2(30 BYTE),
     * */

}
