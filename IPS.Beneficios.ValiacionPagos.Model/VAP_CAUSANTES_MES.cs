﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public partial class VAP_CAUSANTES_MES
    {

        public Int32 RUT_BENEFICIARIO { get; set; }

        public string DV_BENEFICIARIO { get; set; }

        public Int32 RUT_CAUSANTE { get; set; }

        public string DV_CAUSANTE { get; set; }

        public Int32 CAUSANTE_EDAD { get; set; }

        public DateTime FECHA_INSERT { get; set; }

        public DateTime CAUSANTE_FEC_REC { get; set; }

        public DateTime CAUSANTE_FEC_EXT_DEFAULT { get; set; }

        public DateTime CAUSANTE_FEC_EXT_CALC { get; set; }

        public DateTime CAUSANTE_FEC_EXT_EDAD { get; set; }

        public Int32 CAUSANTE_TIPO { get; set; }

        public Int32 CAUSANTE_PERIODO { get; set; }

        public int NOTIFICACION_ID { get; set; }

        public DateTime SISTEMA_FEC_PROCESO { get; set; }

        public Int32 PERIODO_PROCESO { get; set; }

        public string ARCHIVO_IDCARGA { get; set; }
    }

}
