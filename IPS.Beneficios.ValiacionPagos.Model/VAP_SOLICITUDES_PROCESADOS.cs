﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public partial class VAP_SOLICITUDES_PROCESADOS
    {

        public long FOLIO { get; set; }

        public DateTime FECHA_INICIO_COMPENSACION { get; set; }

        public DateTime FECHA_INICIO_CALCULADA { get; set; }

        public DateTime FECHA_FIN_COMPENSACION { get; set; }

        public Int32 RUT_EMPLEADOR { get; set; }

        public string DV_EMPLEADOR { get; set; }

        public Int32 RUT_TRABAJADOR { get; set; }

        public string DV_TRABAJADOR { get; set; }

        public Int32 RUT_CAUSANTE { get; set; }

        public string DV_CAUSANTE { get; set; }

        public int CAUSANTE_EDAD { get; set; }

        public string CAUSANTE_PENDIENTEPAGO { get; set; }

        public DateTime CAUSANTE_FEC_REC { get; set; }

        public DateTime CAUSANTE_FEC_EXT_CALC { get; set; }

        public Int32 CAUSANTE_PERIODO { get; set; }

        public int CAUSANTE_DIAS_SIAGF { get; set; }

        public int CAUSANTE_DIAS_SOLICITADOS { get; set; }

        public int CAUSANTE_DIAS_TRABAJADOS { get; set; }

        public long VALOR_ARCHIVO12 { get; set; }

        public long RETROACTIVO_ACUMULADO { get; set; }

        public long VALORAF_DEFAULT { get; set; }

        public long VALORAF_PAGADO { get; set; }

        public long VALORAF_PENDIENTE { get; set; }

        public int NOTIFICACION_ID { get; set; }

        public int TRAMO { get; set; }

        public DateTime SISTEMA_FEC_PROCESO { get; set; }

        public int PERIODO_PROCESO { get; set; }

        public string ARCHIVO_IDCARGA { get; set; }
    }

}