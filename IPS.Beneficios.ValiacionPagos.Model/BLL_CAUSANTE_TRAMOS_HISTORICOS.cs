﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class BLL_CAUSANTE_TRAMOS_HISTORICOS
    {
        public Int32 RUT_CAUSANTE { get; set; }

        public DateTime FECHA_RECONOCIMIENTO { get; set; }

        public DateTime FECHA_EXTINCION { get; set; }

        public int TRAMO2008 { get; set; }

        public int TRAMO2009 { get; set; }

        public int TRAMO2010 { get; set; }

        public int TRAMO2011 { get; set; }

        public int TRAMO2012 { get; set; }

        public int TRAMO2013 { get; set; }

        public int TRAMO2014 { get; set; }

        public int TRAMO2015 { get; set; }
    }
}
