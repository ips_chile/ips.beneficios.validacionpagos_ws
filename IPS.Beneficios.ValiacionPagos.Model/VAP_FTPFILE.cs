﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_FILE
    {
        public string ARCHIVONOMBRE { get; set; }
        public string ARCHIVORUTA { get; set; }
        public int PERIODO { get; set; }
        public string EXTENSION { get; set; }

    //    public int LOADER_PARAM_ESTADO_ID { get; set; }
    //    public string LOADER_PARAM_TABLA { get; set; }
    //    public string LOADER_PARAM_CTLNOMBRE { get; set; }
    //    public int LOADER_PARAM_CANT_MES_PROCESO { get; set; }
    //    public char LOADER_PARAM_ES_HISTORICO { get; set; }
    //    public string LOADER_PARAM_SQL_QUERY { get; set; }
    //    public int LOADER_PARAM_FILE_COLUMNS { get; set; }
    //    public int LOADER_PARAM_FILE_DELIMETER { get; set; }
    //    public int LOADER_PARAM_LENGHT_TOUSE { get; set; }
    }
}
