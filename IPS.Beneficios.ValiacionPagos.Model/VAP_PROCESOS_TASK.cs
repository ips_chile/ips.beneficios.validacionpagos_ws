﻿using System;

namespace IPS.Beneficios.ValiacionPagos.Model
{
    public class VAP_PROCESOS_TASK
    {
        public int PROCESO_ID { get; set; }
        public int PERIODO_ID { get; set; }
        public int ESTADO_ID { get; set; }
        public DateTime FECHA_INI { get; set; }
        public DateTime FECHA_FIN { get; set; }
    }
}
