﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repository = IPS.Beneficios.ValiacionPagos.DataAccess.Repository;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_PERIODOS
    {
        protected Repository.VAP_PERIODOS _repositoryVAP_PERIODOS;

        public VAP_PERIODOS()
        {
            _repositoryVAP_PERIODOS = new Repository.VAP_PERIODOS();
        }

        public Model.VAP_PERIODOS GET(int ESTADO_ID)
        {
            return
            _repositoryVAP_PERIODOS.GETBY_ESTADO_ID(ESTADO_ID);
        }

        public List<Model.VAP_PERIODOS> GETALL()
        {
            return
            _repositoryVAP_PERIODOS.GETALL();
        }

        public bool UPDATE(int PERIODO_ID, int ESTADO_ID)
        {
            return
            _repositoryVAP_PERIODOS.UPDATE(PERIODO_ID, ESTADO_ID);
        }
    }
}
