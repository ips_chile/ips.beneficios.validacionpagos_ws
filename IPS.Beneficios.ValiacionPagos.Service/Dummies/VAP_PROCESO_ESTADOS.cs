﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repository = IPS.Beneficios.ValiacionPagos.DataAccess.Repository;
using Model = IPS.Beneficios.ValiacionPagos.Model;


namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_PROCESO_ESTADOS
    {
        protected Repository.VAP_PROCESO_ESTADOS _repositoryVAP_ESTADOS;

        public VAP_PROCESO_ESTADOS()
        {
            _repositoryVAP_ESTADOS = new Repository.VAP_PROCESO_ESTADOS();
        }

        public List<Model.VAP_PROCESO_ESTADOS> GETALL()
        {
            return
            _repositoryVAP_ESTADOS.GETALL();
        }
    }
}
