﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repository = IPS.Beneficios.ValiacionPagos.DataAccess.Repository;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_PROCESOS_DEPENDENCY
    {
        protected Repository.VAP_PROCESOS_DEPENDENCY _repositoryVAP_PROCESOS_DEPENDENCY;

        public VAP_PROCESOS_DEPENDENCY()
        {
            _repositoryVAP_PROCESOS_DEPENDENCY = new Repository.VAP_PROCESOS_DEPENDENCY();
        }

        public List<Model.VAP_PROCESOS_DEPENDENCY> GET(int _PROCESO_ID)
        {
            return
            _repositoryVAP_PROCESOS_DEPENDENCY.GET(_PROCESO_ID);
        }
    }
}
