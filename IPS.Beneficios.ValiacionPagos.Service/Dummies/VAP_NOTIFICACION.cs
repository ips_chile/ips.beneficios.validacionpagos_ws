﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repository = IPS.Beneficios.ValiacionPagos.DataAccess.Repository;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_NOTIFICACION
    {
        protected Repository.VAP_NOTIFICACION _repositoryVAP_NOTIFICACION;

        public VAP_NOTIFICACION()
        {
            _repositoryVAP_NOTIFICACION = new Repository.VAP_NOTIFICACION();
        }

        public List<Model.VAP_NOTIFICACION> GETALL()
        {
            return
            _repositoryVAP_NOTIFICACION.GETALL();
        }
    }
}
