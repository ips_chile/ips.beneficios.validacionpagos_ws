﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repository = IPS.Beneficios.ValiacionPagos.DataAccess.Repository;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_PROCESOS_TASK
    {
        protected Repository.VAP_PROCESOS_TASK _repositoryVAP_PROCESOS_TASK;

        public VAP_PROCESOS_TASK()
        {
            _repositoryVAP_PROCESOS_TASK = new Repository.VAP_PROCESOS_TASK();
        }


        public List<Model.VAP_PROCESOS_TASK> GETALL()
        {
            return
            _repositoryVAP_PROCESOS_TASK.GETALL();
        }

        public List<Model.VAP_PROCESOS_TASK> GET(int PROCESO_ID, int PERIODO_ID)
        {
            return
            _repositoryVAP_PROCESOS_TASK.GET(PROCESO_ID, PERIODO_ID);
        }

        public bool UPDATE(Model.VAP_PROCESOS_TASK VAP_PROCESOS_TASK)
        {
            var _serviceBLL_CHECK_PERIODOSPROCESOS = new Service.Dummies.BLL_CHECK_PERIODOSPROCESOS();

            var wasUpdated = _repositoryVAP_PROCESOS_TASK.UPDATE(VAP_PROCESOS_TASK);

            if(wasUpdated)
                _serviceBLL_CHECK_PERIODOSPROCESOS.CheckAndUpdatePeriodo(VAP_PROCESOS_TASK.PERIODO_ID);

            return
                wasUpdated;
        }

        public bool INSERT(Model.VAP_PROCESOS_TASK VAP_PROCESOS_TASK)
        {
            return
            _repositoryVAP_PROCESOS_TASK.INSERT(VAP_PROCESOS_TASK);
        }
    }
}
