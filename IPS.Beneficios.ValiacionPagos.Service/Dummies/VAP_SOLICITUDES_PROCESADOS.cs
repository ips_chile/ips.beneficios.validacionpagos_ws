﻿using IPS.Beneficios.ValiacionPagos.Service.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;


namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_SOLICITUDES_PROCESADOS : IPS_Dummie
    {
        #region //atributes
        protected DataTable _solicitudesProcesadasDataTable;

        private List<Model.VAP_SOLICITUDES_PROCESADOS> _rejectedSolicitudesProcesadasList = new List<Model.VAP_SOLICITUDES_PROCESADOS>();
        private List<Model.VAP_VERIFPAGO_CAUSANTES> _verifPagoCausantesByPeriodoProcesoList = new List<Model.VAP_VERIFPAGO_CAUSANTES>();
        private List<Model.VAP_SOLICITUDES_BASE> _solicitudesByPeriodoList = new List<Model.VAP_SOLICITUDES_BASE>();

        private string _folderPath;
        private string _filePath;
        private int _contadorArchivo;
        #endregion

        public VAP_SOLICITUDES_PROCESADOS(Model.VAP_PERIODOS VAP_PERIODO)
            : base(VAP_PERIODO)
        {
            _contadorArchivo = 0;
            _solicitudesProcesadasDataTable = Common.Helper.Generics.ConfigureDataTable(new Model.VAP_SOLICITUDES_PROCESADOS());

            _folderPath = GetFolder("IPS_SOLICITADOS", Common.Config.IPS_Config.solicitudesProcesadasMesFolder, _VAP_PERIODO);
            _filePath = GetFilePath(_folderPath, "IPS_SOLICITUDESPROCESADAS", _VAP_PERIODO);

            var causantesByPeriodoProceso = _repositoryVAP_VERIFPAGO_CAUSANTES.GET(_VAP_PERIODO.PERIODO_DISPLAY);
            _verifPagoCausantesByPeriodoProcesoList = GeneraCausantesByPeriodo(causantesByPeriodoProceso);

            _solicitudesByPeriodoList = _repositoryVAP_SOLICITUDES_BASE.GET(_VAP_PERIODO.PERIODO_DISPLAY);
        }


        public bool Main()
        {
            var wasCleanded = Common.Helper.FilesHelper.CleanFolder("IPS_SOLICITUDESPROCESADAS_" + _VAP_PERIODO.PERIODO_DISPLAY, _folderPath);

            if (wasCleanded)
            {
                this.Procesasolicitudes(_solicitudesByPeriodoList);

                return
                    ProccessFiles(_VAP_PERIODO);
            }
            else
                return false;
        }

        #region //PRIVATE
        private void Procesasolicitudes(List<Model.VAP_SOLICITUDES_BASE> _solicitudesByPeriodoList)
        {
            #region //Variables genericas metodo
            Int32 listCount = 0;
            List<Model.VAP_SOLICITUDES_PROCESADOS> _solicitudesProcesadasList = new List<Model.VAP_SOLICITUDES_PROCESADOS>();

            #endregion

            //Recorre las solicitudes
            foreach (var solicitud in _solicitudesByPeriodoList)
            {
                #region //Clear List`s
                _solicitudesProcesadasList.Clear();

                #endregion

                #region //Variables por solicitud
                var numeroCargasSolicitud = solicitud.NUMERO_CARGAS;
                var fechaDesdeSolicitudToUse = GetFechaDesdeSolicitud(solicitud);

                var _causantesPendPagoBySolicitud = _verifPagoCausantesByPeriodoProcesoList.Where(o => o.FOLIO_SOLICITUD == solicitud.FOLIO && o.CAUSANTE_PENDIENTEPAGO == "S").ToList();
                var _causantesPagadosBySolicitud = _verifPagoCausantesByPeriodoProcesoList.Where(o => o.FOLIO_SOLICITUD == solicitud.FOLIO && o.CAUSANTE_PENDIENTEPAGO == "N").ToList();

                var _periodosSolicitudArray = Common.Helper.Generics.GetperiodosPerMont(fechaDesdeSolicitudToUse, solicitud.FECHA_FIN_COMPENSACION);

                var _causantesMayorPermanencia = ApplyRule_CausantesMayorPermanencia(_causantesPendPagoBySolicitud, solicitud, fechaDesdeSolicitudToUse);
                var _causantesFechaInsert = ApplyRule_CausantesFechaIngreso(_causantesPendPagoBySolicitud);
                var _causantesRutMenor = ApplyRule_CausantesRutMenor(_causantesPendPagoBySolicitud);
                #endregion


                #region // Recorre pendientes a pagar
                #region //Recorre todos los periodos de la solicitud
                foreach (var periodo in _periodosSolicitudArray)
                {
                    #region //Variables por periodo
                    var _causantesSIAGFByPeriodoList = _causantesPendPagoBySolicitud.Where(o => o.CAUSANTE_PERIODO == periodo).ToList();
                    var _numeroCausantesToproccess = _causantesSIAGFByPeriodoList.Count < numeroCargasSolicitud ? _causantesSIAGFByPeriodoList.Count : numeroCargasSolicitud;
                    Common.Config.IPS_Config.NotificaionID _notificacionID;
                    #endregion

                    #region //Check cant elements and load if theses are greater than setted limit
                    var isEndOfList = Common.Helper.FilesHelper.CheckEndOfList(listCount, _solicitudesByPeriodoList.Count, 10);
                    Common.Helper.FilesHelper.CheckQuantityAndFillCSV(_filePath, "csv", "|", isEndOfList, 100, ref _solicitudesProcesadasDataTable, ref _contadorArchivo);
                    #endregion

                    #region //Recorre causantes segun prioridad y añade causantes
                    var _causantesPeriodoByPrioridadList = Get_CausantesListByPrioridad(_causantesSIAGFByPeriodoList, periodo, _causantesMayorPermanencia, _causantesFechaInsert, _causantesRutMenor);
                    short index = 0;

                    foreach (var causantePrioridad in _causantesPeriodoByPrioridadList)
                    {
                        if (index <= _numeroCausantesToproccess)
                        {
                            _notificacionID = GetNotificacionCausante(causantePrioridad.DIAS_A_PROCESAR, true, true);
                            _solicitudesProcesadasList.Add(GetSolicitud(solicitud, fechaDesdeSolicitudToUse, causantePrioridad, _notificacionID));
                        }
                        else
                        {
                            _notificacionID = GetNotificacionCausante(causantePrioridad.DIAS_A_PROCESAR, true, false);
                            _solicitudesProcesadasList.Add(GetSolicitud(solicitud, fechaDesdeSolicitudToUse, causantePrioridad, _notificacionID));
                        }

                        index++;
                    }
                    #endregion
                }
                #endregion

                #endregion

                #region //Recorre los pagados
                foreach (var causantePagado in _causantesPagadosBySolicitud)
                {
                    #region //Check cant elements and load if theses are greater than setted limit
                    var isEndOfList = Common.Helper.FilesHelper.CheckEndOfList(listCount, _solicitudesByPeriodoList.Count, 10);
                    Common.Helper.FilesHelper.CheckQuantityAndFillCSV(_filePath, "csv", "|", isEndOfList, 100, ref _solicitudesProcesadasDataTable, ref _contadorArchivo);
                    #endregion

                    Common.Config.IPS_Config.NotificaionID _notificacionID = GetNotificacionCausante(causantePagado.DIAS_A_PROCESAR, false, false);
                    _solicitudesProcesadasList.Add(GetSolicitud(solicitud, fechaDesdeSolicitudToUse, causantePagado, _notificacionID));
                }
                #endregion

                #region //Add data to common arrayList
                var cargasPeriodoArray = Common.Helper.FilesHelper.ConvertToDataTable(_solicitudesProcesadasList);

                if (cargasPeriodoArray != null && cargasPeriodoArray.Rows.Count > 0)
                    _solicitudesProcesadasDataTable.Merge(cargasPeriodoArray);
                else
                    _rejectedSolicitudesProcesadasList.AddRange(_solicitudesProcesadasList);//Solicitud no tiene cargas para procesar
                #endregion

                listCount++;
            }
        }

        private bool ProccessFiles(Model.VAP_PERIODOS VAP_PERIODO)
        {
            var criteriaToSearch = string.Format("IPS_SOLICITUDESPROCESADAS_{0}", VAP_PERIODO.PERIODO_DISPLAY);

            Model.VAP_FILE VAP_FILE = new Model.VAP_FILE()
            {
                ARCHIVORUTA = _folderPath
                 ,
                PERIODO = VAP_PERIODO.PERIODO_DISPLAY
                ,
                ARCHIVONOMBRE = criteriaToSearch
                ,
                EXTENSION = ".CSV"
            };

            var _fileDictionary = Common.Helper.FilesHelper.GetFiles(VAP_FILE.ARCHIVORUTA, VAP_FILE.ARCHIVONOMBRE + "*");

            var _successCount = _fileDictionary.Count();//archivos sin errores

            if (_successCount == 0) _successCount = -1;

            foreach (var filePath in _fileDictionary)
            {
                var _tableName = typeof(Model.VAP_SOLICITUDES_PROCESADOS).Name;

                var _fileName = Path.GetFileNameWithoutExtension(filePath);

                var _bulkedData = _serviceVAP_BULKDATA.BulkData(new Model.SQL_LOADER()
                {
                    TABLE_NAME = _tableName
                    ,
                    FILEPATH = filePath
                    ,
                    CLEANTABLE = true
                    ,
                    CLEANTABLE_QUERY = string.Format("DELETE VALIDACION_PAGOS.{0} WHERE PERIODO_PROCESO = {1}", _tableName, VAP_PERIODO.PERIODO_DISPLAY)
                    ,
                    CTLPATH = string.Format(@"{0}LOADERS\CTL\{1}.ctl", Common.Config.IPS_Config.installDirApp, _tableName)
                    ,
                    LOGPATH = string.Format(@"{0}LOADERS\LOG\{1}.log", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    BADPATH = string.Format(@"{0}LOADERS\LOG\{1}.bad", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    DISCARDPATH = string.Format(@"{0}LOADERS\LOG\{1}.dsc", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    DICTIONARY_SETCONTROLFILEVALUES = new Dictionary<string, string>() { {"PERIODO_PROCESO", string.Format("{0}  constant {1}", "PERIODO_PROCESO", VAP_FILE.PERIODO)}
                                                                                         ,{"ARCHIVO_IDCARGA", string.Format("{0}  constant \"{1}{2}\"", "ARCHIVO_IDCARGA", _fileName, VAP_FILE.EXTENSION) }}
                });

                if (_bulkedData)
                    _successCount--;
            }

            return
                _successCount == 0;// Shoulb be 0 if all bulked files run successfully
        }

        private Model.VAP_SOLICITUDES_PROCESADOS GetSolicitud(Model.VAP_SOLICITUDES_BASE _solicitud, DateTime fechaDesdeSolicitudToUse, Model.VAP_VERIFPAGO_CAUSANTES _causantePrioridad, Common.Config.IPS_Config.NotificaionID notificacionID)
        {

            var solicitudToReturn = new Model.VAP_SOLICITUDES_PROCESADOS()
            {
                FOLIO = _solicitud.FOLIO
                ,
                FECHA_INICIO_COMPENSACION = _solicitud.FECHA_INICIO_COMPENSACION
                ,
                FECHA_INICIO_CALCULADA = fechaDesdeSolicitudToUse
                ,
                FECHA_FIN_COMPENSACION = _solicitud.FECHA_FIN_COMPENSACION
                ,
                RUT_EMPLEADOR = _solicitud.RUT_EMPLEADOR
                ,
                DV_EMPLEADOR = _solicitud.DV_EMPLEADOR
                ,
                RUT_TRABAJADOR = _solicitud.RUT_TRABAJADOR
                ,
                DV_TRABAJADOR = _solicitud.DV_TRABAJADOR
                ,
                RUT_CAUSANTE = _causantePrioridad.RUT_CAUSANTE
                ,
                DV_CAUSANTE = _causantePrioridad.DV_CAUSANTE
                ,
                CAUSANTE_EDAD = _repositoryVAP_CAUSANTES_MES.GET_AGE(_causantePrioridad.RUT_CAUSANTE)
                ,
                CAUSANTE_PENDIENTEPAGO = _causantePrioridad.CAUSANTE_PENDIENTEPAGO
                ,
                CAUSANTE_FEC_REC = _causantePrioridad.CAUSANTE_FEC_REC
                ,
                CAUSANTE_FEC_EXT_CALC = _causantePrioridad.CAUSANTE_FEC_EXT_CALC
                ,
                CAUSANTE_PERIODO = _causantePrioridad.CAUSANTE_PERIODO
                ,
                CAUSANTE_DIAS_SIAGF = _causantePrioridad.CAUSANTE_DIAS_SIAGF
                ,
                CAUSANTE_DIAS_SOLICITADOS = _causantePrioridad.CAUSANTE_DIAS_SOLICITADOS
                ,
                CAUSANTE_DIAS_TRABAJADOS = _causantePrioridad.CAUSANTE_DIAS_TRABAJADOS
                ,
                VALOR_ARCHIVO12 = _causantePrioridad.VALOR_ARCHIVO12
                ,
                RETROACTIVO_ACUMULADO = _causantePrioridad.RETROACTIVO_ACUMULADO
                ,
                VALORAF_DEFAULT = _causantePrioridad.VALORAF_DEFAULT
                ,
                VALORAF_PAGADO = _causantePrioridad.VALORAF_PAGADO//_valorPagado
                ,
                VALORAF_PENDIENTE = _causantePrioridad.VALORAF_PENDIENTE//_valorPendiente
                ,
                SISTEMA_FEC_PROCESO = _fechaProceso
                ,
                NOTIFICACION_ID = (int)notificacionID
                ,
                PERIODO_PROCESO = _VAP_PERIODO.PERIODO_DISPLAY
                ,
                ARCHIVO_IDCARGA = string.Empty
                ,
                TRAMO = _causantePrioridad.TRAMO
            };

            return
                solicitudToReturn;

        }

        private List<Model.VAP_VERIFPAGO_CAUSANTES> GeneraCausantesByPeriodo(List<Model.VAP_VERIFPAGO_CAUSANTES> _verifPagoCausantesByPeriodoProcesoList)
        {
            List<Model.VAP_VERIFPAGO_CAUSANTES> causantesList = new List<Model.VAP_VERIFPAGO_CAUSANTES>();

            #region //Agrupa causantes
            var causantesSolicitudAgrupados = _verifPagoCausantesByPeriodoProcesoList
                                    .GroupBy(row => new
                                    {
                                        row.FOLIO_SOLICITUD
                                        ,
                                        row.RUT_BENEFICIARIO
                                        ,
                                        row.RUT_CAUSANTE
                                        ,
                                        row.VALORAF_DEFAULT
                                        ,
                                        row.VALORAF_PAGADO
                                        ,
                                        row.VALORAF_PENDIENTE
                                        ,
                                        row.CAUSANTE_PERIODO
                                        ,
                                        row.TRAMO
                                    })
                                    .Select(g => new
                                    {
                                        FOLIO_SOLICITUD = g.Key.FOLIO_SOLICITUD
                                        ,
                                        RUT_BENEFICIARIO = g.Key.RUT_BENEFICIARIO
                                        ,
                                        RUT_CAUSANTE = g.Key.RUT_CAUSANTE
                                        ,
                                        ING_PROMEDIO = g.Key.VALORAF_DEFAULT
                                        ,
                                        VALORAF_PAGADO = g.Key.VALORAF_PAGADO
                                        ,
                                        VALORAF_PENDIENTE = g.Key.VALORAF_PENDIENTE
                                        ,
                                        CAUSANTE_PERIODO = g.Key.CAUSANTE_PERIODO
                                        ,
                                        TRAMO = g.Key.TRAMO
                                    })
                                    .OrderBy(g => g.RUT_CAUSANTE)
                                    .ThenBy(g => g.CAUSANTE_PERIODO)
                                    .ToList();
            #endregion

            #region //Recorre causantes agupados, eg:(FOLIO_SOLICITUD: 1111, RUT_BENEFICIARIO: 111-1, RUT_CAUSANTE: 1-1, FECHA_INSERT: 01/02/2015, ING_PROMEDIO: $2000, VALORAF_PAGADO: $1500, VALORAF_PENDIENTE; $500  ,CAUSANTE_PERIODO: 201505)
            foreach (var causanteSolicitud in causantesSolicitudAgrupados)
            {
                var dbCausantesPagoList = GetCausantePeriodo(causanteSolicitud.RUT_CAUSANTE, causanteSolicitud.CAUSANTE_PERIODO);

                var _saldoPagadoEnHistorico = dbCausantesPagoList.Sum(o => o.VALORAF_PAGADO);
                var _valorPendientePago = (causanteSolicitud.ING_PROMEDIO - _saldoPagadoEnHistorico);

                var causanteToBeFormatted = _verifPagoCausantesByPeriodoProcesoList
                                            .Where(o => o.FOLIO_SOLICITUD == causanteSolicitud.FOLIO_SOLICITUD
                                                        && o.RUT_BENEFICIARIO == causanteSolicitud.RUT_BENEFICIARIO
                                                        && o.RUT_CAUSANTE == causanteSolicitud.RUT_CAUSANTE
                                                        && o.CAUSANTE_PERIODO == causanteSolicitud.CAUSANTE_PERIODO
                                                        && o.VALORAF_PAGADO == causanteSolicitud.VALORAF_PAGADO
                                                        && o.VALORAF_PENDIENTE == causanteSolicitud.VALORAF_PENDIENTE)
                                            .FirstOrDefault();

                if (causanteToBeFormatted != default(Model.VAP_VERIFPAGO_CAUSANTES))
                {
                    causantesList.Add(new Model.VAP_VERIFPAGO_CAUSANTES()
                    {
                        FOLIO_SOLICITUD = causanteToBeFormatted.FOLIO_SOLICITUD
                        ,
                        RUT_BENEFICIARIO = causanteToBeFormatted.RUT_BENEFICIARIO
                        ,
                        RUT_CAUSANTE = causanteToBeFormatted.RUT_CAUSANTE
                        ,
                        DV_CAUSANTE = causanteToBeFormatted.DV_CAUSANTE
                        ,
                        CAUSANTE_PENDIENTEPAGO = _valorPendientePago > 0 ? "S" : "N"
                        ,
                        CAUSANTE_FEC_REC = causanteToBeFormatted.CAUSANTE_FEC_REC
                        ,
                        CAUSANTE_FEC_EXT_CALC = causanteToBeFormatted.CAUSANTE_FEC_EXT_CALC
                        ,
                        FECHA_INSERT = causanteToBeFormatted.FECHA_INSERT
                        ,
                        CAUSANTE_PERIODO = causanteToBeFormatted.CAUSANTE_PERIODO
                        ,
                        CAUSANTE_DIAS_SIAGF = causanteToBeFormatted.CAUSANTE_DIAS_SIAGF
                        ,
                        CAUSANTE_DIAS_SOLICITADOS = causanteToBeFormatted.CAUSANTE_DIAS_SOLICITADOS
                        ,
                        CAUSANTE_DIAS_TRABAJADOS = causanteToBeFormatted.CAUSANTE_DIAS_TRABAJADOS
                        ,
                        DIAS_A_PROCESAR = causanteToBeFormatted.DIAS_A_PROCESAR
                        ,
                        VALOR_ARCHIVO12 = causanteToBeFormatted.VALOR_ARCHIVO12
                        ,
                        RETROACTIVO_ACUMULADO = causanteToBeFormatted.RETROACTIVO_ACUMULADO
                        ,
                        VALORAF_DEFAULT = causanteToBeFormatted.VALORAF_DEFAULT
                        ,
                        VALORAF_PAGADO = causanteToBeFormatted.VALORAF_PAGADO
                        ,
                        VALORAF_PENDIENTE = causanteToBeFormatted.VALORAF_PENDIENTE //_valorPendientePago
                        ,
                        TRAMO = causanteToBeFormatted.TRAMO
                    });
                }

            }
            #endregion

            return
                causantesList;
        }

        private Common.Config.IPS_Config.NotificaionID GetNotificacionCausante(int diasAProcesar, bool isPendientePago, bool isConsideradoEnPeriodo)
        {
            Common.Config.IPS_Config.NotificaionID notificacionID = default(Common.Config.IPS_Config.NotificaionID);

            if (isConsideradoEnPeriodo)//CONSIDERADO EN PERIODO
            {
                if (isPendientePago)//CON PAGO PENDIENTE
                {
                    if (diasAProcesar > 0)//CON RELACION LABORAL
                        notificacionID = Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_12_7;
                    else//SIN RELACION LABORAL
                        notificacionID = Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_12_17;
                }
            }
            else//NO CONSIDERADO EN PERIODO
            {
                if (isPendientePago)//CON PAGO PENDIENTE
                {
                    if (diasAProcesar > 0)//CON RELACION LABORAL
                        notificacionID = Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_11_7;
                    else//SIN RELACION LABORAL
                        notificacionID = Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_11_17;
                }
                else//SIN PAGO PENDIENTE
                    notificacionID = Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_11_8;
            }

            return
                notificacionID;
        }
        #endregion
    }
}

#region //No se usa

//if (solicitud.FOLIO == 91201401000148)
//{ 
//}

//var isValueAlreadyPresentOtherSolicitudes = dbCausantesPendientePagoList.Exists(o => o.FOLIO_SOLICITUD != causanteSolicitud.FOLIO_SOLICITUD
//                                                                                //&& o.RUT_BENEFICIARIO == causante.RUT_BENEFICIARIO
//                                                                                && o.RUT_CAUSANTE == causanteSolicitud.RUT_CAUSANTE
//                                                                                && o.CAUSANTE_PERIODO == causanteSolicitud.CAUSANTE_PERIODO
//                                                                                && o.VALORAF_PAGADO == _saldoPagadoEnHistorico
//                                                                                && o.VALORAF_PENDIENTE == _valorPendientePago);




//if (causante.RUT_CAUSANTE == 15794088 && causante.CAUSANTE_PERIODO == 201311)
//{ 
//}

//foreach (var causanteSIAGFPeriodo in _causantesSIAGFByPeriodoList)
//{
//    short index = 1;

//    foreach (var causanteID in causanteListToUse)
//    {
//        var periodoHasCausante = _causantesSIAGFByPeriodoList.Exists(o => o.RUT_CAUSANTE == causanteID);

//        if (periodoHasCausante)
//        {
//            var causanteToUse = _causantesSIAGFByPeriodoList.Where(o => o.RUT_CAUSANTE == causanteID).SingleOrDefault();

//            #region //Acumula saldos pendiente de pago

//            long montoApagarCausante = _montoLimiteAPagar > causanteToUse.CAUSANTE_INGPROMEDIO ? causanteToUse.CAUSANTE_INGPROMEDIO : _montoLimiteAPagar;
//            long montoPendienteCausante = causanteToUse.CAUSANTE_INGPROMEDIO - montoApagarCausante;
//            _montoLimiteAPagar -= montoApagarCausante;

//            verifPagoCausanteList.Add(Get_CausanteVerifPago(causanteToUse, montoApagarCausante, montoPendienteCausante));
//            #endregion
//        }

//        if (index == _numeroCausantesToproccess)
//            break;

//        index++;
//    }
//}
#endregion
