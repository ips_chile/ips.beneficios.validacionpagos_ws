﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repository = IPS.Beneficios.ValiacionPagos.DataAccess.Repository;
using Model = IPS.Beneficios.ValiacionPagos.Model;
using Common = IPS.Beneficios.Common;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_PARAMETROS
    {
        protected Repository.VAP_PARAMETROS _repositoryVAP_PARAMETROS;

        public VAP_PARAMETROS()
        {
            _repositoryVAP_PARAMETROS = new Repository.VAP_PARAMETROS();
        }

        public List<Model.VAP_PARAMETROS> GETALL()
        {
            return
            _repositoryVAP_PARAMETROS.GETALL();
        }

        /// <summary>
        /// ApplyConstrain() Compara dos valores acorde a los parametros entregados y el mismo tipo de datos(ambos parametros) y la restriccion entregada, devuelve bolean en cualquier caso.
        /// </summary>
        /// <param name="valueToCompare1"></param>
        /// <param name="valueToCompare2"></param>
        /// <param name="constrain"></param>
        /// <param name="type"></param>
        /// <returns>bolean</returns>
        public static bool ApplyConstrain(object valueToCompare1, object valueToCompare2, string constrain, Type type)
        {
            var complieswithConstrain = false;

            switch (constrain.ToUpper())
            {
                case "<=":
                    complieswithConstrain = GetValue(valueToCompare1, type) <= GetValue(valueToCompare2, type);

                    break;

                case ">=":
                    complieswithConstrain = GetValue(valueToCompare1, type) >= GetValue(valueToCompare2, type);

                    break;

                case "<":
                    complieswithConstrain = GetValue(valueToCompare1, type) < GetValue(valueToCompare2, type);

                    break;

                //case "BETWEEN":
                //    complieswithConstrain = GetValue(valueToCompare1, type) <= GetValue(valueToCompare2, type);

                default:
                    break;
            }

            return
                complieswithConstrain;
        }

        /// <summary>
        /// ApplyConstrain() Compara dos valores acorde a los parametros entregados con tipo de datos distinto uno de otro y la restriccion entregada, devuelve bolean en cualquier caso.
        /// </summary>
        /// <param name="valueToCompare1"></param>
        /// <param name="valueToCompare2"></param>
        /// <param name="constrain"></param>
        /// <param name="valueType1"></param>
        /// <param name="valueType2"></param>
        /// <returns>bolean</returns>
        public static bool ApplyConstrain(object valueToCompare1, object valueToCompare2, string constrain, Type valueType1, Type valueType2)
        {
            var complieswithConstrain = false;

            switch (constrain.ToUpper())
            {
                case "BETWEEN":
                    var singleValue = GetValue(valueToCompare1, valueType1);
                    var arrayValue = GetValue(valueToCompare2, valueType2);

                    complieswithConstrain = IsInBetween(singleValue, arrayValue);

                    break;

                default:
                    break;
            }

            return
                complieswithConstrain;
        }

        /// <summary>
        /// IsInBetween(), Determine if an Int element is between two digits
        /// </summary>
        /// <param name="singleValue"></param>
        /// <param name="arrayValue"></param>
        /// <returns></returns>
        public static bool IsInBetween(int singleValue, int[] arrayValue)
        {
                try 
	            {	        
                    if (arrayValue.Count() > 1)
                    {
                        var existElement = singleValue > arrayValue.First() && singleValue <= arrayValue.Last();
                            
                        return
                            existElement;
                    }
                    else
                        return
                            false;
	            }
	            catch (Exception ex)
	            {
		            Common.Config.IPS_Config.CaptureException(ex);

		            return
                       false;
	            }

        }

        #region //PRIVATE
        private static dynamic GetValue(object value, Type type)
        {
            dynamic castedValue = null;

            if (type.IsArray)
            {
                if (Common.Helper.Generics.IsArrayOf<string>(type))
                    castedValue = (string[])value;

                else if (Common.Helper.Generics.IsArrayOf<int>(type))
                    castedValue = (int[])value;

                else if (Common.Helper.Generics.IsArrayOf<char>(type))
                    castedValue = (char[])value;

                else if (Common.Helper.Generics.IsArrayOf<DateTime>(type))
                    castedValue = (DateTime[])value;

            }
            else
            {
                switch (Type.GetTypeCode(type))
                {
                    case TypeCode.Int32:
                        castedValue = (Int32)value;

                        break;

                    case TypeCode.Double:
                        castedValue = (Double)value;

                        break;

                    case TypeCode.Char:
                        castedValue = (Char)value;

                        break;

                    case TypeCode.Boolean:
                        castedValue = (Boolean)value;

                        break;

                    case TypeCode.String:
                        castedValue = (string)value;

                        break;

                    case TypeCode.DateTime:
                        castedValue = (DateTime)value;

                        break;
                }
            }

            return
                castedValue;
        }
        #endregion

    }
}
