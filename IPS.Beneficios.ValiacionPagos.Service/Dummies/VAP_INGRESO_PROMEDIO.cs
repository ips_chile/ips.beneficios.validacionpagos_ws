﻿using IPS.Beneficios.ValiacionPagos.Service.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_INGRESO_PROMEDIO : IPS_Dummie
    {
        #region //atributes
        protected DataTable _calcIngresoPromedioDataTable;
        private List<Model.VAP_INGRESO_PROMEDIO> _rejectedIngresoPromedioList = new List<Model.VAP_INGRESO_PROMEDIO>();
        private List<Model.VAP_TRAMOS> _tramosList = new List<Model.VAP_TRAMOS>();
        private List<Model.VAP_CAUSANTES_MES> _causantesPeriodo_GreaterThanFReconocimientoList = new List<Model.VAP_CAUSANTES_MES>();
        private List<Model.VAP_SOLICITUDES_BASE> _solicitudesPeriodoList = new List<Model.VAP_SOLICITUDES_BASE>();

        private Model.VAP_PARAMETROS _paramAnoVigenciaIngPromedio;
        private Model.VAP_PARAMETROS _paramFecReconocimiento;

        private int[] _tipoCausanteDuploArray;
        private string _folderPath;
        private string _filePath;
        private int _contadorArchivo;
        #endregion

        public VAP_INGRESO_PROMEDIO(Model.VAP_PERIODOS VAP_PERIODO) : base(VAP_PERIODO)
        {
            _contadorArchivo = 0;
            _calcIngresoPromedioDataTable = Common.Helper.Generics.ConfigureDataTable(new Model.VAP_INGRESO_PROMEDIO());

            _folderPath = GetFolder("IPS_SOLICITADOS", Common.Config.IPS_Config.calculoIngresoPromedioMesFolder, _VAP_PERIODO);
            _filePath = GetFilePath(_folderPath, "IPS_CALCULOPROMEDIO", _VAP_PERIODO);

            _paramAnoVigenciaIngPromedio = _parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.AÑO_DESDE_VIGENCIA_INGRESO_PROMEDIO);
            _paramFecReconocimiento = _parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.FECHA_RECONOCIMIENTO);
            _tipoCausanteDuploArray = Common.Helper.Generics.GetIntArray(_parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.TIPO_CAUSANTE_DUPLO).PARAMETRO_VALOR);

            _tramosList = _repositoryVAP_TRAMOS.GET_ALL();

            _causantesPeriodo_GreaterThanFReconocimientoList = _repositoryVAP_CAUSANTES_MES.GET(DateTime.Parse(_paramFecReconocimiento.PARAMETRO_VALOR), _VAP_PERIODO.PERIODO_DISPLAY, Common.Config.IPS_Config.NotificaionID.CAUSANTE_UNICO_ENPERIODO);

            _solicitudesPeriodoList = _repositoryVAP_SOLICITUDES_BASE.GET(_VAP_PERIODO.PERIODO_DISPLAY);
        }

        public bool Main()
        {
            var wasCleanded = Common.Helper.FilesHelper.CleanFolder("IPS_CALCULOPROMEDIO_" + _VAP_PERIODO.PERIODO_DISPLAY, _folderPath);

            if (wasCleanded)
            {
                ProcesaCalculoPromedio();

                return 
                    ProccessFiles(_VAP_PERIODO);
            }
            else
                return false;
        }


        #region //PRIVATE
        private void ProcesaCalculoPromedio()
        {
            #region //Variables metodo
            Int32 listCount = 0;
            List<Model.VAP_INGRESO_PROMEDIO> _calcIngresoPromedioList = new List<Model.VAP_INGRESO_PROMEDIO>();
            #endregion

            //foreach (var solicitud in _solicitudesPeriodoList)
            //{
                //_calcIngresoPromedioList.Clear();

                //#region //Varaibles por solicitud
                //var fechaDesdeToUse = GetFechaDesdeSolicitud(solicitud);
                //var causantesBySolicitudList = GetCausantes_BySolicitud(solicitud, fechaDesdeToUse);
                //#endregion

                foreach (var causante in _causantesPeriodo_GreaterThanFReconocimientoList)
                {
                    #region //Vars
                    int _tramoToUse = 0;
	                #endregion

                    #region //Check cant elements and load if theses are greater than setted limit
                    var isEndOfList = Common.Helper.FilesHelper.CheckEndOfList(listCount, _causantesPeriodo_GreaterThanFReconocimientoList.Count, 100);
                    Common.Helper.FilesHelper.CheckQuantityAndFillCSV(_filePath, "csv", "|", isEndOfList, 1000, ref _calcIngresoPromedioDataTable, ref _contadorArchivo);
                    #endregion


                    //if (causante.RUT_BENEFICIARIO == 12736927 && causante.RUT_CAUSANTE == 20999135 && causante.CAUSANTE_PERIODO == 201311)
                    //{
 
                    //}

                    var valAsigIngPromedio = GetIngresoPromedio_Normal(causante, ref _tramoToUse);

                    if (valAsigIngPromedio < 0)
                        valAsigIngPromedio = GetIngresoPromedio_Historico(causante, ref _tramoToUse);

                    var causanteToAdd = GetIngresoPromedioToAdd(causante, valAsigIngPromedio, _tramoToUse);

                    _calcIngresoPromedioList.Add(causanteToAdd);

                    #region //Add data to common arrayList
                    if (_calcIngresoPromedioList.Count >= 100 || isEndOfList)
                    {
                        var cargasPeriodoArray = Common.Helper.FilesHelper.ConvertToDataTable(_calcIngresoPromedioList);

                        if (cargasPeriodoArray != null && cargasPeriodoArray.Rows.Count > 0)
                        {
                            _calcIngresoPromedioDataTable.Merge(cargasPeriodoArray);

                            _calcIngresoPromedioList.Clear();
                        }
                        else
                            _rejectedIngresoPromedioList.AddRange(_calcIngresoPromedioList);// Trabajador no tiene cargas reconocidas
                    }
                    #endregion

                    listCount++;
                }
            //}
        }

        private bool ProccessFiles(Model.VAP_PERIODOS VAP_PERIODO)
        {
            var criteriaToSearch = string.Format("IPS_CALCULOPROMEDIO_{0}", VAP_PERIODO.PERIODO_DISPLAY);

            Model.VAP_FILE VAP_FTPFILE = new Model.VAP_FILE()
            {
                ARCHIVORUTA = _folderPath
                 ,
                PERIODO = VAP_PERIODO.PERIODO_DISPLAY
                ,
                ARCHIVONOMBRE = criteriaToSearch
                ,
                EXTENSION = ".CSV"
            };

            var _fileDictionary = Common.Helper.FilesHelper.GetFiles(VAP_FTPFILE.ARCHIVORUTA, VAP_FTPFILE.ARCHIVONOMBRE + "*");

            var _successCount = _fileDictionary.Count();//archivos sin errores

            if (_successCount == 0) _successCount = -1;

            foreach (var filePath in _fileDictionary)
            {
                var _tableName = typeof(Model.VAP_INGRESO_PROMEDIO).Name;

                var _fileName = Path.GetFileNameWithoutExtension(filePath);

                var _bulkedData = _serviceVAP_BULKDATA.BulkData(new Model.SQL_LOADER()
                {
                    TABLE_NAME = _tableName
                    ,
                    FILEPATH = filePath
                    ,
                    CLEANTABLE = true
                    ,
                    CLEANTABLE_QUERY = string.Format("DELETE VALIDACION_PAGOS.{0} WHERE PERIODO_PROCESO = {1}", _tableName, VAP_PERIODO.PERIODO_DISPLAY)
                    ,
                    CTLPATH = string.Format(@"{0}LOADERS\CTL\{1}.ctl", Common.Config.IPS_Config.installDirApp, _tableName)
                    ,
                    LOGPATH = string.Format(@"{0}LOADERS\LOG\{1}.log", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    BADPATH = string.Format(@"{0}LOADERS\LOG\{1}.bad", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    DISCARDPATH = string.Format(@"{0}LOADERS\LOG\{1}.dsc", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    DICTIONARY_SETCONTROLFILEVALUES = new Dictionary<string, string>() { {"PERIODO_PROCESO", string.Format("{0}  constant {1}", "PERIODO_PROCESO", VAP_FTPFILE.PERIODO)}
                                                                                        ,{"ARCHIVO_IDCARGA", string.Format("{0}  constant \"{1}{2}\"", "ARCHIVO_IDCARGA", _fileName, VAP_FTPFILE.EXTENSION) }}
                });

                if (_bulkedData)
                    _successCount--;
            }

            return
                _successCount == 0;// Shoulb be 0 if all bulked files run successfully
        }

        private long GetIngresoPromedio_Normal(Model.VAP_CAUSANTES_MES causante, ref int _tramoToUse)
        {
            long valorAsignacion = -1;
            var tramoToSearch = _tramosList.Where(o => causante.CAUSANTE_PERIODO >= o.PERIODOD && causante.CAUSANTE_PERIODO <= o.PERIODOH).Select(o => o).FirstOrDefault();

            if (tramoToSearch != default(Model.VAP_TRAMOS))
            {
                var tableName = string.Format("IngresoPromedio_{0}_Unicos_y_Repetidos", tramoToSearch.PERIODO);

                var ingresoPromedio = GetIngresoPromedio(tableName, causante.RUT_BENEFICIARIO);

                if (ingresoPromedio != default(Model.BLL_INGRESO_PROMEDIO))
                {
                    var tramoToTake = _tramosList
                        .Where(o =>
                            tramoToSearch.PERIODO == o.PERIODO
                            && (causante.CAUSANTE_PERIODO >= o.PERIODOD && causante.CAUSANTE_PERIODO <= o.PERIODOH)
                            && (ingresoPromedio.INGRESO_PROM >= o.ING_DESDE && ingresoPromedio.INGRESO_PROM <= o.ING_HASTA))
                        .Select(o => o).SingleOrDefault();

                    if (tramoToTake != default(Model.VAP_TRAMOS))
                    {
                        var isCausanteDuplo = _tipoCausanteDuploArray.Contains(causante.CAUSANTE_TIPO);

                        valorAsignacion = isCausanteDuplo ? tramoToTake.VALOR_ASIGDUPLO : tramoToTake.VALOR_ASIGSIMPLE;

                        _tramoToUse = tramoToTake.TRAMOA;
                    }
                }
            }

            return
                valorAsignacion;
        }

        private long GetIngresoPromedio_Historico(Model.VAP_CAUSANTES_MES causante, ref int _tramoToUse)
        {
            //1._ consultar rango desde-hasta en aftramos y obtener año
            //2._ ir a tramos historicos y consultar para el causante segun rut causante el la columna tramo{año} el Id del tramo
            //3._ consultar en aftramos segun el año y el Id del tramo cual le corresponde

            long valorAsignacion = -1;
            var tramoToSearch = _tramosList.Where(o => causante.CAUSANTE_PERIODO >= o.PERIODOD && causante.CAUSANTE_PERIODO <= o.PERIODOH).Select(o => o).FirstOrDefault();

            if (tramoToSearch != default(Model.VAP_TRAMOS))
            {
                var idTramo = _serviceBLL_CAUSANTE_TRAMOS_HISTORICOS.GET_IdTramo(causante, tramoToSearch.PERIODO);// hay que dar Grant permisos sobre SELECT queries
                    
                var tramoToTake = _tramosList
                        .Where(o =>
                            o.PERIODO == tramoToSearch.PERIODO && (o.TRAMOA == idTramo))
                        .Select(o => o).SingleOrDefault();

                if (tramoToTake != default(Model.VAP_TRAMOS))
                {
                    var isCausanteDuplo = _tipoCausanteDuploArray.Contains(causante.CAUSANTE_TIPO);

                    valorAsignacion = isCausanteDuplo ? tramoToTake.VALOR_ASIGDUPLO : tramoToTake.VALOR_ASIGSIMPLE;

                    _tramoToUse = tramoToTake.TRAMOA;
                }
            }

            return
                valorAsignacion;
        }

        private Model.BLL_INGRESO_PROMEDIO GetIngresoPromedio(string tableName, Int32 rutTrabajador)
        {
            return
                _repositoryBLL_CALCULO_INGRESOPROMEDIO.GET(tableName, rutTrabajador);
        }

        private List<Model.VAP_CAUSANTES_MES> GetCausantes_BySolicitud(Model.VAP_SOLICITUDES_BASE solicitud, DateTime fechaDesdeToUse)
        {
            var fecHastaSolicitud = solicitud.FECHA_FIN_COMPENSACION;

            var causantesList = _causantesPeriodo_GreaterThanFReconocimientoList
                .Where(o => o.RUT_BENEFICIARIO == solicitud.RUT_TRABAJADOR
                       && (o.CAUSANTE_PERIODO >= int.Parse(fechaDesdeToUse.ToString("yyyyMM"))
                            && o.CAUSANTE_PERIODO <= int.Parse(fecHastaSolicitud.ToString("yyyyMM"))))
                       .ToList();

            return
                causantesList;

        }

        #endregion
    }
}
