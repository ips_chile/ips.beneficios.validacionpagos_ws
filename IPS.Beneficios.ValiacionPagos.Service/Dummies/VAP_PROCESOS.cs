﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repository = IPS.Beneficios.ValiacionPagos.DataAccess.Repository;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_PROCESOS
    {
        protected Repository.VAP_PROCESOS _repositoryVAP_PROCESOS;

        public VAP_PROCESOS()
        {
            _repositoryVAP_PROCESOS = new Repository.VAP_PROCESOS();
        }

        public List<Model.VAP_PROCESOS> GET()
        {
            return
            _repositoryVAP_PROCESOS.GETALL();
        }
    }
}
