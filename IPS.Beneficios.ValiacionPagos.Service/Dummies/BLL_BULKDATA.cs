﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repository = IPS.Beneficios.ValiacionPagos.DataAccess.Repository;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class BLL_BULKDATA
    {
        Repository.BLL_BULKDATA _repositoryBLL_BULKDATA;

        public BLL_BULKDATA()
        {
            _repositoryBLL_BULKDATA = new Repository.BLL_BULKDATA();
        }

        public bool BulkData(Model.SQL_LOADER SQL_LOADER)
        {
            return
                _repositoryBLL_BULKDATA.BulkData(SQL_LOADER);
        }

        public bool CleanTable(Model.SQL_LOADER SQL_LOADER)
        {
            return
                _repositoryBLL_BULKDATA.CleanTable(SQL_LOADER);
        }

        public bool CleanTable(Model.SQL_LOADER SQL_LOADER, Common.Config.IPS_Config.ConnString connString)
        {
            return
                _repositoryBLL_BULKDATA.CleanTable(SQL_LOADER, connString);
        }

        public bool BulkDataMysql(string sqlStatement, List<Model.BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO> listToInsert, Common.Config.IPS_Config.ConnString connString)
        {
            return
                _repositoryBLL_BULKDATA.BulkDataMysql(sqlStatement, listToInsert, connString);
        }

    }
}
