﻿using IPS.Beneficios.ValiacionPagos.Service.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class BLL_CHECK_PERIODOSPROCESOS : IPS_Dummie
    {
        #region //atributes

        private Model.VAP_PARAMETROS _paramPeriodoInicioSolicitudesIngresadas;
        private List<Model.VAP_PERIODOS> _periodosList;
        private List<Model.VAP_PROCESOS> _procesosList;
        #endregion

        public BLL_CHECK_PERIODOSPROCESOS()
        {
            _paramPeriodoInicioSolicitudesIngresadas = _parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.PERIODO_INICIO_SOLICITUDES_INGRESADAS);

            _periodosList = _repositoryVAP_PERIODOS.GETALL();

            _procesosList = _repositoryVAP_PROCESOS.GETALL();
        }

        public void Main()
        {
            var _fechaDesde = Common.Helper.Generics.GetPeriodoFormatted(int.Parse(_paramPeriodoInicioSolicitudesIngresadas.PARAMETRO_VALOR));

            var periodoPerMonthList = Common.Helper.Generics.GetperiodosPerMont(_fechaDesde, _fechaProceso);

            foreach (int _periodo in periodoPerMonthList)
            {
                var _PERIODO = new Model.VAP_PERIODOS()
                {

                    PERIODO_DISPLAY = _periodo
                    ,
                    ESTADO_ID = (int)Common.Config.IPS_Config.PeriodoEstado.PENDIENTE
                    ,
                    PERIODO_ISACTIVE = 'S'
                };


                var _periodoInDataBase = _periodosList.Find(o => o.PERIODO_DISPLAY == _PERIODO.PERIODO_DISPLAY);

                if (_periodoInDataBase == default(Model.VAP_PERIODOS))
                    _repositoryVAP_PERIODOS.INSERT(_PERIODO);


                var _procesosTaskByPeriodoList = _repositoryVAP_PROCESOS_TASK.GETBY_PERIODO_ID(_periodoInDataBase == default(Model.VAP_PERIODOS) ? default(int) : _periodoInDataBase.PERIODO_ID);


                if (_procesosTaskByPeriodoList == null || _procesosTaskByPeriodoList.Count == 0)//No existe procesosTask para este periodo
                {
                    var _periodoTouse = _repositoryVAP_PERIODOS.GETBY_PERIODO_DISPLAY(_PERIODO.PERIODO_DISPLAY);

                    foreach (var _proceso in _procesosList)
                    {

                        _repositoryVAP_PROCESOS_TASK.INSERT(new Model.VAP_PROCESOS_TASK()
                        {

                            PROCESO_ID = _proceso.PROCESO_ID
                            ,
                            PERIODO_ID = _periodoTouse.PERIODO_ID
                            ,
                            ESTADO_ID = (int)Common.Config.IPS_Config.ProcesoEstado.EN_ESPERA_PROCESAMIENTO
                            ,
                            FECHA_INI = default(DateTime)
                            ,
                            FECHA_FIN = default(DateTime)
                        });
                    }
                }

            }
        }

        internal void CheckAndUpdatePeriodo(int _PERIODO_ID)
        {
            #region //Variables
            bool _allProccessAreSuccess = true;
            #endregion

            if(_PERIODO_ID != default(int))
            { 
                var _procesosTaskByPeriodoList = _repositoryVAP_PROCESOS_TASK.GETBY_PERIODO_ID(_PERIODO_ID);

                if(_procesosTaskByPeriodoList.Count > 0)
                {
                    foreach (var _procesoTask in _procesosTaskByPeriodoList)
	                {
		                if(_procesoTask.ESTADO_ID != (int)Common.Config.IPS_Config.ProcesoEstado.TERMINADO_SUCCESS)
                        {
                            _allProccessAreSuccess = false;
                            break;
                        }
	                }
                }
                else
                    _allProccessAreSuccess = false;

                if (_allProccessAreSuccess)
                {
                    _repositoryVAP_PERIODOS.UPDATE(_PERIODO_ID, (int)Common.Config.IPS_Config.PeriodoEstado.TERMINADO);

                    //var _nextItem = Common.Helper.Generics.NextOf(_periodosList, _periodosList.Where(o => o.PERIODO_ID == _PERIODO_ID).FirstOrDefault());

                    //if (_nextItem != default(Model.VAP_PERIODOS))
                    //    _repositoryVAP_PERIODOS.UPDATE(_nextItem.PERIODO_ID, (int)Common.Config.IPS_Config.PeriodoEstado.LISTO_PARAPROCESAR);
                }
            }
            
        }
    }
}