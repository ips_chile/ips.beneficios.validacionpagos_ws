﻿using IPS.Beneficios.ValiacionPagos.Service.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;


namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_DESAGREGACION_PORMES : IPS_Dummie
    {
        #region //atributes
        protected DataTable _causantesDataTable;
        protected List<Model.VAP_CAUSANTES_BASE> _reconocidosToProccessList;
        protected List<Model.VAP_CAUSANTES_BASE> _causantesRejectedList;


        protected Model.VAP_PARAMETROS _paramFecReconocimiento;
        protected Model.VAP_PARAMETROS _paramCausanteNormal;
        protected Model.VAP_PARAMETROS _paramCausanteEstudiante;
        protected Model.VAP_PARAMETROS _paramEdadCausanteNormal;
        protected Model.VAP_PARAMETROS _paramEdadCausanteEstudiante;

        protected string[] _tipoCausanteNormalArray;
        protected string[] _tipoCausanteEstudianteArray;
        protected int[] _ageEstudianteArray;

        protected bool _isEndOfList { get; set; }
        protected short _contadorArchivo;

        protected string _criteriaToSearchFile;
        protected string _folderPath;
        protected string _filePath;
        #endregion

        public VAP_DESAGREGACION_PORMES(Model.VAP_PERIODOS VAP_PERIODO /*, Common.Config.IPS_Config.DesagregacionMes desagregacionMes*/)
            : base(VAP_PERIODO)
        {
            _causantesDataTable = Common.Helper.Generics.ConfigureDataTable(new Model.VAP_CAUSANTES_MES());
            _causantesRejectedList = new List<Model.VAP_CAUSANTES_BASE>();

            _paramFecReconocimiento = _parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.FECHA_RECONOCIMIENTO);
            _paramCausanteNormal = _parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.TIPO_CAUSANTE_NORMAL);
            _paramCausanteEstudiante = _parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.TIPO_CAUSANTE_ESTUDIANTE);
            _paramEdadCausanteNormal = _parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.FILTRO_EDAD_CAUSANTE_NORMAL);
            _paramEdadCausanteEstudiante = _parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.FILTRO_EDAD_CAUSANTE_ESTUDIANTE);

            _tipoCausanteNormalArray = _paramCausanteNormal.PARAMETRO_VALOR.Split(',');
            _tipoCausanteEstudianteArray = _paramCausanteEstudiante.PARAMETRO_VALOR.Split(',');
            _ageEstudianteArray = Common.Helper.Generics.GetIntArray(_paramEdadCausanteEstudiante.PARAMETRO_VALOR);

            this._isEndOfList = false;
        }

        public DateTime GetFechaExtincionCalculada(Model.VAP_CAUSANTES_BASE reconocido)
        {
            DateTime _fechaExtincionCalculada = default(DateTime);

            if (_tipoCausanteNormalArray.Contains(reconocido.COD_TIPO_CAUSANTE.ToString()))//Existe dentro de los tiops de causante normal. TipoCausanteNormal value array
            {
                var dateAt18YearsOld = reconocido.FECHA_NACIMIENTO_CAUSANTE.AddYears(int.Parse(_paramEdadCausanteNormal.PARAMETRO_VALOR));

                _fechaExtincionCalculada = new DateTime(dateAt18YearsOld.Year, 12, 31);
            }
            else if (_tipoCausanteEstudianteArray.Contains(reconocido.COD_TIPO_CAUSANTE.ToString()))//Existe dentro de los tiops de causante estudiante. tipoCausanteEstudiante value array
            {
                var dateAt24YearsOld = reconocido.FECHA_NACIMIENTO_CAUSANTE.AddYears(_ageEstudianteArray.Last());//24 años edad

                _fechaExtincionCalculada = new DateTime(dateAt24YearsOld.Year, 12, 31);
            }
            else if (reconocido.TIPO_RECONOCIDO == "V")// Es tipo Vigente
            {
                if (_fechaExtincionCalculada > DateTime.Parse(_paramFecReconocimiento.PARAMETRO_VALOR))
                    _fechaExtincionCalculada = _fechaProceso;
            }
            else//Todos los otros registros
                _fechaExtincionCalculada = reconocido.FECHA_EXTINCION;
            return
                _fechaExtincionCalculada;
        }

        public List<DataRow> GetCausanteMes(Model.VAP_CAUSANTES_BASE _reconocido, int _ageReconocido, DateTime _fechaExtincionCalculada)
        {
            List<DataRow> dataRowList = new List<DataRow>();

            var fechaExtincionToUse = _reconocido.TIPO_RECONOCIDO == "V" ? _fechaExtincionCalculada : _reconocido.FECHA_EXTINCION;//solo si es vigente asigna f.calculada.

            var periodoList = Common.Helper.Generics.GetperiodosPerMont(_reconocido.FECHA_RECONOCIMIENTO, fechaExtincionToUse);

            foreach (int _periodo in periodoList)
            {
                DataRow row = _causantesDataTable.NewRow();

                var isCausanteDuplicatedFromDataBaseInPeriodo = IsCausanteDuplicatedInPeriodo(_reconocido, _periodo/*, _causantesFromFileList*/);


                row["RUT_BENEFICIARIO"] = _reconocido.RUT_BENEFICIARIO;
                row["DV_BENEFICIARIO"] = _reconocido.DV_BENEFICIARIO;
                row["RUT_CAUSANTE"] = _reconocido.RUT_CAUSANTE;
                row["DV_CAUSANTE"] = _reconocido.DV_CAUSANTE;
                row["CAUSANTE_EDAD"] = _ageReconocido;
                row["FECHA_INSERT"] = _reconocido.FECHA_INSERT;
                row["CAUSANTE_FEC_REC"] = _reconocido.FECHA_RECONOCIMIENTO;
                row["CAUSANTE_FEC_EXT_DEFAULT"] = _reconocido.FECHA_EXTINCION;
                row["CAUSANTE_FEC_EXT_CALC"] = fechaExtincionToUse;
                row["CAUSANTE_FEC_EXT_EDAD"] = _fechaExtincionCalculada;
                row["CAUSANTE_TIPO"] = _reconocido.COD_TIPO_CAUSANTE;
                row["CAUSANTE_PERIODO"] = _periodo;
                row["NOTIFICACION_ID"] = isCausanteDuplicatedFromDataBaseInPeriodo ? Common.Config.IPS_Config.NotificaionID.CAUSANTE_DUPLICADO_ENPERIODO : Common.Config.IPS_Config.NotificaionID.CAUSANTE_UNICO_ENPERIODO;
                row["SISTEMA_FEC_PROCESO"] = _fechaProceso;
                //row["PERIODO_PROCESO"] = _VAP_PERIODO.PERIODO_DISPLAY; //nose porque no funciona

                dataRowList.Add(row);
            }

            return
                dataRowList;
        }

        public bool IsCausanteDuplicatedInPeriodo(Model.VAP_CAUSANTES_BASE _reconocido, int _periodo)
        {
            var isDuplicatedOnDataBase = _repositoryVAP_CAUSANTES_MES.GET(_reconocido.RUT_CAUSANTE, _periodo).Count > 0;

            return
                isDuplicatedOnDataBase;
        }


        public void SetDuplicadoNotificacionIDinFile()
        {
            #region //Variables
            List<Model.VAP_CAUSANTES_MES> causantesRepeatedList = new List<Model.VAP_CAUSANTES_MES>();
            var _fileDictionary = Common.Helper.FilesHelper.GetFiles(this._folderPath, this._criteriaToSearchFile + "*");
            var delimiter = "|";
            #endregion

            #region //Recorre archivos
            foreach (var file in _fileDictionary)
            {
                #region //Agrupa causantes repetidos
                string[] allLines = File.ReadAllLines(@file);

                foreach (var line in allLines)
                {
                    var data = line.Split('|');

                    var causanteToLook = new Model.VAP_CAUSANTES_MES()
                    {
                        RUT_CAUSANTE = string.IsNullOrEmpty(data[2]) ? default(int) : int.Parse(data[2])//RUT_CAUSANTE
                        ,
                        CAUSANTE_PERIODO = string.IsNullOrEmpty(data[11]) ? default(int) : int.Parse(data[11])//CAUSANTE_PERIODO
                    };

                    var causantesList = GetCausantesFromFile(causanteToLook, allLines);

                    if (causantesList.Count > 1)//Esta repetido mas de una vez
                        causantesRepeatedList.AddRange(causantesList);

                }

                var groupedList = causantesRepeatedList.GroupBy(o => new { o.RUT_CAUSANTE, o.CAUSANTE_PERIODO }).Select(o => new { RUT_CAUSANTE = o.Key.RUT_CAUSANTE, CAUSANTE_PERIODO = o.Key.CAUSANTE_PERIODO }).ToList();
                #endregion

                #region //Setea causantes repetidos en archivo

                    foreach (var causanteRepeated in groupedList)
                    {
                        var tempPath = Path.GetTempFileName();

                        using (var writer = new StreamWriter(tempPath, true))
                        using (var reader = new StreamReader(new FileStream(@file, FileMode.Open)))
                        {
                            while (!reader.EndOfStream)
                            {
                                var line = reader.ReadLine();

                                string[] lineFields = line.Split(char.Parse(delimiter), '\t');

                                if (lineFields[2] == causanteRepeated.RUT_CAUSANTE.ToString() && lineFields[11] == causanteRepeated.CAUSANTE_PERIODO.ToString())
                                    lineFields[12] = ((int)Common.Config.IPS_Config.NotificaionID.CAUSANTE_DUPLICADO_ENPERIODO).ToString();

                                writer.WriteLine(string.Join(delimiter, lineFields));
                            }
                        }

                        File.Delete(file);
                        File.Move(tempPath, file);
                    }


                #endregion
            }
            #endregion
        }

        public List<Model.VAP_CAUSANTES_MES> GetCausantesFromFile(Model.VAP_CAUSANTES_MES reconocido, string[] allLines)
        {
            List<Model.VAP_CAUSANTES_MES> reconocidoList = new List<Model.VAP_CAUSANTES_MES>();

            var query = (from line in allLines

                         let data = line.Split('|')

                         select new Model.VAP_CAUSANTES_MES()
                         {
                             RUT_CAUSANTE = string.IsNullOrEmpty(data[2]) ? default(int) : int.Parse(data[2])//RUT_CAUSANTE
                             ,
                             CAUSANTE_PERIODO = string.IsNullOrEmpty(data[11]) ? default(int) : int.Parse(data[11])//CAUSANTE_PERIODO
                         })
                        .Where(o => o.RUT_CAUSANTE == reconocido.RUT_CAUSANTE && o.CAUSANTE_PERIODO == reconocido.CAUSANTE_PERIODO)
                        .ToList();

            reconocidoList.AddRange(query);

            return
                reconocidoList;
        }
    }
}
