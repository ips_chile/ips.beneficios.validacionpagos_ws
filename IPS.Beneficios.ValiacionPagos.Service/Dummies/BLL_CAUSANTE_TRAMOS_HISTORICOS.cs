﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repository = IPS.Beneficios.ValiacionPagos.DataAccess.Repository;
using Model = IPS.Beneficios.ValiacionPagos.Model;
using IPS.Beneficios.ValiacionPagos.Service.Base;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class BLL_CAUSANTE_TRAMOS_HISTORICOS
    {
        Repository.BLL_CAUSANTE_TRAMOS_HISTORICOS _repositoryBLL_CAUSANTE_TRAMOS_HISTORICOS;

        public BLL_CAUSANTE_TRAMOS_HISTORICOS()
        {
            _repositoryBLL_CAUSANTE_TRAMOS_HISTORICOS = new Repository.BLL_CAUSANTE_TRAMOS_HISTORICOS();
        }

        public int GET_IdTramo(Model.VAP_CAUSANTES_MES causante, int _periodo)
        {
            int idTramo = default(int);
            var causanteTramoHistoricoList = _repositoryBLL_CAUSANTE_TRAMOS_HISTORICOS.GET(causante);

            if (causanteTramoHistoricoList != null)
            {
                foreach (var causanteItem in causanteTramoHistoricoList)// asigna fecha extincion del causante
                {
                    if (causanteItem.FECHA_EXTINCION == default(DateTime))//ES VIGENTE
                        causanteItem.FECHA_EXTINCION = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd"));// Fecha de proceso, CAMBIAR A COMMON CONFIG;
                }

                var causanteTramoToTake = causanteTramoHistoricoList
                    .Where(o => causante.CAUSANTE_PERIODO >= int.Parse(o.FECHA_RECONOCIMIENTO.ToString("yyyyMM"))
                                && causante.CAUSANTE_PERIODO <= int.Parse(o.FECHA_EXTINCION.ToString("yyyyMM")))
                    .FirstOrDefault();

                if (causanteTramoToTake != default(Model.BLL_CAUSANTE_TRAMOS_HISTORICOS))
                {
                    switch (_periodo)
                    {
                        case 2008:
                            idTramo = causanteTramoToTake.TRAMO2008;
                            break;
                        case 2009:
                            idTramo = causanteTramoToTake.TRAMO2009;
                            break;
                        case 2010:
                            idTramo = causanteTramoToTake.TRAMO2010;
                            break;
                        case 2011:
                            idTramo = causanteTramoToTake.TRAMO2011;
                            break;
                        case 2012:
                            idTramo = causanteTramoToTake.TRAMO2012;
                            break;
                        case 2013:
                            idTramo = causanteTramoToTake.TRAMO2013;
                            break;
                        case 2014:
                            idTramo = causanteTramoToTake.TRAMO2014;
                            break;
                        case 2015:
                            idTramo = causanteTramoToTake.TRAMO2015;
                            break;
                    }
                }
            }

            return
                idTramo;
        }
    }
}
