﻿using IPS.Beneficios.ValiacionPagos.Service.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_CAUSANTES_MES : VAP_DESAGREGACION_PORMES
    {
        #region //atributes

        private int _contadorArchivo;
        #endregion

        public VAP_CAUSANTES_MES(Model.VAP_PERIODOS VAP_PERIODO)
            : base(VAP_PERIODO)
        {
            var monthName = Common.Helper.Generics.GetMonthName(_VAP_PERIODO.PERIODO_DISPLAY.ToString());

            this._criteriaToSearchFile = string.Format("IPS_RECONOCIDOS_{0}", _VAP_PERIODO.PERIODO_DISPLAY);
            this._folderPath = GetFolder("IPS_RECONOCIDOS", Common.Config.IPS_Config.desagragacionMesFolder, _VAP_PERIODO);
            this._filePath = GetFilePath(_folderPath, "IPS_RECONOCIDOS", _VAP_PERIODO);

            _contadorArchivo = 0;
        }

        public bool Main()
        {
            var wasFileCleanded = Common.Helper.FilesHelper.CleanFolder("IPS_RECONOCIDOS_" + _VAP_PERIODO.PERIODO_DISPLAY, this._folderPath);

            if (wasFileCleanded)
            {
                var extinguidoList = new Service.Dummies.VAP_CAUSANTES_BASE().GET_EXTINGUIDOS(_VAP_PERIODO.PERIODO_DISPLAY);
                var vigenteList = new Service.Dummies.VAP_CAUSANTES_BASE().GET_VIGENTES(_VAP_PERIODO.PERIODO_DISPLAY);

                var solicitudesList = new Service.Dummies.VAP_SOLICITUDES_BASE().GET(_VAP_PERIODO.PERIODO_DISPLAY);

                //Filtra todos los potenciales reconocidos que pertenezcan a los trabajadores de Previred
                this._reconocidosToProccessList = extinguidoList
                                                .Union(vigenteList)
                                                .Where(i => solicitudesList.Select(o => o.RUT_TRABAJADOR)
                                                    .Contains(i.RUT_BENEFICIARIO)).Select(o => o)
                                                    .ToList();

                this.ProcesaReconocidos();//Seria bueno que devolviera boleano para saber si proceso correctamente.

                return
                    ProccessFiles(_VAP_PERIODO);
            }
            else
                return false;
        }

        #region //PRIVATE
        private void ProcesaReconocidos()
        {
            Int32 listCount = 0;

            foreach (var reconocido in this._reconocidosToProccessList)
            {
                #region //Check cant elements and load if theses are greater than setted limit
                var isEndOfList = Common.Helper.FilesHelper.CheckEndOfList(listCount, this._reconocidosToProccessList.Count, 10);
                Common.Helper.FilesHelper.CheckQuantityAndFillCSV(this._filePath, "csv", "|", isEndOfList, 1000, ref _causantesDataTable, ref _contadorArchivo);
                #endregion

                #region //Private bucle vars
                int _ageReconocido = Common.Helper.Generics.GetAge(reconocido.FECHA_NACIMIENTO_CAUSANTE);
                DateTime _fechaExtincionCalculada = GetFechaExtincionCalculada(reconocido);
                #endregion

                #region //Add data to common arrayList

                var reconocidoMesArray = GetCausanteMes(reconocido, _ageReconocido, _fechaExtincionCalculada);

                if (reconocidoMesArray != null && reconocidoMesArray.Count > 0)

                    reconocidoMesArray.CopyToDataTable(_causantesDataTable, LoadOption.PreserveChanges);
                else
                    _causantesRejectedList.Add(reconocido);

                #endregion

                listCount++;

            }

            SetDuplicadoNotificacionIDinFile();
        }

        private bool ProccessFiles(Model.VAP_PERIODOS VAP_PERIODO)
        {
            Model.VAP_FILE VAP_FTPFILE = new Model.VAP_FILE()
            {
                ARCHIVORUTA = this._folderPath
                 ,
                PERIODO = VAP_PERIODO.PERIODO_DISPLAY
                ,
                ARCHIVONOMBRE = this._criteriaToSearchFile
                ,
                EXTENSION = ".CSV"
            };

            var _fileDictionary = Common.Helper.FilesHelper.GetFiles(VAP_FTPFILE.ARCHIVORUTA, VAP_FTPFILE.ARCHIVONOMBRE + "*");

            var _successCount = _fileDictionary.Count();//archivos sin errores

            if (_successCount == 0) _successCount = -1;

            foreach (var filePath in _fileDictionary)
            {
                var _tableName = typeof(Model.VAP_CAUSANTES_MES).Name;

                var _fileName = Path.GetFileNameWithoutExtension(filePath);

                var _bulkedData = _serviceVAP_BULKDATA.BulkData(new Model.SQL_LOADER()
                {
                    TABLE_NAME = _tableName
                    ,
                    FILEPATH = filePath
                    ,
                    CLEANTABLE = true
                    ,
                    CLEANTABLE_QUERY = string.Format("DELETE VALIDACION_PAGOS.{0} WHERE PERIODO_PROCESO = {1}", _tableName, VAP_PERIODO.PERIODO_DISPLAY)
                    ,
                    CTLPATH = string.Format(@"{0}LOADERS\CTL\{1}.ctl", Common.Config.IPS_Config.installDirApp, _tableName)
                    ,
                    LOGPATH = string.Format(@"{0}LOADERS\LOG\{1}.log", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    BADPATH = string.Format(@"{0}LOADERS\LOG\{1}.bad", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    DISCARDPATH = string.Format(@"{0}LOADERS\LOG\{1}.dsc", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    DICTIONARY_SETCONTROLFILEVALUES = new Dictionary<string, string>() { {"PERIODO_PROCESO", string.Format("{0}  constant {1}", "PERIODO_PROCESO", VAP_FTPFILE.PERIODO)}
                                                                                        ,{"ARCHIVO_IDCARGA", string.Format("{0}  constant \"{1}{2}\"", "ARCHIVO_IDCARGA", _fileName, VAP_FTPFILE.EXTENSION) }}
                });

                if (_bulkedData)
                    _successCount--;
            }

            return
                _successCount == 0;// Shoulb be 0 if all bulked files run successfully
        }


        //private List<Model.VAP_CAUSANTES_BASE> GetExtinguidos()
        //{
        //    var paramFecReconocimiento =  _parametroList.FirstOrDefault(o=>o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.FECHA_RECONOC_EXTINGUIDOS);

        //    var extinguidoList = _repositoryVAP_CAUSANTES_BASE.GET(DateTime.Parse(paramFecReconocimiento.PARAMETRO_VALOR), paramFecReconocimiento.PARAMETRO_CONSTRAIN, _archivoIDcargaLike, "E");

        //    return
        //        extinguidoList;
        //}

        //private List<Model.VAP_CAUSANTES_BASE> GetVigentes()
        //{
        //    var vigenteList = _repositoryVAP_CAUSANTES_BASE.GET(_archivoIDcargaLike, "V");

        //    return
        //        vigenteList;
        //}

        //private bool CheckQuantityAndFillCSV()
        //{
        //    var filePath = string.Format(@"{0}\IPS_RECONOCIDOS_{1}_{2}.csv", Common.Config.IPS_Config.desagragacionMesFolder, _VAP_PERIODO.PERIODO_DISPLAY, _contadorArchivo);

        //    var sizeMB = Common.Helper.FilesHelper.GetFileSize(filePath);

        //    if (sizeMB >= 1000)
        //        filePath = string.Format(@"{0}\IPS_RECONOCIDOS_{1}_{2}.csv", Common.Config.IPS_Config.desagragacionMesFolder, _VAP_PERIODO.PERIODO_DISPLAY, _contadorArchivo++);

        //    var wasFilled = false;

        //    if (_causantesDTtoCSV.Rows.Count >= 100000)
        //    {
        //        wasFilled = Common.Helper.FilesHelper.FillCSV(_causantesDTtoCSV, filePath, ",");

        //        _causantesDTtoCSV.Clear();
        //    }// preguntar por los menores a 100.000, y si se esta cerca del final, si es asi añadir al csv.

        //    return
        //        wasFilled;
        //}

        #endregion


    }
}
