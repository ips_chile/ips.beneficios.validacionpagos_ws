﻿using IPS.Beneficios.ValiacionPagos.Service.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class BLL_DISTRIBUCION_PAGOS : IPS_Dummie
    {
        #region //atributes
        private DataTable _causantesDataTable;
        private DataTable _beneficiariosDataTable;

        private List<Model.VAP_INGRESO_PROMEDIO> _ingresoPromedioList = new List<Model.VAP_INGRESO_PROMEDIO>();
        private List<Model.VAP_VERIFPAGO_BENEFICIARIOS> _rejectedDistPagoBeneficiarioList = new List<Model.VAP_VERIFPAGO_BENEFICIARIOS>();
        private List<Model.VAP_VERIFPAGO_CAUSANTES> _rejectedDistPagoCausanteList = new List<Model.VAP_VERIFPAGO_CAUSANTES>();
        private List<Model.VAP_SOLICITUDES_BASE> _solicitudesPeriodoList = new List<Model.VAP_SOLICITUDES_BASE>();

        private Model.VAP_PARAMETROS _paramDiasAProcesar;
        private string _folderPath_DistPagoCausante;
        private string _folderPath_DistPagoBeneficiario;
        private string _filePath_DistPagoCausante;
        private string _filePath_DistPagoBeneficiario;

        private int _contadorArchivoCausantes;
        private int _contadorArchivoBeneficiarios;
        #endregion

        public BLL_DISTRIBUCION_PAGOS(Model.VAP_PERIODOS VAP_PERIODO)
            : base(VAP_PERIODO)
        {
            _contadorArchivoCausantes = 0;
            _contadorArchivoBeneficiarios = 0;

            _causantesDataTable = Common.Helper.Generics.ConfigureDataTable(new Model.VAP_VERIFPAGO_CAUSANTES());
            _beneficiariosDataTable = Common.Helper.Generics.ConfigureDataTable(new Model.VAP_VERIFPAGO_BENEFICIARIOS());

            _folderPath_DistPagoCausante = GetFolder("IPS_SOLICITADOS", Common.Config.IPS_Config.distribucionPagosMesFolder + "\\Causantes", _VAP_PERIODO);
            _folderPath_DistPagoBeneficiario = GetFolder("IPS_SOLICITADOS", Common.Config.IPS_Config.distribucionPagosMesFolder + "\\Beneficiarios", _VAP_PERIODO);

            _filePath_DistPagoCausante = GetFilePath(_folderPath_DistPagoCausante, "IPS_DISTRIBUCIONPAGOS_CAUSANTES", _VAP_PERIODO);
            _filePath_DistPagoBeneficiario = GetFilePath(_folderPath_DistPagoBeneficiario, "IPS_DISTRIBUCIONPAGOS_BENEFICIARIOS", _VAP_PERIODO);

            _paramDiasAProcesar = _parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.DIAS_FORMULA_CALCULO_PROPORCIONAL);

            //_archivoIDcargaSolicitudesPeriodoLike = string.Format("CRI{0}", _VAP_PERIODO.PERIODO_DISPLAY);
            _solicitudesPeriodoList = _repositoryVAP_SOLICITUDES_BASE.GET(_VAP_PERIODO.PERIODO_DISPLAY);

            _ingresoPromedioList = _repositoryVAP_INGRESO_PROMEDIO.GET(_VAP_PERIODO.PERIODO_DISPLAY);
        }

        public bool Main()
        {
            var wasCleandedFolderCausante = Common.Helper.FilesHelper.CleanFolder("IPS_DISTRIBUCIONPAGOS_CAUSANTES_" + _VAP_PERIODO.PERIODO_DISPLAY, _folderPath_DistPagoCausante);
            var wasCleandedFolderBeneficiario = Common.Helper.FilesHelper.CleanFolder("IPS_DISTRIBUCIONPAGOS_BENEFICIARIOS_" + _VAP_PERIODO.PERIODO_DISPLAY, _folderPath_DistPagoBeneficiario);


            if (wasCleandedFolderCausante && wasCleandedFolderBeneficiario)
            {
                ProcesaDistribucionPagos();

                return
                    ProccessFiles(_VAP_PERIODO);
            }
            else
                return false;
        }

        #region //PRIVATE

        private void ProcesaDistribucionPagos()
        {
            #region //Variables genericas metodo
            Int32 listCount = 0;
            List<Model.VAP_VERIFPAGO_BENEFICIARIOS> _distPagoBeneficiariosList = new List<Model.VAP_VERIFPAGO_BENEFICIARIOS>();
            List<Model.VAP_VERIFPAGO_CAUSANTES> _distPagoCausantesList = new List<Model.VAP_VERIFPAGO_CAUSANTES>();

            #endregion

            //Recorre las solicitudes
            foreach (var solicitud in _solicitudesPeriodoList)
            {
                #region //Clean List`s
                _distPagoBeneficiariosList.Clear();
                _distPagoCausantesList.Clear();
                #endregion

                #region //Variables por solicitud
                //var numeroCargasSolicitud = solicitud.NUMERO_CARGAS;

                var fechaDesdeSolicitudToUse = GetFechaDesdeSolicitud(solicitud);
                solicitud.FECHA_INICIO_COMPENSACION = fechaDesdeSolicitudToUse;

                var periodosSolicitudArray = Common.Helper.Generics.GetperiodosPerMont(solicitud.FECHA_INICIO_COMPENSACION, solicitud.FECHA_FIN_COMPENSACION);

                var _causantesSIAGFBySolicitudList = GetCausantes_SIAGF(solicitud);

                //var _causantesMayorPermanencia = ApplyRule_CausantesMayorPermanencia(_causantesSIAGFBySolicitudList);//No se usa
                var _causantesByFechaInsertList = ApplyRule_CausantesFechaIngreso(_causantesSIAGFBySolicitudList);
                var _causantesByRutMenorList = ApplyRule_CausantesRutMenor(_causantesSIAGFBySolicitudList);
                #endregion

                #region //Recorre todos los periodos de la solicitud
                foreach (var periodo in periodosSolicitudArray)
                {
                    #region //Variables por periodo
                    var _causantesSIAGFByPeriodo = _causantesSIAGFBySolicitudList.Where(o => o.CAUSANTE_PERIODO == periodo).ToList();
                    var _valorSumaCausantesPeriodo = GetSumaMontoAFCausantes(_causantesSIAGFByPeriodo, solicitud);
                    var _valorPlanillaPeriodo = _repositoryBLL_PAGOS.GET(solicitud.RUT_TRABAJADOR, periodo);
                    Common.Config.IPS_Config.NotificaionID _notificacionID;
                    #endregion

                    #region //Check cant elements and load if theses are greater than setted limit
                    var isEndOfList_Causantes = Common.Helper.FilesHelper.CheckEndOfList(listCount, _solicitudesPeriodoList.Count, 3);
                    Common.Helper.FilesHelper.CheckQuantityAndFillCSV(_filePath_DistPagoCausante, "csv", "|", isEndOfList_Causantes, 100, ref _causantesDataTable, ref _contadorArchivoCausantes);

                    var isEndOfList_Beneficiarios = Common.Helper.FilesHelper.CheckEndOfList(listCount, _solicitudesPeriodoList.Count, 3);
                    Common.Helper.FilesHelper.CheckQuantityAndFillCSV(_filePath_DistPagoBeneficiario, "csv", "|", isEndOfList_Beneficiarios, 100, ref _beneficiariosDataTable, ref _contadorArchivoBeneficiarios);
                    #endregion



                    #region //Distribuye segun diferencia de montos para el periodo

                    #region //Distribuye pago a causantes
                    var verifPagoCausanteList = AsignaPago_Causantes(_causantesSIAGFByPeriodo, _valorPlanillaPeriodo, solicitud, periodo, _causantesByFechaInsertList, _causantesByRutMenorList);

                    _distPagoCausantesList.AddRange(verifPagoCausanteList);
                    #endregion

                    #endregion

                    #region //Distribuye pago a trabajador-beneficiario
                    var cantidadCausantesSinPago = verifPagoCausanteList.Where(o => o.VALORAF_PENDIENTE > 0).Count();
                    var valorPagado = verifPagoCausanteList.Sum(o => o.VALORAF_PAGADO);

                    var valorPendiente = _valorPlanillaPeriodo.MONTO >= _valorSumaCausantesPeriodo ? ((_valorPlanillaPeriodo.MONTO - _valorSumaCausantesPeriodo) * -1) : (_valorSumaCausantesPeriodo - _valorPlanillaPeriodo.MONTO);

                    _notificacionID = GetNotificacionTrabajador(verifPagoCausanteList, _valorPlanillaPeriodo.MONTO, valorPendiente);

                    var verifPagoBeneficiario = AsignaPago_Trabajador(solicitud, periodo, _notificacionID, _valorPlanillaPeriodo.MONTO, valorPagado, valorPendiente, cantidadCausantesSinPago);
                    _distPagoBeneficiariosList.Add(verifPagoBeneficiario);

                    #endregion

                #endregion

                }
        #endregion

                #region //Add data to common arrayList
                var distPagoCausantesArray = Common.Helper.FilesHelper.ConvertToDataTable(_distPagoCausantesList);
                var distPagoBeneficiariosArray = Common.Helper.FilesHelper.ConvertToDataTable(_distPagoBeneficiariosList);

                if (distPagoCausantesArray != null && distPagoCausantesArray.Rows.Count > 0)
                    _causantesDataTable.Merge(distPagoCausantesArray);
                else
                    _rejectedDistPagoCausanteList.AddRange(_distPagoCausantesList);// Trabajador no tiene cargas reconocidas


                if (distPagoBeneficiariosArray != null && distPagoBeneficiariosArray.Rows.Count > 0)
                    _beneficiariosDataTable.Merge(distPagoBeneficiariosArray);
                else
                    _rejectedDistPagoBeneficiarioList.AddRange(_distPagoBeneficiariosList);
                #endregion

                listCount++;
            }
        }

        #region //SQL Loaders Methods
        private bool ProccessFiles(Model.VAP_PERIODOS VAP_PERIODO)
        {
            var criteriaToSearch_Causantes = string.Format("IPS_DISTRIBUCIONPAGOS_CAUSANTES_{0}", VAP_PERIODO.PERIODO_DISPLAY);

            var criteriaToSearch_Beneficiarios = string.Format("IPS_DISTRIBUCIONPAGOS_BENEFICIARIOS_{0}", VAP_PERIODO.PERIODO_DISPLAY);

            Model.VAP_FILE VAP_FILE_CAUSANTE = new Model.VAP_FILE()
            {
                ARCHIVORUTA = _folderPath_DistPagoCausante
                 ,
                PERIODO = VAP_PERIODO.PERIODO_DISPLAY
                ,
                ARCHIVONOMBRE = criteriaToSearch_Causantes
                ,
                EXTENSION = ".CSV"
            };

            Model.VAP_FILE VAP_FILE_BENEFICIARIO = new Model.VAP_FILE()
            {
                ARCHIVORUTA = _folderPath_DistPagoBeneficiario
                 ,
                PERIODO = VAP_PERIODO.PERIODO_DISPLAY
                ,
                ARCHIVONOMBRE = criteriaToSearch_Beneficiarios
                ,
                EXTENSION = ".CSV"
            };

            bool verifPagoCausantes_Success = RunSQLLoader(VAP_FILE_CAUSANTE, VAP_PERIODO, typeof(Model.VAP_VERIFPAGO_CAUSANTES).Name);

            bool verifPagoBeneficiarios_Success = RunSQLLoader(VAP_FILE_BENEFICIARIO, VAP_PERIODO, typeof(Model.VAP_VERIFPAGO_BENEFICIARIOS).Name);

            return
                verifPagoCausantes_Success && verifPagoBeneficiarios_Success;
        }

        private bool RunSQLLoader(Model.VAP_FILE VAP_FILE, Model.VAP_PERIODOS VAP_PERIODO, string _tableName)
        {
            var _fileDictionary = Common.Helper.FilesHelper.GetFiles(VAP_FILE.ARCHIVORUTA, VAP_FILE.ARCHIVONOMBRE + "*");

            var _successCount = _fileDictionary.Count();//archivos sin errores

            if (_successCount == 0) _successCount = -1;

            foreach (var filePath in _fileDictionary)
            {
                var _fileName = Path.GetFileNameWithoutExtension(filePath);

                var _bulkedData = _serviceVAP_BULKDATA.BulkData(new Model.SQL_LOADER()
                {
                    TABLE_NAME = _tableName
                    ,
                    FILEPATH = filePath
                    ,
                    CLEANTABLE = true
                    ,
                    CLEANTABLE_QUERY = string.Format("DELETE VALIDACION_PAGOS.{0} WHERE PERIODO_PROCESO = {1}", _tableName, VAP_PERIODO.PERIODO_DISPLAY)
                    ,
                    CTLPATH = string.Format(@"{0}LOADERS\CTL\{1}.ctl", Common.Config.IPS_Config.installDirApp, _tableName)
                    ,
                    LOGPATH = string.Format(@"{0}LOADERS\LOG\{1}.log", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    BADPATH = string.Format(@"{0}LOADERS\LOG\{1}.bad", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    DISCARDPATH = string.Format(@"{0}LOADERS\LOG\{1}.dsc", Common.Config.IPS_Config.installDirApp, _fileName)
                    ,
                    DICTIONARY_SETCONTROLFILEVALUES = new Dictionary<string, string>() { {"ARCHIVO_IDCARGA", string.Format("{0}  constant \"{1}{2}\"", "ARCHIVO_IDCARGA", _fileName, VAP_FILE.EXTENSION) }
                                                                                        ,{"PERIODO_PROCESO", string.Format("{0}  constant {1}", "PERIODO_PROCESO", VAP_FILE.PERIODO)}}
                });

                if (_bulkedData)
                    _successCount--;
            }

            return
                _successCount == 0;// Shoulb be 0 if all bulked files run successfully
        }
        #endregion

        #region //Get data to be procceced

        private Common.Config.IPS_Config.NotificaionID GetNotificacionTrabajador(List<Model.VAP_VERIFPAGO_CAUSANTES> causantePeriodoList, long _montoArchivo12Periodo, long _valorPendiente)
        {
            Common.Config.IPS_Config.NotificaionID notificacionID;

            if (causantePeriodoList.Count > 0)
                notificacionID = _montoArchivo12Periodo != default(long)
                        ? (_valorPendiente > 0 ? Common.Config.IPS_Config.NotificaionID.TRABAJBENEF_DIFERENCIA_MONTOS : Common.Config.IPS_Config.NotificaionID.TRABAJBENEF_MONTOS_IGUALES)
                        : Common.Config.IPS_Config.NotificaionID.TRABAJADOR_PERIODO_NOEXISTE_EN_PLANILLA;
            else
                notificacionID = Common.Config.IPS_Config.NotificaionID.TRABAJADOR_NOTIENE_CAUSANTES_ENPERIODO;

            return
                notificacionID;
        }
        private Common.Config.IPS_Config.NotificaionID GetNotificacionCausante(string isPendientePago, long _montoApagarCausante)
        {
            Common.Config.IPS_Config.NotificaionID notificacionID;

            if (_montoApagarCausante == 0 && isPendientePago == "S")
                notificacionID = Common.Config.IPS_Config.NotificaionID.CAUSANTE_PAGO_PENDIENTE_SIN_RELACION_LABORAL;
            else
                notificacionID = isPendientePago == "S" ? Common.Config.IPS_Config.NotificaionID.CAUSANTE_PAGO_PENDIENTE_CON_RELACION_LABORAL : Common.Config.IPS_Config.NotificaionID.CAUSANTE_PAGADO_EXITOSAMENTE;

            return
                notificacionID;
        }


        private long GetCalculoProporcional(Model.VAP_INGRESO_PROMEDIO causanteIngPromedio, Model.VAP_SOLICITUDES_BASE solicitud)
        {
            long calculoProporcional = 0;
            int diasAProcesar = GetDiasAProcesar(causanteIngPromedio, solicitud);

            var isMenorADiasAProcesar = VAP_PARAMETROS.ApplyConstrain(diasAProcesar, int.Parse(_paramDiasAProcesar.PARAMETRO_VALOR), _paramDiasAProcesar.PARAMETRO_CONSTRAIN, typeof(Int32));

            try
            {
                if (isMenorADiasAProcesar)
                    calculoProporcional = long.Parse(Math.Round(double.Parse(((causanteIngPromedio.CAUSANTE_INGPROMEDIO / 30) * diasAProcesar).ToString())).ToString());
                else
                    calculoProporcional = causanteIngPromedio.CAUSANTE_INGPROMEDIO;
            }
            catch (Exception)
            {
            }

            return
                calculoProporcional;
        }

        private List<Model.VAP_INGRESO_PROMEDIO> GetCausantesIngPromedio_ByBeneficiario(Int32 _RUT_BENEFICIARIO, int _PERIODO)
        {
            return
                _ingresoPromedioList.Where(o => o.RUT_BENEFICIARIO == _RUT_BENEFICIARIO && o.CAUSANTE_PERIODO == _PERIODO).ToList();
        }

        public List<Model.VAP_INGRESO_PROMEDIO> GetCausantes_SIAGF(Model.VAP_SOLICITUDES_BASE solicitud)
        {
            var fechaDesdeSolicitud = solicitud.FECHA_INICIO_COMPENSACION;
            var fecHastaSolicitud = solicitud.FECHA_FIN_COMPENSACION;

            var causantesList = _ingresoPromedioList
                .Where(o => o.RUT_BENEFICIARIO == solicitud.RUT_TRABAJADOR
                       && (o.CAUSANTE_PERIODO >= int.Parse(fechaDesdeSolicitud.ToString("yyyyMM"))
                            && o.CAUSANTE_PERIODO <= int.Parse(fecHastaSolicitud.ToString("yyyyMM"))))
                       .ToList();

            return
                causantesList;

        }

        private long GetSumaMontoAFCausantes(List<Model.VAP_INGRESO_PROMEDIO> causanteIngPromedioList, Model.VAP_SOLICITUDES_BASE solicitud)
        {
            long totalSuma = 0;

            foreach (var causanteIngPromedio in causanteIngPromedioList)
                totalSuma += GetCalculoProporcional(causanteIngPromedio, solicitud);

            return
                totalSuma;
        }
        #endregion

        #region //Completa listas Beneiciario/Causantes
        private Model.VAP_VERIFPAGO_CAUSANTES Get_CausanteVerifPago(
                                                                    Model.VAP_INGRESO_PROMEDIO causanteIngProm
                                                                    , Common.Config.IPS_Config.NotificaionID notificacionID
                                                                    , Model.VAP_SOLICITUDES_BASE solicitud = default(Model.VAP_SOLICITUDES_BASE)
                                                                    , long _valorPlanillaCausante = default(long)
                                                                    , long _retroactivoAcumulado_aRegistrar = default(long)
                                                                    , long _montoApagarCausante = default(long)
                                                                    , long _montoPendienteCausante = default(long)
                                                                    , string pendientePago = null)
        {

            return
                new Model.VAP_VERIFPAGO_CAUSANTES()
                {
                    FOLIO_SOLICITUD = solicitud.FOLIO
                    ,
                    RUT_EMPLEADOR = solicitud.RUT_EMPLEADOR
                    ,
                    DV_EMPLEADOR = solicitud.DV_EMPLEADOR
                    ,
                    RUT_BENEFICIARIO = causanteIngProm.RUT_BENEFICIARIO
                    ,
                    DV_BENEFICIARIO = causanteIngProm.DV_BENEFICIARIO
                    ,
                    RUT_CAUSANTE = causanteIngProm.RUT_CAUSANTE
                    ,
                    DV_CAUSANTE = causanteIngProm.DV_CAUSANTE
                    ,
                    CAUSANTE_PENDIENTEPAGO = pendientePago
                    ,
                    CAUSANTE_PERIODO = causanteIngProm.CAUSANTE_PERIODO
                    ,
                    VALORAF_DEFAULT = causanteIngProm.CAUSANTE_INGPROMEDIO// Valor Tramo
                    ,
                    VALOR_ARCHIVO12 = _valorPlanillaCausante//Valor Planilla
                    ,
                    RETROACTIVO_ACUMULADO = _retroactivoAcumulado_aRegistrar
                    ,
                    VALORAF_PAGADO = _montoApagarCausante
                    ,
                    VALORAF_PENDIENTE = _montoPendienteCausante
                    ,
                    FECHA_INSERT = causanteIngProm.FECHA_INSERT
                    ,
                    CAUSANTE_FEC_REC = causanteIngProm.CAUSANTE_FEC_REC
                    ,
                    CAUSANTE_FEC_EXT_CALC = causanteIngProm.CAUSANTE_FEC_EXT_CALC
                    ,
                    CAUSANTE_DIAS_SIAGF = GetDiasSIAGF(causanteIngProm)
                    ,
                    CAUSANTE_DIAS_SOLICITADOS = GetDiasSolicitados(solicitud, causanteIngProm)
                    ,
                    CAUSANTE_DIAS_TRABAJADOS = GetDiasTrabajados(causanteIngProm)
                    ,
                    DIAS_A_PROCESAR = GetDiasAProcesar(causanteIngProm, solicitud)
                    ,
                    NOTIFICACION_ID = (int)notificacionID
                    ,
                    SISTEMA_FEC_PROCESO = _fechaProceso
                    ,
                    ARCHIVO_IDCARGA = string.Empty
                    ,
                    TRAMO = _ingresoPromedioList.Count > 0
                             ? _ingresoPromedioList
                                    .Where(o => o.RUT_BENEFICIARIO == causanteIngProm.RUT_BENEFICIARIO && o.RUT_CAUSANTE == causanteIngProm.RUT_CAUSANTE && o.CAUSANTE_PERIODO == causanteIngProm.CAUSANTE_PERIODO)
                                    .Select(o => o.TRAMO).SingleOrDefault()
                            : 0
                };
        }
        #endregion

        #region //Distribuye fondos

        private Model.VAP_VERIFPAGO_BENEFICIARIOS AsignaPago_Trabajador(
                                                                        Model.VAP_SOLICITUDES_BASE solicitud
                                                                        , int periodo
                                                                        , Common.Config.IPS_Config.NotificaionID notificacionID
                                                                        , long valorDefault = default(long)
                                                                        , long valorPagado = default(long)
                                                                        , long valorPendiente = default(long)
                                                                        , int cantidadCausantesSinPago = default(int))
        {

            return
                new Model.VAP_VERIFPAGO_BENEFICIARIOS()
                                {

                                    FOLIO_SOLICITUD = solicitud.FOLIO
                                    ,
                                    RUT_EMPLEADOR = solicitud.RUT_EMPLEADOR
                                  ,
                                    DV_EMPLEADOR = solicitud.DV_EMPLEADOR
                                  ,
                                    RUT_BENEFICIARIO = solicitud.RUT_TRABAJADOR
                                  ,
                                    DV_BENEFICIARIO = solicitud.DV_TRABAJADOR
                                  ,
                                    VALORAF_DEFAULT = valorDefault
                                  ,
                                    VALORAF_PAGADO = valorPagado
                                  ,
                                    VALORAF_PENDIENTE = valorPendiente
                                  ,
                                    BENEFICIARIO_PERIODO = periodo
                                  ,
                                    CAUSANTES_PENDIENTEPAGO = cantidadCausantesSinPago
                                  ,
                                    NOTIFICACION_ID = (int)notificacionID
                                  ,
                                    SISTEMA_FEC_PROCESO = _fechaProceso
                                  ,
                                    PERIODO_PROCESO = _VAP_PERIODO.PERIODO_DISPLAY
                                  ,
                                    ARCHIVO_IDCARGA = string.Empty
                                };

        }

        private List<Model.VAP_VERIFPAGO_CAUSANTES> AsignaPago_Causantes(List<Model.VAP_INGRESO_PROMEDIO> _causantesSIAGFByPeriodoList, Model.BLL_PAGO _valorPlanillaPeriodo, Model.VAP_SOLICITUDES_BASE solicitud, int periodo, List<int> _causantesFechaInsert = default(List<int>), List<int> _causantesRutMenor = default(List<int>))
        {
            #region //Variables
            List<Model.VAP_VERIFPAGO_CAUSANTES> verifPagoCausanteList = new List<Model.VAP_VERIFPAGO_CAUSANTES>();

            var _causantesByPrioridadList = Get_CausantesListByPrioridad(_causantesSIAGFByPeriodoList, periodo, default(List<int>), _causantesFechaInsert, _causantesRutMenor);
            var _valorPlanilaDisponible = _valorPlanillaPeriodo.MONTO;

            #endregion

            foreach (var causanteIngProm in _causantesByPrioridadList)
            {
                #region //Variables
                //var _diasAprocesar = GetDiasAProcesar(causanteIngProm, solicitud);
                var _historicoCausanteList = GetHistoricoCausante(solicitud.RUT_EMPLEADOR, causanteIngProm, _valorPlanillaPeriodo.MONTO);
                var _retroactivoAcumuladoHistorico = _historicoCausanteList.Sum(o => o.VALORAF_PAGADO);

                Common.Config.IPS_Config.NotificaionID _notificacionID;
                string _pendientePago = string.Empty;
                long _montoApagarCausante = 0;
                long _montoPendienteCausante = 0;
                //long _valorPlanillaByDias = ((_valorPlanilaDisponible / 30) * _diasAprocesar);
                long _valorPlanillaCausante = _valorPlanilaDisponible > causanteIngProm.CAUSANTE_INGPROMEDIO
                                                                      ? causanteIngProm.CAUSANTE_INGPROMEDIO
                                                                      : _valorPlanilaDisponible;

                var _retroactivoAcumulado_aRegistrar = _retroactivoAcumuladoHistorico;
                var _pagoSegunDias_aProcesar = GetCalculoProporcional(causanteIngProm, solicitud);
                var _pagoSegunDisponible_aPagar = (causanteIngProm.CAUSANTE_INGPROMEDIO - (_valorPlanillaCausante + _retroactivoAcumulado_aRegistrar));

                #endregion

                #region //Distribuye pago a causante 

                _montoApagarCausante = _pagoSegunDisponible_aPagar <= 0
                                                                    ? 0
                                                                    : (_pagoSegunDias_aProcesar >= _pagoSegunDisponible_aPagar
                                                                        ? _pagoSegunDisponible_aPagar
                                                                        : _pagoSegunDias_aProcesar);

                _montoPendienteCausante = (causanteIngProm.CAUSANTE_INGPROMEDIO - (_valorPlanillaCausante + _retroactivoAcumulado_aRegistrar + _montoApagarCausante));


                _valorPlanilaDisponible -= _valorPlanillaCausante;

                if (_valorPlanilaDisponible < 0)
                    _valorPlanilaDisponible = 0;


                _pendientePago = _montoPendienteCausante > 0 ? "S" : "N";
                _notificacionID = GetNotificacionCausante(_pendientePago, _montoApagarCausante);

                verifPagoCausanteList.Add(Get_CausanteVerifPago(causanteIngProm, _notificacionID, solicitud, _valorPlanillaCausante, _retroactivoAcumulado_aRegistrar, _montoApagarCausante, _montoPendienteCausante, _pendientePago));

                #endregion
            }

            return
                verifPagoCausanteList;
        }
        #endregion
    }
}


/*
 *                     //DICTIONARY_SETCONTROLFILEVALUES = new Dictionary<string, string>() { { "ARCHIVO_IDCARGA", string.Format("{0}  constant \"{1}{2}\"", "ARCHIVO_IDCARGA", _fileName, VAP_FILE.EXTENSION) } }

 * 
 * 
 * 
 *         //private string _archivoID_DistPagoCausantePeriodoLike { get; set; }//No se usa
        //private string _archivoID_DistPagoBeneficiarioPeriodoLike { get; set; }//No se usa
        //private string _archivoIDcargaSolicitudesPeriodoLike { get; set; }//No se usa

 * 
 * 
 * 
 * 
 *             //var _historicoBeneficiarioList = GetHistoricoBeneficiario(solicitud.RUT_TRABAJADOR, periodo);
            //var _valorPlanillaAcumuladoHistorico = _historicoBeneficiarioList.Sum(o => o.VALOR_ARCHIVO12) + ;

 * 
 * 
 * 
 * 
 *                     //long _montoLimiteAPagar = _valorPlanillaPeriodo.MONTO;

 *                     //#region //Distribuye segun diferencia de montos para el periodo _1
                    //long _montoLimiteAPagar = _valorSumaCausantesPeriodo == _valorPlanillaPeriodo.MONTO
                    //                                            ? _valorSumaCausantesPeriodo//Son iguales, se distribuye de acuerdo a cargas SIAGF
                    //                                            : (_valorSumaCausantesPeriodo < _valorPlanillaPeriodo.MONTO
                    //                                                ? _valorSumaCausantesPeriodo
                    //                                                : _valorPlanillaPeriodo.MONTO);//Diferencia de saldos, el monto menor es el limite de asignacion

                    //#region //Distribuye pago a causantes
                    //var verifPagoCausanteList = AsignaPago_Causantes(_causantesSIAGFByPeriodo, _valorPlanillaPeriodo, _montoLimiteAPagar, solicitud, periodo, _causantesByFechaInsertList, _causantesByRutMenorList);
                    //_distPagoCausantesList.AddRange(verifPagoCausanteList);
                    //#endregion
 * 
 * 
                //var _valorPendientePago = (causanteIngProm.CAUSANTE_INGPROMEDIO - _saldoPagadoEnHistorico);
 * 
 *                 //#region //Distribuye pago a causante _1
                ////Tiene saldo pendiente para pagos
                //if (_valorPendientePago != 0)
                //{
                //    /*Si "valorPendientePago" es mayor a 0 entonces se procesa normal, por el contrario si es menor a 0 se dejara saldo negativo en "_montoApagarCausante"
                //     *como antecedente historico, de esta forma se asegura que al sumar "VALORAF_PAGADO" cuadre su monto con el Ingreso promedio del causante en el periodo en cuestion*/
//    _montoApagarCausante = (_valorPendientePago <= _montoLimiteAPagar)
//                                ? _valorPendientePago
//                                : _montoLimiteAPagar;

//    _montoPendienteCausante = causanteIngProm.CAUSANTE_INGPROMEDIO - (_saldoPagadoEnHistorico + _montoApagarCausante);

//}

////Sin saldo pendiente, no existe causante
//else if (_saldoPagadoEnHistorico == default(long))
//{
//    _montoApagarCausante = (_montoLimiteAPagar > causanteIngProm.CAUSANTE_INGPROMEDIO)
//        ? causanteIngProm.CAUSANTE_INGPROMEDIO
//        : _montoLimiteAPagar;

//    _montoPendienteCausante = causanteIngProm.CAUSANTE_INGPROMEDIO - _montoApagarCausante;
//}
////Existe causante, se registrara con pago 0 ya que fue pagado con aterioridad y no se debe ni se a pagado demas
//else
//{
//    _montoApagarCausante = 0;

//    _montoPendienteCausante = 0;
//}




//_pendientePago = _montoPendienteCausante > 0 ? "S" : "N";
//_notificacionID = GetNotificacionCausante(_pendientePago, _montoApagarCausante);

//if (_montoApagarCausante > 0)
//    _montoLimiteAPagar -= _montoApagarCausante;

//verifPagoCausanteList.Add(Get_CausanteVerifPago(causanteIngProm, _notificacionID, solicitud, _valorPlanillaPeriodo, _montoApagarCausante, _montoPendienteCausante, _pendientePago));
//#endregion

//if (solicitud.FOLIO == 91201401000044 && solicitud.RUT_TRABAJADOR == 17081820 && (periodo == 201310 || periodo == 201311))
//{

//}

//private List<Model.VAP_VERIFPAGO_CAUSANTES> AsignaPago_CausantesTotalesIguales(List<Model.VAP_INGRESO_PROMEDIO> _causantesSIAGFByPeriodo)
//{
//    List<Model.VAP_VERIFPAGO_CAUSANTES> verifPagoCausanteList = new List<Model.VAP_VERIFPAGO_CAUSANTES>();

//    foreach (var causanteIngProm in _causantesSIAGFByPeriodo)
//    {
//        verifPagoCausanteList.Add(Get_CausanteVerifPago(causanteIngProm, causanteIngProm.CAUSANTE_INGPROMEDIO));
//    }

//    return
//        verifPagoCausanteList;
//}
//if (valorPendientePago > 0)
//    _montoApagarCausante = (saldoPendientePeriodosAnteriores <= _montoLimiteAPagar)
//                            ? saldoPendientePeriodosAnteriores
//                            : _montoLimiteAPagar;
//else
//    _montoApagarCausante = (saldoPendientePeriodosAnteriores <= _montoLimiteAPagar)
//                            ? saldoPendientePeriodosAnteriores
//                            : _montoLimiteAPagar;
////Son iguales, se distribuye de acuerdo a cargas SIAGF
//if (_montoSIAGFPeriodo == _montoArchivo12Periodo.MONTO)
//{
//    #region //Distribuye pago a causantes
//    var verifPagoCausanteList = AsignaPago_Causantes(_causantesSIAGFByPeriodo, _montoLimiteAPagar);
//    _distPagoCausantesList.AddRange(verifPagoCausanteList);
//    #endregion

//    #region //Distribuye pago a trabajador
//    var cantidadCausantesSinPago = verifPagoCausanteList.Where(o => o.VALORAF_PAGADO > 0).Count();
//    var valorPagado = verifPagoCausanteList.Sum(o => o.VALORAF_PAGADO);
//    var valorPendiente = _montoArchivo12Periodo.MONTO >= _montoSIAGFPeriodo ? ((_montoArchivo12Periodo.MONTO - _montoSIAGFPeriodo) * -1) : (_montoSIAGFPeriodo - _montoArchivo12Periodo.MONTO);
//    var verifPagoBeneficiario = AsignaPago_Trabajador(solicitud, periodo, _montoArchivo12Periodo.MONTO, valorPagado, valorPendiente, cantidadCausantesSinPago);
//    //var verifPagoBeneficiario = AsignaPago_Trabajador(solicitud, periodo, _montoArchivo12Periodo.MONTO, _montoArchivo12Periodo.MONTO, default(long), cantidadCausantesSinPago);
//    _distPagoBeneficiariosList.Add(verifPagoBeneficiario);

//    #endregion
//}
////Diferencia de saldos, el monto menor es el limite de asignacion
//else
//{
//    #region //Distribuye pago a causantes
//    //long _montoLimiteAPagar = _montoSIAGFPeriodo < _montoArchivo12Periodo.MONTO ? _montoSIAGFPeriodo : _montoArchivo12Periodo.MONTO;
//    var verifPagoCausanteList = AsignaPago_Causantes(_causantesSIAGFByPeriodo, _montoLimiteAPagar);
//    _distPagoCausantesList.AddRange(verifPagoCausanteList);
//    #endregion

//    #region //Distribuye pago a trabajador
//    var cantidadCausantesSinPago = verifPagoCausanteList.Where(o => o.VALORAF_PAGADO > 0).Count();
//    var valorPagado = verifPagoCausanteList.Sum(o => o.VALORAF_PAGADO);
//    var valorPendiente = _montoArchivo12Periodo.MONTO >= _montoSIAGFPeriodo ? ((_montoArchivo12Periodo.MONTO - _montoSIAGFPeriodo) * -1) : (_montoSIAGFPeriodo - _montoArchivo12Periodo.MONTO);
//    var verifPagoBeneficiario = AsignaPago_Trabajador(solicitud, periodo, _montoArchivo12Periodo.MONTO, valorPagado, valorPendiente, cantidadCausantesSinPago);
//    _distPagoBeneficiariosList.Add(verifPagoBeneficiario);

//    #endregion
//}

//santesToproccess = numeroCargasSIAGFByPeriodo < numeroCargasSolicitud ? numeroCargasSIAGFByPeriodo : numeroCargasSolicitud;

//var ruleToUse = GetRulePrio
//DistribuyeFondos_SaldoMayorQueArchivo12(
//                        _causantesSIAGFByPeriodo
//                        , _montoArchivo12Periodo.MONTO
//                        , ruleToUse
//                        , _numeroCausantesToproccess
//                        , _causantesMayorPermanencia
//                        , _causantesFechaInsert
//                        , _causantesRutMenor);






//var _periodoHasCausante = causantesSIAGFBySolicitudList.Where(o => o.CAUSANTE_PERIODO == periodo && o.RUT_CAUSANTE == causanteSolicitud.RUT_CAUSANTE).Count() > 0;

//if (_periodoHasCausante)
//{
//}
//else //No reconocido para el periodo, se registra causante sin pago fuera del periodo(no se considerara como causante sin pago para el calculo con Retroactivo, puesto que tiene que pertenecer al periodo señalado)
//{
//    _distPagoCausantesList.Add(FillCausante_SinPagoNoReconocido(causanteSolicitud));

//}

//foreach (var periodo in periodosSolicitudArray)
//{
//    var causantesToDistribuirPago = default(Model.VAP_INGRESO_PROMEDIO);
//    var _causantesByPeriodo = causantesSIAGFBySolicitudList.Where(o => o.CAUSANTE_PERIODO == periodo).ToList();


//    var numCargasSIAGFByPeriodo = _causantesByPeriodo.Count;







//if (_montoAFCausantesPeriodo == _montoAFBeneficiarioPeriodo.MONTO)
//{

//causantesToDistribuirPago = 


//var _montoADistribuir = (_montoAFCausantesPeriodo / numCargasSIAGFByPeriodo);

//foreach (var causante in _causantesByPeriodo)
//{
//    _distPagoCausantesList.Add(FillCausante_SaldosIguales(causante, _montoADistribuir));
//}


//}
//else if (_montoAFCausantesPeriodo > _montoAFBeneficiarioPeriodo.MONTO)
//{   

//}
//else//Es menor
//{

//}





//var _causantesPeriodoIngPromList = GetCausantesIngPromedio_ByBeneficiario(solicitud.RUT_TRABAJADOR, periodo);

//var _numCargasByPeriodoSIAGF = _causantesPeriodoIngPromList.Count;

//}
//  private List<Model.VAP_VERIFPAGO_CAUSANTES> DistribuyeFondos_SaldoMayorQueArchivo12(
//                                            List<Model.VAP_INGRESO_PROMEDIO> _causantesSIAGFByPeriodoList
//                                            , long _montoLimiteAPagar
//                                            , Common.Config.IPS_Config.RulePriorizationToUse ruleToUse
//                                            , int numeroCausantesToproccess
//                                            , List<int> _causantesMayorPermanenciaDict = default(List<int>)
//                                            , List<int> _causantesFechaInsertDict = default(List<int>)
//                                            , List<int> _causantesRutMenorDict = default(List<int>))
//{

//    List<Model.VAP_VERIFPAGO_CAUSANTES>  verifPagoCausanteList = new List<Model.VAP_VERIFPAGO_CAUSANTES>();

//    short index = 1;

//    var causanteDictionaryToUse = ruleToUse == Common.Config.IPS_Config.RulePriorizationToUse.MAYOR_PERMANENCIA 
//        ? _causantesMayorPermanenciaDict
//        : (ruleToUse == Common.Config.IPS_Config.RulePriorizationToUse.MENOR_RUT ? _causantesRutMenorDict : _causantesFechaInsertDict);


//    foreach (var causanteID in causanteDictionaryToUse)
//    {
//        var periodoHasCausante = _causantesSIAGFByPeriodoList.Exists(o=>o.RUT_CAUSANTE ==  causanteID);

//        if(periodoHasCausante)
//        {
//            var causanteToUse = _causantesSIAGFByPeriodoList.Where(o=>o.RUT_CAUSANTE ==  causanteID).SingleOrDefault();

//                #region //Distribuye pago a causantes

//                    long montoApagarCausante = _montoLimiteAPagar > causanteToUse.CAUSANTE_INGPROMEDIO ? causanteToUse.CAUSANTE_INGPROMEDIO : _montoLimiteAPagar;
//                    long montoPendienteCausante = causanteToUse.CAUSANTE_INGPROMEDIO - montoApagarCausante;
//                    _montoLimiteAPagar -= montoApagarCausante;

//                    verifPagoCausanteList.Add(Get_CausanteVerifPago(causanteToUse, montoApagarCausante, montoPendienteCausante));
//                #endregion
//        }

//        //if(index == numeroCausantesToproccess)
//        //    break;

//        index++;
//    }

//    return
//        verifPagoCausanteList;
//}
