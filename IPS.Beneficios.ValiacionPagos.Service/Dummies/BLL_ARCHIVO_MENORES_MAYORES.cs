﻿using IPS.Beneficios.ValiacionPagos.Service.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using SpreadsheetLight;
using SpreadsheetLight.Charts;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;
using DocumentFormat.OpenXml.Spreadsheet;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class BLL_ARCHIVO_MENORES_MAYORES : IPS_Dummie
    {
        #region //atributes
        private List<Model.VAP_SOLICITUDES_PROCESADOS> _solicitudesProcesadasList;
        private List<Model.BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO> _retroactivosAgrupadosByBeneficiarioList;
        private Model.VAP_PARAMETROS _paramMesesNoProcesamiento;
        private string _folderPath;
        #endregion

        public BLL_ARCHIVO_MENORES_MAYORES(Model.VAP_PERIODOS VAP_PERIODO)
            : base(VAP_PERIODO)
        {
            _paramMesesNoProcesamiento = _parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.MESES_NOPROCESAMIENTO_ARCHIVO_MENORESMAYORES);

            var periodoHastaToProccess = Common.Helper.Generics.GetPeriodoFormatted(_VAP_PERIODO.PERIODO_DISPLAY, int.Parse(_paramMesesNoProcesamiento.PARAMETRO_VALOR) * -1);

            _solicitudesProcesadasList = _repositoryVAP_SOLICITUDES_PROCESADOS.GET(periodoHastaToProccess);

            _retroactivosAgrupadosByBeneficiarioList = _repositoryBLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO.GETALL();

            _folderPath = GetFolder("IPS_SOLICITADOS", Common.Config.IPS_Config.mayoresMenoresFolder, _VAP_PERIODO);
        }

        public bool Main()
        {
            //Borra los registros de la tabla "RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO" antes de comenzar
            var wasCleanded = _repositoryBLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO.DELETE();

            if (wasCleanded)
                return
                   this.Procesasolicitudes();
            else
                return false;
        }

        #region //PRIVATE
        private bool Procesasolicitudes()
        {
            #region //Variables genericas metodo
            var wasFilledAndBulkedDAta = false;
            var wasExcelGenerated = false;
            #endregion

            wasFilledAndBulkedDAta = FillandBulk_RetroactivosAgrupadosList();

            wasExcelGenerated = GeneraArchivoDetalleMayoresMenores();

            return
                wasFilledAndBulkedDAta && wasExcelGenerated;
        }

        /// <summary>
        /// Asigna sumatoria(pagos pendiente) de causantes por beneficiario y determina si pago fue correcto en lista de beneficiarios, ademas una vez seteado esta informacion se genera un bulk data sobre la tabla fisica en cuestion
        /// </summary>
        private bool FillandBulk_RetroactivosAgrupadosList()
        {
            #region //Variables
            var wasFilled = false;
            #endregion

            #region //Recorre los beneficiarios
            foreach (var beneficiario in _retroactivosAgrupadosByBeneficiarioList)
            {
                beneficiario.LIQUIDOAPAGAR_SISTEMA = _solicitudesProcesadasList.Where(o => o.RUT_EMPLEADOR == beneficiario.RUT_BENEFICIARIO && o.NOTIFICACION_ID == (int)Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_12_7).Select(o => o.VALORAF_PENDIENTE).Sum();

                beneficiario.PAGOCORRECTO = beneficiario.LIQUIDOAPAGAR_SISTEMA >= beneficiario.LIQUIDOAPAGAR_AGRUPADO ? 'S' : 'N';

                beneficiario.SISTEMA_FEC_PROCESO = _fechaProceso;
            }
            #endregion

            var sqlStatement = string.Format("insert {0} ({1}, {2}, {3}, {4}, {5}, {6}) values(@{1}, @{2}, @{3}, @{4}, @{5}, @{6})"
                , "RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO"
                , "RUT_BENEFICIARIO"
                , "DV_BENEFICIARIO"
                , "LIQUIDOAPAGAR_AGRUPADO"
                , "LIQUIDOAPAGAR_SISTEMA"
                , "PAGOCORRECTO"
                , "SISTEMA_FEC_PROCESO"
                );

            var listRetroactivosSplitted = IPS.Beneficios.Common.Helper.Generics.ChunkBy(_retroactivosAgrupadosByBeneficiarioList, 5000);

            foreach (var lista in listRetroactivosSplitted)
            {
                wasFilled = _serviceVAP_BULKDATA.BulkDataMysql(sqlStatement, lista, Common.Config.IPS_Config.ConnString.MYSQL_PROD_ARCHIVOS22);

                if (wasFilled == false)
                    break;
            }

            return
                wasFilled;
        }

        private bool GeneraArchivoDetalleMayoresMenores()
        {
            #region //Variables
            var _filePathList = new List<string>();
            var _fila = 2;
            string _colorToApply = "B";

            #region //Estilos excel

            SLThemeSettings theme = new SLThemeSettings();
            theme.MinorLatinFont = "Arial";
            SLDocument excelDoc = new SLDocument(theme);
            SLComment comm;
            comm = excelDoc.CreateComment();
            comm.Width = 150;
            comm.Height = 85;
            comm.SetPosition(16, 14);
            //comm.AutoSize = true;

            #region //Font color
            SLStyle styleFontColorA = excelDoc.CreateStyle();
            styleFontColorA.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.WhiteSmoke, System.Drawing.Color.Black);

            SLStyle styleFontColorB = excelDoc.CreateStyle();
            styleFontColorB.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.LightGray, System.Drawing.Color.Black);
            #endregion

            #region //Border
            SLStyle styleBorder = excelDoc.CreateStyle();
            styleBorder.Font.FontSize = 9;
            styleBorder.Border.LeftBorder.BorderStyle = BorderStyleValues.Thin;
            styleBorder.Border.LeftBorder.Color = System.Drawing.Color.Black;

            styleBorder.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
            styleBorder.Border.BottomBorder.Color = System.Drawing.Color.Black;

            styleBorder.Border.RightBorder.BorderStyle = BorderStyleValues.Thin;
            styleBorder.Border.BottomBorder.Color = System.Drawing.Color.Black;

            styleBorder.Border.TopBorder.BorderStyle = BorderStyleValues.Thin;
            styleBorder.Border.BottomBorder.Color = System.Drawing.Color.Black;
            #endregion

            #region //Dates
            SLStyle styleDate = excelDoc.CreateStyle();
            styleDate.FormatCode = "dd/MM/yyyy";
            #endregion

            #region //Numeric
            SLStyle styleNumeric = excelDoc.CreateStyle();
            styleNumeric.FormatCode = "#";
            #endregion

            #region //Row color
            SLStyle styleColorRowRed = excelDoc.CreateStyle();
            styleColorRowRed.Font.FontColor = System.Drawing.Color.Red;

            SLStyle styleColorRowGreen = excelDoc.CreateStyle();
            styleColorRowGreen.Font.FontColor = System.Drawing.Color.Green;
            #endregion


            #endregion

            #endregion

            #region //Encabezados
            //excelDoc.AddWorksheet("Detalle Pago Retroactivo");

            excelDoc.SetCellValue(_fila, 1, "Notificacion ID:");
            _fila++;
            excelDoc.SetCellValue(_fila, 1, "18 = 'CAUSANTE CONSIDERADO EN PERIODO, CAUSANTE CON PAGO PENDIENTE CON RELACION LABORAL.'");
            excelDoc.SetRowStyle(_fila, styleColorRowGreen);
            _fila++;
            excelDoc.SetCellValue(_fila, 1, "19 = 'CAUSANTE CONSIDERADO EN PERIODO, CAUSANTE PAGO PENDIENTE SIN RELACION LABORAL.'");
            excelDoc.SetRowStyle(_fila, styleColorRowRed);
            _fila++;
            excelDoc.SetCellValue(_fila, 1, "20 = 'CAUSANTE NO CONSIDERADO EN PERIODO POR REGLAS DE PRIORIZACION, CAUSANTE PAGO PENDIENTE CON RELACION LABORAL.'");
            excelDoc.SetRowStyle(_fila, styleColorRowRed);
            _fila++;
            excelDoc.SetCellValue(_fila, 1, "21 = 'CAUSANTE NO CONSIDERADO EN PERIODO POR REGLAS DE PRIORIZACION, CAUSANTE PAGADO EXITOSAMENTE.'");
            excelDoc.SetRowStyle(_fila, styleColorRowRed);
            _fila++;
            excelDoc.SetCellValue(_fila, 1, "22 = 'CAUSANTE NO CONSIDERADO EN PERIODO POR REGLAS DE PRIORIZACION, CAUSANTE PAGO PENDIENTE SIN RELACION LABORAL.'");
            excelDoc.SetRowStyle(_fila, styleColorRowRed);

            SLStyle styleNotes = excelDoc.CreateStyle();
            styleNotes.Font.FontSize = 8;
            styleNotes.Font.Bold = true;
            excelDoc.SetCellStyle("A2", "A7", styleNotes);

            _fila += 3;
            #endregion


            #region //Agrupa informacion

            var empleadoresList = (from empleador in _solicitudesProcesadasList
                                   group empleador by empleador.RUT_EMPLEADOR into g
                                   select g.Last()).ToList();
            #endregion

            foreach (var empleador in empleadoresList)
            {
                #region //style variables
                var _filaInicio = _fila;
                _colorToApply = _colorToApply == "B" ? "A" : "B";
                #endregion

                #region //Cabecera & Encabezados
                AddExcelHeaders(excelDoc, _fila);
                _fila++;
                #endregion

                #region //Empleador
                excelDoc.SetCellValue(_fila, 1, empleador.RUT_EMPLEADOR);// Rut Empleador
                excelDoc.SetCellValue(_fila, 2, empleador.DV_EMPLEADOR);// Dv Empleador

                var solicitudesByEmpleadorList = (from solicitud in _solicitudesProcesadasList
                                                  where solicitud.RUT_EMPLEADOR == empleador.RUT_EMPLEADOR
                                                  group solicitud by solicitud.FOLIO into g
                                                  select g.Last()).ToList();
                #endregion

                foreach (var solicitud in solicitudesByEmpleadorList)
                {
                    #region //Variables
                    var solicitudRepeatedList = GetSolicitudRepeated(solicitud);
                    long _totalPagado = default(long);
                    long _totalPendiente = default(long);

                    #endregion

                    #region //Solicitud
                    excelDoc.SetCellValue(_fila, 3, solicitud.FOLIO);// Folio Solicitud
                    excelDoc.SetCellStyle(_fila, 3, styleNumeric);

                    var _filaAux = _fila;
                    foreach (var solicitudRepeated in solicitudRepeatedList)
                    {
                        excelDoc.SetCellValue(_filaAux, 4, solicitudRepeated.FECHA_INICIO_COMPENSACION);// Fecha Inicio
                        excelDoc.SetCellValue(_filaAux, 5, solicitudRepeated.FECHA_INICIO_CALCULADA);// Fecha Inicio Calculada
                        excelDoc.SetCellValue(_filaAux, 6, solicitudRepeated.FECHA_FIN_COMPENSACION);// Fecha Fin
                        excelDoc.SetCellStyle(_filaAux, 4, _filaAux, 6, styleDate);
                        _filaAux++;
                    }

                    var trabajadoresBySolicitudList = (from trabajador in _solicitudesProcesadasList
                                                       where trabajador.RUT_EMPLEADOR == empleador.RUT_EMPLEADOR && trabajador.FOLIO == solicitud.FOLIO
                                                       group trabajador by trabajador.RUT_TRABAJADOR into g
                                                       select g.Last()).ToList();
                    #endregion

                    foreach (var trabajador in trabajadoresBySolicitudList)
                    {
                        #region //Trabajador
                        excelDoc.SetCellValue(_fila, 7, trabajador.RUT_TRABAJADOR);// Rut Trabajador
                        excelDoc.SetCellValue(_fila, 8, trabajador.DV_TRABAJADOR);// Dv Trabajador

                        var causantesByTrabajadorList = _solicitudesProcesadasList
                                    .Where(o =>
                                        o.RUT_EMPLEADOR == empleador.RUT_EMPLEADOR
                                        && o.FOLIO == solicitud.FOLIO
                                        && o.RUT_TRABAJADOR == trabajador.RUT_TRABAJADOR
                                        )
                                    .GroupBy(o => new
                                    {
                                        o.RUT_CAUSANTE
                                        ,
                                        o.DV_CAUSANTE
                                        ,
                                        o.CAUSANTE_EDAD
                                        ,
                                        o.CAUSANTE_PENDIENTEPAGO
                                        ,
                                        o.CAUSANTE_FEC_REC
                                        ,
                                        o.CAUSANTE_FEC_EXT_CALC
                                        ,
                                        o.CAUSANTE_DIAS_SIAGF
                                        ,
                                        o.CAUSANTE_DIAS_SOLICITADOS
                                        ,
                                        o.CAUSANTE_DIAS_TRABAJADOS
                                        ,
                                        o.CAUSANTE_PERIODO
                                        ,
                                        o.VALOR_ARCHIVO12
                                        ,
                                        o.RETROACTIVO_ACUMULADO
                                        ,
                                        o.VALORAF_DEFAULT
                                        ,
                                        o.VALORAF_PAGADO
                                        ,
                                        o.VALORAF_PENDIENTE
                                        ,
                                        o.NOTIFICACION_ID
                                        ,
                                        o.PERIODO_PROCESO
                                        ,
                                        o.TRAMO
                                    })
                                    .Select(o => new
                                    {
                                        RUT_CAUSANTE = o.Key.RUT_CAUSANTE
                                        ,
                                        DV_CAUSANTE = o.Key.DV_CAUSANTE
                                        ,
                                        CAUSANTE_EDAD = o.Key.CAUSANTE_EDAD
                                        ,
                                        CAUSANTE_PENDIENTEPAGO = o.Key.CAUSANTE_PENDIENTEPAGO
                                        ,
                                        CAUSANTE_FEC_REC = o.Key.CAUSANTE_FEC_REC
                                        ,
                                        CAUSANTE_FEC_EXT_CALC = o.Key.CAUSANTE_FEC_EXT_CALC
                                        ,
                                        CAUSANTE_DIAS_SIAGF = o.Key.CAUSANTE_DIAS_SIAGF
                                        ,
                                        CAUSANTE_DIAS_SOLICITADOS = o.Key.CAUSANTE_DIAS_SOLICITADOS
                                        ,
                                        CAUSANTE_DIAS_TRABAJADOS = o.Key.CAUSANTE_DIAS_TRABAJADOS
                                        ,
                                        CAUSANTE_PERIODO = o.Key.CAUSANTE_PERIODO
                                        ,
                                        VALOR_ARCHIVO12 = o.Key.VALOR_ARCHIVO12
                                        ,
                                        RETROACTIVO_ACUMULADO = o.Key.RETROACTIVO_ACUMULADO
                                        ,
                                        VALORAF_DEFAULT = o.Key.VALORAF_DEFAULT
                                        ,
                                        VALORAF_PAGADO = o.Key.VALORAF_PAGADO
                                        ,
                                        VALORAF_PENDIENTE = o.Key.VALORAF_PENDIENTE
                                        ,
                                        NOTIFICACION_ID = o.Key.NOTIFICACION_ID
                                        ,
                                        PERIODO_PROCESO = o.Key.PERIODO_PROCESO
                                        ,
                                        TRAMO = o.Key.TRAMO
                                    })
                                    .OrderBy(o => o.CAUSANTE_PERIODO)
                                    .ToList();

                        #endregion

                        foreach (var causante in causantesByTrabajadorList)
                        {
                            #region //Vars
                            var _estadoSaldo = causante.VALORAF_PENDIENTE > 0
                                                ? "A favor del causante"
                                                : (causante.VALORAF_PENDIENTE < 0 ? "A favor de IPS" : "Liquidado");

                            #endregion


                            #region //Causante
                            excelDoc.SetCellValue(_fila, 9, causante.RUT_CAUSANTE);// Rut Causante
                            excelDoc.SetCellValue(_fila, 10, causante.DV_CAUSANTE);// Dv Causante
                            excelDoc.SetCellValue(_fila, 11, causante.CAUSANTE_EDAD);// Edad Causante
                            excelDoc.SetCellValue(_fila, 12, causante.CAUSANTE_PENDIENTEPAGO);// Pendiente Pago: (S/N)
                            excelDoc.SetCellValue(_fila, 13, causante.CAUSANTE_FEC_REC);// Fec.Reconocimiento
                            excelDoc.SetCellValue(_fila, 14, causante.CAUSANTE_FEC_EXT_CALC);// Fec.Extincion calc
                            excelDoc.SetCellValue(_fila, 15, causante.CAUSANTE_DIAS_SIAGF);// Dias Siagf
                            excelDoc.SetCellValue(_fila, 16, causante.CAUSANTE_DIAS_SOLICITADOS);// Dias Solicitados
                            excelDoc.SetCellValue(_fila, 17, causante.CAUSANTE_DIAS_TRABAJADOS);// Dias Trabajados
                            excelDoc.SetCellValue(_fila, 18, causante.CAUSANTE_PERIODO);// Periodo Mes
                            excelDoc.SetCellValue(_fila, 19, causante.TRAMO);// ID Tramo


                            excelDoc.SetCellValue(_fila, 20, causante.VALORAF_DEFAULT);// Valor Tramo

                            excelDoc.SetCellValue(_fila, 21, causante.VALOR_ARCHIVO12);//Valor Planilla
                            excelDoc.SetCellValue(_fila, 22, causante.RETROACTIVO_ACUMULADO);//Retroactivo acumulado

                            excelDoc.SetCellValue(_fila, 23, causante.VALORAF_PAGADO);// Valor Pagado
                            excelDoc.SetCellValue(_fila, 24, causante.VALORAF_PENDIENTE);//Saldo
                            excelDoc.SetCellValue(_fila, 25, _estadoSaldo);//Estado Saldo

                            excelDoc.SetCellValue(_fila, 26, causante.NOTIFICACION_ID);// Notificacion ID
                            excelDoc.SetCellValue(_fila, 27, causante.PERIODO_PROCESO);// Periodo solicitudes emitidas en Previred

                            excelDoc.SetCellStyle(_fila, 13, _fila, 14, styleDate);

                            #region //Add comments
                            var _valorPlanilla = _repositoryBLL_PAGOS.GET(trabajador.RUT_TRABAJADOR, causante.CAUSANTE_PERIODO);
                            string comentarioPeriodoCell = string.Format("Valor totalizado en planilla ${2}. (Rut beneficiario: {0}, periodo: {1})", trabajador.RUT_TRABAJADOR, causante.CAUSANTE_PERIODO, _valorPlanilla.MONTO);
                            comm.SetText(comentarioPeriodoCell);
                            excelDoc.InsertComment(_fila, 18, comm);

                            string comentarioValorPagadoCell = causante.VALORAF_PAGADO > 0
                                                                ? string.Format("Pagado según relación menor de los días: [{0}]", GetMenorDia(causante.CAUSANTE_DIAS_SIAGF, causante.CAUSANTE_DIAS_SOLICITADOS, causante.CAUSANTE_DIAS_TRABAJADOS))
                                                                : (causante.CAUSANTE_DIAS_TRABAJADOS > 0 ? "Causante saldado" : "Sin dias trabajados");
                            comm.SetText(comentarioValorPagadoCell);
                            excelDoc.InsertComment(_fila, 23, comm);

                            string comentarioNotificacionCell = GetNotificationComment(excelDoc, causante.NOTIFICACION_ID);
                            comm.SetText(comentarioNotificacionCell);
                            excelDoc.InsertComment(_fila, 26, comm);
                            #endregion

                            _totalPagado += causante.NOTIFICACION_ID == (int)IPS.Beneficios.Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_12_7 ? causante.VALORAF_PAGADO : 0;
                            _totalPendiente += causante.NOTIFICACION_ID == (int)IPS.Beneficios.Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_12_7 ? causante.VALORAF_PENDIENTE : 0;
                            _fila++;
                            #endregion
                        }
                        _fila++;
                    }

                    #region //Totales
                    excelDoc.SetCellValue(_fila, 22, "Totales: ");
                    excelDoc.SetCellValue(_fila, 23, _totalPagado);
                    excelDoc.SetCellValue(_fila, 24, _totalPendiente);
                    SLStyle style = excelDoc.CreateStyle();
                    style.Font.Bold = true;
                    excelDoc.SetCellStyle(_fila, 19, _fila, 21, style);

                    string comentarioTotalesCell = "Sumatoria valida solo para registros con notificacionID: 18";
                    comm.SetText(comentarioTotalesCell);
                    excelDoc.InsertComment(_fila, 23, comm);
                    excelDoc.InsertComment(_fila, 24, comm);
                    #endregion

                    _fila++;
                }

                #region //Set font color
                var styleToApply = _colorToApply == "A" ? styleFontColorA : styleFontColorB;

                for (int row = _filaInicio; row < _fila; row++)
                {
                    var styleToApplyColor = excelDoc.GetCellValueAsInt32(row, 26) == 18 ? styleColorRowGreen : styleColorRowRed;

                    for (int column = 0; column <= 27; column++)
                    {
                        excelDoc.SetCellStyle(row, column, styleToApply);

                        if (excelDoc.GetCellValueAsInt32(row, 9) > 0 && excelDoc.GetCellValueAsInt32(row, 9) != null)// Es distinto cabecera
                        {
                            if (column >= 9)
                                excelDoc.SetCellStyle(row, column, styleToApplyColor);
                        }
                    }
                }
                #endregion
            }

            #region //Set generic styles
            for (int row = 10; row < _fila; row++)
            {
                for (int column = 0; column <= 27; column++)
                    excelDoc.SetCellStyle(row, column, styleBorder);
            }

            for (int column = 0; column <= 27; column++)
            {
                switch (column)
                {
                    case 3:
                        excelDoc.SetColumnWidth(column, 15);
                        break;
                    case 4:
                        excelDoc.SetColumnWidth(column, 12);
                        break;
                    case 11:
                        excelDoc.SetColumnWidth(column, 8);
                        break;
                    case 12:
                        excelDoc.SetColumnWidth(column, 13);
                        break;
                    case 13:
                        excelDoc.SetColumnWidth(column, 17);
                        break;
                    case 14:
                        excelDoc.SetColumnWidth(column, 15);
                        break;
                    case 15:
                        excelDoc.SetColumnWidth(column, 9);
                        break;
                    case 16:
                        excelDoc.SetColumnWidth(column, 14);
                        break;
                    case 17:
                        excelDoc.SetColumnWidth(column, 14);
                        break;
                    case 18:
                        excelDoc.SetColumnWidth(column, 10);
                        break;
                    case 19:
                        excelDoc.SetColumnWidth(column, 10);
                        break;
                    case 21:
                        excelDoc.SetColumnWidth(column, 11);
                        break;
                    case 22:
                        excelDoc.SetColumnWidth(column, 18);
                        break;
                    case 23:
                        excelDoc.SetColumnWidth(column, 15);
                        break;
                    case 24:
                        excelDoc.SetColumnWidth(column, 15);
                        break;
                    case 25:
                        excelDoc.SetColumnWidth(column, 15);
                        break;
                    case 26:
                        excelDoc.SetColumnWidth(column, 15);
                        break;
                    case 27:
                        excelDoc.SetColumnWidth(column, 30);
                        break;
                    default:
                        excelDoc.SetColumnWidth(column, 12);
                        break;
                }
            }

            #endregion

            #region //Save file
            var _hour = DateTime.Now.ToString("ddMMyyyy HHmmss");
            var _filePath = string.Format("{0}//{1}{2}_fec{3}.xlsx", _folderPath, "PagosRetroactivos", _VAP_PERIODO.PERIODO_DISPLAY, _hour);

            _filePathList.Add(_folderPath + ".xlsx");

            excelDoc.SaveAs(_filePath);
            excelDoc.Dispose();
            #endregion

            return
                IPS.Beneficios.Common.Helper.FilesHelper.Exist(_filePath);
        }

        private void AddExcelHeaders(SLDocument excelDoc, int _fila)
        {

            SLStyle style = excelDoc.CreateStyle();
            style.Font.Bold = true;

            excelDoc.SetCellValue(_fila, 1, "Rut Empleador");
            excelDoc.SetCellValue(_fila, 2, "Dv Empleador");

            excelDoc.SetCellValue(_fila, 3, "Folio Solicitud");
            excelDoc.SetCellValue(_fila, 4, "Fec.Inicio");
            excelDoc.SetCellValue(_fila, 5, "Fec.Inicio calc");
            excelDoc.SetCellValue(_fila, 6, "Fec.Fin");

            excelDoc.SetCellValue(_fila, 7, "Rut Trabajador");
            excelDoc.SetCellValue(_fila, 8, "Dv Trabajador");

            excelDoc.SetCellValue(_fila, 9, "Rut Causante");
            excelDoc.SetCellValue(_fila, 10, "Dv Causante");
            excelDoc.SetCellValue(_fila, 11, "Edad");
            excelDoc.SetCellValue(_fila, 12, "Pendiente Pago");
            excelDoc.SetCellValue(_fila, 13, "Fec.Reconocimiento");
            excelDoc.SetCellValue(_fila, 14, "Fec.Extincion calc");
            excelDoc.SetCellValue(_fila, 15, "Dias Siagf");
            excelDoc.SetCellValue(_fila, 16, "Dias Solicitados");
            excelDoc.SetCellValue(_fila, 17, "Dias Trabajados");
            excelDoc.SetCellValue(_fila, 18, "Periodo Mes");
            excelDoc.SetCellValue(_fila, 19, "ID Tramo");

            excelDoc.SetCellValue(_fila, 20, "Valor Tramo");
            excelDoc.SetCellValue(_fila, 21, "Valor Planilla");
            excelDoc.SetCellValue(_fila, 22, "Retroactivo Acumulado");
            excelDoc.SetCellValue(_fila, 23, "Valor Pagado");
            excelDoc.SetCellValue(_fila, 24, "Saldo"); //En Base Datos es valor pendiente
            excelDoc.SetCellValue(_fila, 25, "Estado Saldo");

            excelDoc.SetCellValue(_fila, 26, "Notificacion ID");
            excelDoc.SetCellValue(_fila, 27, "Periodo solicitudes emitidas en Previred");

            excelDoc.SetRowStyle(_fila, style);

        }

        private List<Model.VAP_SOLICITUDES_PROCESADOS> GetSolicitudRepeated(Model.VAP_SOLICITUDES_PROCESADOS solicitud)
        {
            return
                _solicitudesProcesadasList
                .Where(o => o.FOLIO == solicitud.FOLIO)
                .GroupBy(o => new
                {
                    o.FOLIO
                    ,
                    o.FECHA_INICIO_COMPENSACION
                    ,
                    o.FECHA_INICIO_CALCULADA
                    ,
                    o.FECHA_FIN_COMPENSACION
                })
                .Select(o => new Model.VAP_SOLICITUDES_PROCESADOS
                {
                    FOLIO = o.Key.FOLIO
                    ,
                    FECHA_INICIO_COMPENSACION = o.Key.FECHA_INICIO_COMPENSACION
                    ,
                    FECHA_INICIO_CALCULADA = o.Key.FECHA_INICIO_CALCULADA
                    ,
                    FECHA_FIN_COMPENSACION = o.Key.FECHA_FIN_COMPENSACION
                })
                .ToList();
        }

        private string GetNotificationComment(SLDocument excelDoc, int _notificationID)
        {
            string comment = string.Empty;

            switch (_notificationID)
            {
                case (int)IPS.Beneficios.Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_12_7:
                    comment = excelDoc.GetCellValueAsString("A3");
                    break;
                case (int)IPS.Beneficios.Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_12_17:
                    comment = excelDoc.GetCellValueAsString("A4");
                    break;
                case (int)IPS.Beneficios.Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_11_7:
                    comment = excelDoc.GetCellValueAsString("A5");
                    break;
                case (int)IPS.Beneficios.Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_11_8:
                    comment = excelDoc.GetCellValueAsString("A6");
                    break;
                case (int)IPS.Beneficios.Common.Config.IPS_Config.NotificaionID.GROUP_NOTIFICACION_11_17:
                    comment = excelDoc.GetCellValueAsString("A7");
                    break;
            }

            return
                comment;
        }
        #endregion
    }
}
