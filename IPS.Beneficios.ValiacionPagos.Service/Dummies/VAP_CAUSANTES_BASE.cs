﻿using IPS.Beneficios.ValiacionPagos.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;


namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_CAUSANTES_BASE : IPS_Dummie
    {

        BLL_BULKDATA _serviceVAP_BULKDATA;

        public VAP_CAUSANTES_BASE()
        {
            _serviceVAP_BULKDATA = new BLL_BULKDATA();
        }

        public Common.Config.IPS_Config.ProcesoEstado ProccessFile(Model.VAP_PERIODOS VAP_PERIODO)
        {
            #region //Variables
            var _monthName = Common.Helper.Generics.GetMonthName(VAP_PERIODO.PERIODO_DISPLAY.ToString());
            var _downloadFolder = string.Format("{0}", Common.Config.IPS_Config.archivosCargaFolder);
            List<string> listFile = new List<string>();
            Common.Config.IPS_Config.ProcesoEstado _procesoEstado = default(Common.Config.IPS_Config.ProcesoEstado);
            #endregion

            var wasCleanded = Common.Helper.FilesHelper.CleanFolder("IPS_30100_", _downloadFolder);

            if (wasCleanded)
            {

                listFile.Add(string.Format("_{0}_{1}_VIG", _monthName, VAP_PERIODO.PERIODO_DISPLAY.ToString().Substring(0, 4)));
                listFile.Add(string.Format("_{0}_{1}_EXT", _monthName, VAP_PERIODO.PERIODO_DISPLAY.ToString().Substring(0, 4)));

                var _successCount = listFile.Count;

                if (_successCount == 0) _successCount = -1;

                foreach (var fileName in listFile)
                {
                    Model.VAP_FILE VAP_FILE = new Model.VAP_FILE()
                    {
                        ARCHIVORUTA = Common.Config.IPS_Config._ftpPath
                         ,
                        PERIODO = VAP_PERIODO.PERIODO_DISPLAY
                        ,
                        ARCHIVONOMBRE = fileName
                        ,
                        EXTENSION = ".TXT"
                    };

                    var _file = Common.Helper.FtpComponent.GetFileInfo(VAP_FILE);

                    if (_file != null)
                    {
                        var _wasDownloaded = Common.Helper.FtpComponent.DownloadFile(_file.FullName, _downloadFolder);

                        if (_wasDownloaded)
                        {
                            var _tableName = typeof(Model.VAP_CAUSANTES_BASE).Name;

                            var _tipoReconocido = _file.Name.ToUpper().Contains("VIG") ? "V" : "E";

                            var _bulkedData = _serviceVAP_BULKDATA.BulkData(new Model.SQL_LOADER()
                            {
                                TABLE_NAME = _tableName
                                ,
                                FILEPATH = _downloadFolder + "\\" + _file.Name
                                ,
                                CLEANTABLE = true
                                ,
                                CLEANTABLE_QUERY = string.Format("DELETE VALIDACION_PAGOS.{0} WHERE PERIODO_PROCESO = {1} AND TIPO_RECONOCIDO = '{2}'", _tableName, VAP_FILE.PERIODO, _tipoReconocido)
                                ,
                                CTLPATH = string.Format(@"{0}LOADERS\CTL\{1}.ctl", Common.Config.IPS_Config.installDirApp, _tableName)
                                ,
                                LOGPATH = string.Format(@"{0}LOADERS\LOG\{1}.log", Common.Config.IPS_Config.installDirApp, _file.Name)
                                ,
                                BADPATH = string.Format(@"{0}LOADERS\LOG\{1}.bad", Common.Config.IPS_Config.installDirApp, _file.Name)
                                ,
                                DISCARDPATH = string.Format(@"{0}LOADERS\LOG\{1}.dsc", Common.Config.IPS_Config.installDirApp, _file.Name)
                                ,
                                DICTIONARY_SETCONTROLFILEVALUES = new Dictionary<string, string>() { {"PERIODO_PROCESO", string.Format("{0}  constant {1}", "PERIODO_PROCESO", VAP_FILE.PERIODO)}
                                                                                                ,{"ARCHIVO_IDCARGA", string.Format("{0}  constant \"{1}\"", "ARCHIVO_IDCARGA", _file.Name) }}
                            });

                            if (_bulkedData)
                                _successCount--;
                        }
                    }
                }

                // Shoulb be 0 if all bulked files run successfully
                if (_successCount == 0)
                    _procesoEstado = Common.Config.IPS_Config.ProcesoEstado.TERMINADO_SUCCESS;
                else if (listFile.Count > 0 && _successCount != 0)
                    _procesoEstado = Common.Config.IPS_Config.ProcesoEstado.TERMINADO_ERROR;
                else if (listFile.Count == 0 && _successCount < 0)
                    _procesoEstado = Common.Config.IPS_Config.ProcesoEstado.ENESPERA_ARCHIVOPERIODO;
                else
                    _procesoEstado = Common.Config.IPS_Config.ProcesoEstado.TERMINADO_ERROR;
            }
            return
                _procesoEstado;
        }


        public List<Model.VAP_CAUSANTES_BASE> GET(DateTime fechaReconocimiento, string constrain, int periodoProceso, string tipoReconocido)
        {
            return
                _repositoryVAP_CAUSANTES_BASE.GET(fechaReconocimiento, constrain, periodoProceso, tipoReconocido);
        }

        public List<Model.VAP_CAUSANTES_BASE> GET(int periodoProceso, string tipoReconocido)
        {
            return
                _repositoryVAP_CAUSANTES_BASE.GET(periodoProceso, tipoReconocido);
        }

        public List<Model.VAP_CAUSANTES_BASE> GET(int periodoProceso)
        {
            return
                _repositoryVAP_CAUSANTES_BASE.GET(periodoProceso);
        }

        public List<Model.VAP_CAUSANTES_BASE> GET_EXTINGUIDOS(int periodoProceso)
        {
            var paramFecReconocimiento = _parametroList.FirstOrDefault(o => o.PARAMETRO_ID == (int)Common.Config.IPS_Config.ParametroID.FECHA_RECONOCIMIENTO);

            var extinguidoList = _repositoryVAP_CAUSANTES_BASE.GET(DateTime.Parse(paramFecReconocimiento.PARAMETRO_VALOR), paramFecReconocimiento.PARAMETRO_CONSTRAIN, periodoProceso, "E");

            return
                extinguidoList;
        }

        public List<Model.VAP_CAUSANTES_BASE> GET_VIGENTES(int periodoProceso)
        {
            var vigenteList = _repositoryVAP_CAUSANTES_BASE.GET(periodoProceso, "V");

            return
                vigenteList;
        }

    }
}
