﻿using IPS.Beneficios.ValiacionPagos.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using Common = IPS.Beneficios.Common;
using Model = IPS.Beneficios.ValiacionPagos.Model;

namespace IPS.Beneficios.ValiacionPagos.Service.Dummies
{
    public class VAP_SOLICITUDES_BASE : IPS_Dummie
    {
        BLL_BULKDATA _serviceVAP_BULKDATA;

        public VAP_SOLICITUDES_BASE()
        {
            _serviceVAP_BULKDATA = new BLL_BULKDATA();
        }

        public Common.Config.IPS_Config.ProcesoEstado ProccessFile(Model.VAP_PERIODOS VAP_PERIODO)
        {
            #region //Variables

            var criteriaToSearch = string.Format("CRI{0}", VAP_PERIODO.PERIODO_DISPLAY);//"SAFEM_ASFAM_13-01-2016"; to test only
            var _downloadFolder = string.Format("{0}", Common.Config.IPS_Config.archivosCargaFolder);
            Common.Config.IPS_Config.ProcesoEstado _procesoEstado = default(Common.Config.IPS_Config.ProcesoEstado);
            #endregion


            Model.VAP_FILE VAP_FTPFILE = new Model.VAP_FILE()
            {
                ARCHIVORUTA = Common.Config.IPS_Config._ftpPath
                 ,
                PERIODO = VAP_PERIODO.PERIODO_DISPLAY
                ,
                ARCHIVONOMBRE = criteriaToSearch
                ,
                EXTENSION = ".TXT"
            };

            var _fileDictionary = Common.Helper.FtpComponent.GetFilesInfo(VAP_FTPFILE);

            var _successCount = _fileDictionary.Where(o => o.Value == true).Count();//archivos sin errores

            if (_successCount == 0) _successCount = -1;

            foreach (var item in _fileDictionary)
            {
                if (item.Value == true)//archivo sin errores
                {
                    var _wasDownloaded = Common.Helper.FtpComponent.DownloadFile(item.Key.FullName, _downloadFolder);

                    if (_wasDownloaded)
                    {
                        var _tableName = typeof(Model.VAP_SOLICITUDES_BASE).Name;

                        var _bulkedData = _serviceVAP_BULKDATA.BulkData(new Model.SQL_LOADER()
                        {
                            TABLE_NAME = _tableName
                            ,
                            FILEPATH = _downloadFolder + "\\" + item.Key.Name
                            ,
                            CLEANTABLE = true
                            ,
                            CLEANTABLE_QUERY = string.Format("DELETE VALIDACION_PAGOS.{0} WHERE TRIM(ARCHIVO_IDCARGA) = '{1}'", _tableName, item.Key.Name)
                            ,
                            CTLPATH = string.Format(@"{0}LOADERS\CTL\{1}.ctl", Common.Config.IPS_Config.installDirApp, _tableName)
                            ,
                            LOGPATH = string.Format(@"{0}LOADERS\LOG\{1}.log", Common.Config.IPS_Config.installDirApp, item.Key.Name)
                            ,
                            BADPATH = string.Format(@"{0}LOADERS\LOG\{1}.bad", Common.Config.IPS_Config.installDirApp, item.Key.Name)
                            ,
                            DISCARDPATH = string.Format(@"{0}LOADERS\LOG\{1}.dsc", Common.Config.IPS_Config.installDirApp, item.Key.Name)
                            ,
                            DICTIONARY_SETCONTROLFILEVALUES = new Dictionary<string, string>() { {"PERIODO_PROCESO", string.Format("{0}  constant {1}", "PERIODO_PROCESO", VAP_FTPFILE.PERIODO)}
                                                                                                ,{"ARCHIVO_IDCARGA", string.Format("{0}  constant \"{1}\"", "ARCHIVO_IDCARGA", item.Key.Name.ToUpper()) }}
                        });

                        if (_bulkedData)
                            _successCount--;
                    }
                }
            }


            // Shoulb be 0 if all bulked files run successfully
            if (_successCount == 0)
                _procesoEstado = Common.Config.IPS_Config.ProcesoEstado.TERMINADO_SUCCESS;
            else if (_fileDictionary.Count > 0 && _successCount != 0)
                _procesoEstado = Common.Config.IPS_Config.ProcesoEstado.TERMINADO_ERROR;
            else if (_fileDictionary.Count == 0 && _successCount < 0)
                _procesoEstado = Common.Config.IPS_Config.ProcesoEstado.ENESPERA_ARCHIVOPERIODO;
            else
                _procesoEstado = Common.Config.IPS_Config.ProcesoEstado.TERMINADO_ERROR;

            return
                _procesoEstado;

        }


        //public List<Model.VAP_SOLICITUDES_BASE> GET(string _archivoIDcargaLike)
        //{
        //    var solicitudesList = _repositoryVAP_SOLICITUDES_BASE.GET(_archivoIDcargaLike);          

        //    return
        //        solicitudesList;
        //}

        public List<Model.VAP_SOLICITUDES_BASE> GET(int periodoProceso)
        {
            var solicitudesList = _repositoryVAP_SOLICITUDES_BASE.GET(periodoProceso);

            return
                solicitudesList;
        }
    }
}
