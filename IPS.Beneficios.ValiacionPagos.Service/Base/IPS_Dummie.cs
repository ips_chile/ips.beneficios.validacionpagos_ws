﻿using System;
using System.IO;
using System.Globalization;
using System.Threading;
using Common = IPS.Beneficios.Common;
using Repository = IPS.Beneficios.ValiacionPagos.DataAccess;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using IPS.Beneficios.ValiacionPagos.Service.Dummies;

namespace IPS.Beneficios.ValiacionPagos.Service.Base
{
    public abstract class IPS_Dummie
    {
        protected Model.VAP_PERIODOS _VAP_PERIODO;

        protected BLL_BULKDATA _serviceVAP_BULKDATA;
        protected BLL_CAUSANTE_TRAMOS_HISTORICOS _serviceBLL_CAUSANTE_TRAMOS_HISTORICOS;
        protected Repository.Repository.VAP_VERIFPAGO_CAUSANTES _repositoryVAP_VERIFPAGO_CAUSANTES;
        protected Repository.Repository.BLL_CAUSANTE_TRAMOS_HISTORICOS _repositoryBLL_CAUSANTE_TRAMOS_HISTORICOS;
        protected Repository.Repository.BLL_PLANILLA_EMPLEADORES _repositoryBLL_PLANILLA_EMPLEADORES;
        protected Repository.Repository.BLL_PAGOS _repositoryBLL_PAGOS;
        protected Repository.Repository.BLL_CALCULO_INGRESOPROMEDIO _repositoryBLL_CALCULO_INGRESOPROMEDIO;
        protected Repository.Repository.BLL_INFPROCESO_PREVIRED _repositoryBLL_INFPROCESO_PREVIRED;
        protected Repository.Repository.BLL_DISTRIBUCION_PAGOS _repositoryBLL_DISTRIBUCION_PAGOS;
        protected Repository.Repository.BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO _repositoryBLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO;

        protected Repository.Repository.VAP_PROCESOS _repositoryVAP_PROCESOS;
        protected Repository.Repository.VAP_PROCESOS_TASK _repositoryVAP_PROCESOS_TASK;
        protected Repository.Repository.VAP_PERIODOS _repositoryVAP_PERIODOS;
        protected Repository.Repository.VAP_NOTIFICACION _repositoryVAP_NOTIFICACION;
        protected Repository.Repository.VAP_INGRESO_PROMEDIO _repositoryVAP_INGRESO_PROMEDIO;
        protected Repository.Repository.VAP_CAUSANTES_BASE _repositoryVAP_CAUSANTES_BASE;
        protected Repository.Repository.VAP_CAUSANTES_MES _repositoryVAP_CAUSANTES_MES;
        protected Repository.Repository.VAP_SOLICITUDES_BASE _repositoryVAP_SOLICITUDES_BASE;
        protected Repository.Repository.VAP_SOLICITUDES_PROCESADOS _repositoryVAP_SOLICITUDES_PROCESADOS;
        protected Repository.Repository.VAP_TRAMOS _repositoryVAP_TRAMOS;

        public static List<Model.VAP_PARAMETROS> _parametroList;
        public static List<Model.VAP_NOTIFICACION> _notificacionesList;
        public DateTime _fechaProceso;

        public IPS_Dummie()
        {
            _serviceVAP_BULKDATA = new BLL_BULKDATA();
            _serviceBLL_CAUSANTE_TRAMOS_HISTORICOS = new BLL_CAUSANTE_TRAMOS_HISTORICOS();
            _repositoryBLL_PAGOS = new Repository.Repository.BLL_PAGOS();
            _repositoryBLL_DISTRIBUCION_PAGOS = new Repository.Repository.BLL_DISTRIBUCION_PAGOS();
            _repositoryBLL_CAUSANTE_TRAMOS_HISTORICOS = new Repository.Repository.BLL_CAUSANTE_TRAMOS_HISTORICOS();
            _repositoryBLL_PLANILLA_EMPLEADORES = new Repository.Repository.BLL_PLANILLA_EMPLEADORES();
            _repositoryBLL_INFPROCESO_PREVIRED = new Repository.Repository.BLL_INFPROCESO_PREVIRED();
            _repositoryBLL_CALCULO_INGRESOPROMEDIO = new Repository.Repository.BLL_CALCULO_INGRESOPROMEDIO();
            _repositoryBLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO = new Repository.Repository.BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO();

            _repositoryVAP_PROCESOS = new Repository.Repository.VAP_PROCESOS();
            _repositoryVAP_PROCESOS_TASK = new Repository.Repository.VAP_PROCESOS_TASK();
            _repositoryVAP_PERIODOS = new Repository.Repository.VAP_PERIODOS();
            _repositoryVAP_VERIFPAGO_CAUSANTES = new Repository.Repository.VAP_VERIFPAGO_CAUSANTES();
            _repositoryVAP_INGRESO_PROMEDIO = new Repository.Repository.VAP_INGRESO_PROMEDIO();
            _repositoryVAP_CAUSANTES_BASE = new Repository.Repository.VAP_CAUSANTES_BASE();
            _repositoryVAP_SOLICITUDES_BASE = new Repository.Repository.VAP_SOLICITUDES_BASE();
            _repositoryVAP_SOLICITUDES_PROCESADOS = new Repository.Repository.VAP_SOLICITUDES_PROCESADOS();
            _repositoryVAP_CAUSANTES_MES = new Repository.Repository.VAP_CAUSANTES_MES();
            _repositoryVAP_TRAMOS = new Repository.Repository.VAP_TRAMOS();
            _parametroList = new Service.Dummies.VAP_PARAMETROS().GETALL();
            _notificacionesList = new Service.Dummies.VAP_NOTIFICACION().GETALL();
            _fechaProceso = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd"));// Fecha de proceso
        }

        public IPS_Dummie(Model.VAP_PERIODOS VAP_PERIODO)
        {
            _VAP_PERIODO = VAP_PERIODO;
            _serviceVAP_BULKDATA = new BLL_BULKDATA();
            _serviceBLL_CAUSANTE_TRAMOS_HISTORICOS = new BLL_CAUSANTE_TRAMOS_HISTORICOS();
            _repositoryBLL_DISTRIBUCION_PAGOS = new Repository.Repository.BLL_DISTRIBUCION_PAGOS();
            _repositoryBLL_CAUSANTE_TRAMOS_HISTORICOS = new Repository.Repository.BLL_CAUSANTE_TRAMOS_HISTORICOS();
            _repositoryBLL_PLANILLA_EMPLEADORES = new Repository.Repository.BLL_PLANILLA_EMPLEADORES();
            _repositoryBLL_INFPROCESO_PREVIRED = new Repository.Repository.BLL_INFPROCESO_PREVIRED();
            _repositoryBLL_CALCULO_INGRESOPROMEDIO = new Repository.Repository.BLL_CALCULO_INGRESOPROMEDIO();
            _repositoryBLL_PAGOS = new Repository.Repository.BLL_PAGOS();
            _repositoryBLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO = new Repository.Repository.BLL_RETROACTIVO_ARCHIVOPREVIRED_CONSOLIDADO();

            _repositoryVAP_PROCESOS = new Repository.Repository.VAP_PROCESOS();
            _repositoryVAP_PROCESOS_TASK = new Repository.Repository.VAP_PROCESOS_TASK();
            _repositoryVAP_PERIODOS = new Repository.Repository.VAP_PERIODOS();
            _repositoryVAP_SOLICITUDES_PROCESADOS = new Repository.Repository.VAP_SOLICITUDES_PROCESADOS();
            _repositoryVAP_VERIFPAGO_CAUSANTES = new Repository.Repository.VAP_VERIFPAGO_CAUSANTES();
            _repositoryVAP_INGRESO_PROMEDIO = new Repository.Repository.VAP_INGRESO_PROMEDIO();
            _repositoryVAP_SOLICITUDES_BASE = new Repository.Repository.VAP_SOLICITUDES_BASE();
            _repositoryVAP_CAUSANTES_MES = new Repository.Repository.VAP_CAUSANTES_MES();
            _repositoryVAP_TRAMOS = new Repository.Repository.VAP_TRAMOS();
            _parametroList = new Service.Dummies.VAP_PARAMETROS().GETALL();
            _notificacionesList = new Service.Dummies.VAP_NOTIFICACION().GETALL();
            _fechaProceso = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd"));// Fecha de proceso
        }

        public string GetTableName(string className)
        {
            var type = Type.GetType("IPS.Beneficios.ValiacionPagos.Model." + className + ", IPS.Beneficios.ValiacionPagos.Model");
            dynamic myObject = Activator.CreateInstance(type);

            return
                myObject.GetTableName();
        }

        public string GetFolder(string originSource, string folder, Model.VAP_PERIODOS periodo)
        {

            var folderPath = string.Format(@"{0}\{1}\{2}", folder, originSource, periodo.PERIODO_DISPLAY);
            var folderExist = Common.Helper.FilesHelper.checkAndCreateDir(folderPath);

            if (folderExist == false)
                folderPath = string.Format(@"{0}\{1}", folder, originSource);

            return
                folderPath;
        }

        public string GetFilePath(string folderPath, string originSource, Model.VAP_PERIODOS periodo)
        {

            var filePath = string.Format("{0}\\{1}_{2}", folderPath, originSource, periodo.PERIODO_DISPLAY);

            return
                filePath;
        }

        public DateTime GetFechaDesdeSolicitud(Model.VAP_SOLICITUDES_BASE solicitud)
        {
            var fec60MonthsAgoToUse = solicitud.FECHA_DECLARACION_CARGAS.AddMonths(-60);//EN DURO
            var fecDesdeSolicitud = solicitud.FECHA_INICIO_COMPENSACION;

            var fechaDesdeToUse = fecDesdeSolicitud >= fec60MonthsAgoToUse
                                    ? fecDesdeSolicitud
                                    : fec60MonthsAgoToUse;

            return
                fechaDesdeToUse;
        }


        #region //Distribucion pagos & Solicitudes
        public List<Model.VAP_VERIFPAGO_CAUSANTES> GetHistoricoCausante(Int32 RUT_EMPLEADOR, Model.VAP_INGRESO_PROMEDIO _CAUSANTE, long _valorPlanillaPeriodo)
        {
            var _verifPagoCausantesList = _repositoryVAP_VERIFPAGO_CAUSANTES.GET(RUT_EMPLEADOR, _CAUSANTE, _valorPlanillaPeriodo)
                                                                            .GroupBy(o => new { o.RUT_CAUSANTE, o.CAUSANTE_PERIODO,o.RETROACTIVO_ACUMULADO, o.VALOR_ARCHIVO12, o.VALORAF_PAGADO, o.VALORAF_PENDIENTE })
                                                                            .Select(o => new Model.VAP_VERIFPAGO_CAUSANTES()
                                                                            {
                                                                                RUT_CAUSANTE = o.Key.RUT_CAUSANTE
                                                                                ,
                                                                                CAUSANTE_PERIODO = o.Key.CAUSANTE_PERIODO
                                                                                ,
                                                                                RETROACTIVO_ACUMULADO = o.Key.RETROACTIVO_ACUMULADO
                                                                                ,
                                                                                VALOR_ARCHIVO12 = o.Key.VALOR_ARCHIVO12//Valor Planilla
                                                                                ,
                                                                                VALORAF_PAGADO = o.Key.VALORAF_PAGADO
                                                                                ,
                                                                                VALORAF_PENDIENTE = o.Key.VALORAF_PENDIENTE
                                                                            }).ToList();

            return
                _verifPagoCausantesList;
        }

        public List<Model.VAP_VERIFPAGO_CAUSANTES> GetHistoricoBeneficiario(Int32 RUT_BENEFICIARIO, int _PERIODO)
        {
            var _verifPagoCausantesList = _repositoryVAP_VERIFPAGO_CAUSANTES.GET(RUT_BENEFICIARIO, _PERIODO)
                                                                            .GroupBy(o => new { o.RUT_BENEFICIARIO, o.CAUSANTE_PERIODO, o.VALOR_ARCHIVO12})
                                                                            .Select(o => new Model.VAP_VERIFPAGO_CAUSANTES()
                                                                            {
                                                                                RUT_CAUSANTE = o.Key.RUT_BENEFICIARIO
                                                                                ,
                                                                                CAUSANTE_PERIODO = o.Key.CAUSANTE_PERIODO
                                                                                ,
                                                                                VALOR_ARCHIVO12 = o.Key.VALOR_ARCHIVO12
                                                                            }).ToList();

            return
                _verifPagoCausantesList;
        }

        //public long GetValorPlanillaHistoricoCausante(Int32 _RUT_CAUSANTE, int _CAUSANTE_PERIODO)
        //{
        //    var _verifPagoCausantesList = _repositoryVAP_VERIFPAGO_CAUSANTES.GET(_RUT_CAUSANTE, _CAUSANTE_PERIODO)
        //                                                                    .GroupBy(o => new { o.RUT_CAUSANTE, o.CAUSANTE_PERIODO, o.VALOR_ARCHIVO12, o.VALORAF_PAGADO, o.VALORAF_PENDIENTE })
        //                                                                    .Select(o => new Model.VAP_VERIFPAGO_CAUSANTES()
        //                                                                    {
        //                                                                        RUT_CAUSANTE = o.Key.RUT_CAUSANTE
        //                                                                        ,
        //                                                                        CAUSANTE_PERIODO = o.Key.CAUSANTE_PERIODO
        //                                                                        ,
        //                                                                        VALOR_ARCHIVO12 = o.Key.VALOR_ARCHIVO12
        //                                                                        ,
        //                                                                        VALORAF_PAGADO = o.Key.VALORAF_PAGADO
        //                                                                        ,
        //                                                                        VALORAF_PENDIENTE = o.Key.VALORAF_PENDIENTE
        //                                                                    }).ToList();

        //    long valorPlanilla = _verifPagoCausantesList.Sum(o => o.VALOR_ARCHIVO12);


        //    return
        //         valorPlanilla;
        //}

        public List<Model.VAP_VERIFPAGO_CAUSANTES> GetCausantePeriodo(Int32 _RUT_CAUSANTE, int _CAUSANTE_PERIODO)
        {
            var _verifPagoCausantesList = _repositoryVAP_VERIFPAGO_CAUSANTES.GET(_RUT_CAUSANTE, _CAUSANTE_PERIODO);

            return
                _verifPagoCausantesList
                .GroupBy(o => new { o.FOLIO_SOLICITUD, o.RUT_CAUSANTE, o.CAUSANTE_PERIODO, o.VALORAF_PAGADO, o.VALORAF_PENDIENTE })
                .Select(o => new Model.VAP_VERIFPAGO_CAUSANTES()
                    {
                        FOLIO_SOLICITUD = o.Key.FOLIO_SOLICITUD
                        ,
                        RUT_CAUSANTE = o.Key.RUT_CAUSANTE
                        ,
                        CAUSANTE_PERIODO = o.Key.CAUSANTE_PERIODO
                        ,
                        VALORAF_PAGADO = o.Key.VALORAF_PAGADO
                        ,
                        VALORAF_PENDIENTE = o.Key.VALORAF_PENDIENTE
                    })
                .ToList();
        }

        #region //Ordena lista causantes por prioridad
        public List<Model.VAP_VERIFPAGO_CAUSANTES> Get_CausantesListByPrioridad(List<Model.VAP_VERIFPAGO_CAUSANTES> _causantesSIAGFByPeriodoList
                                                                                , int periodo
                                                                                , List<int> _causantesMayorPermanenciaList = default(List<int>)
                                                                                , List<int> _causantesFechaInsert = default(List<int>)
                                                                                , List<int> _causantesRutMenor = default(List<int>))
        {
            #region //variables
            List<Model.VAP_VERIFPAGO_CAUSANTES> causantesToReturnList = new List<Model.VAP_VERIFPAGO_CAUSANTES>();
            List<int> filteredList = new List<int>();

            #endregion

            if (_causantesMayorPermanenciaList != default(List<int>))
                causantesToReturnList = FillPrioridadList(_causantesMayorPermanenciaList, _causantesSIAGFByPeriodoList, periodo);

            if (_causantesFechaInsert != default(List<int>))
            {
                filteredList = _causantesFechaInsert.Where(x => causantesToReturnList.All(y => y.RUT_CAUSANTE != x)).ToList();

                causantesToReturnList.AddRange(FillPrioridadList(filteredList, _causantesSIAGFByPeriodoList, periodo));
            }

            if (_causantesRutMenor != default(List<int>))
            {
                filteredList = _causantesRutMenor.Where(x => causantesToReturnList.All(y => y.RUT_CAUSANTE != x)).ToList();

                causantesToReturnList.AddRange(FillPrioridadList(filteredList, _causantesSIAGFByPeriodoList, periodo));
            }

            return
                causantesToReturnList;
        }

        public List<Model.VAP_INGRESO_PROMEDIO> Get_CausantesListByPrioridad(List<Model.VAP_INGRESO_PROMEDIO> _causantesSIAGFByPeriodoList
                                                                                , int periodo
                                                                                , List<int> _causantesMayorPermanenciaList = default(List<int>)
                                                                                , List<int> _causantesFechaInsert = default(List<int>)
                                                                                , List<int> _causantesRutMenor = default(List<int>))
        {
            #region //variables
            List<Model.VAP_INGRESO_PROMEDIO> causantesToReturnList = new List<Model.VAP_INGRESO_PROMEDIO>();
            List<int> filteredList = new List<int>();

            #endregion

            if (_causantesMayorPermanenciaList != default(List<int>))
                causantesToReturnList = FillPrioridadList(_causantesMayorPermanenciaList, _causantesSIAGFByPeriodoList, periodo);

            if (_causantesFechaInsert != default(List<int>))
            {
                filteredList = _causantesFechaInsert.Where(x => causantesToReturnList.All(y => y.RUT_CAUSANTE != x)).ToList();

                causantesToReturnList.AddRange(FillPrioridadList(filteredList, _causantesSIAGFByPeriodoList, periodo));
            }

            if (_causantesRutMenor != default(List<int>))
            {
                filteredList = _causantesRutMenor.Where(x => causantesToReturnList.All(y => y.RUT_CAUSANTE != x)).ToList();

                causantesToReturnList.AddRange(FillPrioridadList(filteredList, _causantesSIAGFByPeriodoList, periodo));
            }

            return
                causantesToReturnList;
        }

        public List<Model.VAP_VERIFPAGO_CAUSANTES> FillPrioridadList(List<int> filteredList, List<Model.VAP_VERIFPAGO_CAUSANTES> causantesSIAGFByPeriodoList, int periodo)
        {
            #region //variables
            List<Model.VAP_VERIFPAGO_CAUSANTES> causantesToReturnList = new List<Model.VAP_VERIFPAGO_CAUSANTES>();

            #endregion

            foreach (var causanteRUT in filteredList)
            {
                var causanteInPeriodo = causantesSIAGFByPeriodoList.Where(o => o.RUT_CAUSANTE == causanteRUT && o.CAUSANTE_PERIODO == periodo).SingleOrDefault();

                if (causanteInPeriodo != default(Model.VAP_VERIFPAGO_CAUSANTES))
                {
                    var isCausanteAlreadyPresentInList = causantesToReturnList.Exists(o => o.RUT_CAUSANTE == causanteInPeriodo.RUT_CAUSANTE);

                    if (isCausanteAlreadyPresentInList == false)
                        causantesToReturnList.Add(causanteInPeriodo);
                    else
                        break;
                }
            }

            return
                causantesToReturnList;
        }
        public List<Model.VAP_INGRESO_PROMEDIO> FillPrioridadList(List<int> filteredList, List<Model.VAP_INGRESO_PROMEDIO> causantesSIAGFByPeriodoList, int periodo)
        {
            #region //variables
            List<Model.VAP_INGRESO_PROMEDIO> causantesToReturnList = new List<Model.VAP_INGRESO_PROMEDIO>();
            #endregion

            foreach (var causanteRUT in filteredList)
            {
                var causanteInPeriodo = causantesSIAGFByPeriodoList.Where(o => o.RUT_CAUSANTE == causanteRUT && o.CAUSANTE_PERIODO == periodo).SingleOrDefault();

                if (causanteInPeriodo != default(Model.VAP_INGRESO_PROMEDIO))
                {
                    var isCausanteAlreadyPresentInList = causantesToReturnList.Exists(o => o.RUT_CAUSANTE == causanteInPeriodo.RUT_CAUSANTE);

                    if (isCausanteAlreadyPresentInList == false)
                        causantesToReturnList.Add(causanteInPeriodo);
                    else
                        break;
                }
            }

            return
                causantesToReturnList;
        }

        #endregion

        #region //Reglas de Asignacion por prioridad
        public List<Int32> ApplyRule_CausantesMayorPermanencia(List<Model.VAP_VERIFPAGO_CAUSANTES> _causantesSIAGFBySolicitudList, Model.VAP_SOLICITUDES_BASE solicitud, DateTime fechaDesdeSolicitudToUse)
        {
            #region //Variables
            //Dictionary<Rut_causante, cantidad>
            Dictionary<Int32, int> causantesRepetidosolicitudDictionary = new Dictionary<Int32, int>();
            solicitud.FECHA_INICIO_COMPENSACION = fechaDesdeSolicitudToUse;

            //Causantes SIAGF agrupados
            var causantesList = (from causante in _causantesSIAGFBySolicitudList
                                 group causante by causante.RUT_CAUSANTE into g
                                 select g.Last()).ToList();
            #endregion

            foreach (var causanteAgrupado in causantesList)
            {
                int diasEnSolicitud = default(int);

                foreach (var causantePeriodo in _causantesSIAGFBySolicitudList.Where(o => o.RUT_CAUSANTE == causanteAgrupado.RUT_CAUSANTE).ToList())
                    diasEnSolicitud += GetDiasEnSolicitud(causantePeriodo, solicitud);

                causantesRepetidosolicitudDictionary.Add(causanteAgrupado.RUT_CAUSANTE, diasEnSolicitud);
            }

            return
                causantesRepetidosolicitudDictionary.OrderByDescending(g => g.Value).Select(o => o.Key).ToList();
        }

        public List<Int32> ApplyRule_CausantesFechaIngreso(List<Model.VAP_VERIFPAGO_CAUSANTES> causantesSIAGFBySolicitudList)
        {
            #region //Variables
            //Dictionary<Rut_causante, cantidad>
            Dictionary<Int32, DateTime> causantesFechaInsertDictionary = new Dictionary<Int32, DateTime>();
            #endregion

            //Causantes SIAGF
            var causantesList = (from causante in causantesSIAGFBySolicitudList
                                 group causante by causante.RUT_CAUSANTE into g
                                 select g.Last()).ToList();

            foreach (var causante in causantesList)
            {
                var fechaInsert = causantesSIAGFBySolicitudList.Where(o => o.RUT_CAUSANTE == causante.RUT_CAUSANTE).Select(o => o.FECHA_INSERT).OrderBy(o => o).FirstOrDefault();
                causantesFechaInsertDictionary.Add(causante.RUT_CAUSANTE, fechaInsert);
            }

            return
                causantesFechaInsertDictionary.OrderBy(g => g.Value).Select(o => o.Key).ToList();
        }
        public List<Int32> ApplyRule_CausantesFechaIngreso(List<Model.VAP_INGRESO_PROMEDIO> causantesSIAGFBySolicitudList)
        {
            #region //Variables
            //Dictionary<Rut_causante, cantidad>
            Dictionary<Int32, DateTime> causantesFechaInsertDictionary = new Dictionary<Int32, DateTime>();
            #endregion

            //Causantes SIAGF
            var causantesList = (from causante in causantesSIAGFBySolicitudList
                                 group causante by causante.RUT_CAUSANTE into g
                                 select g.Last()).ToList();

            foreach (var causante in causantesList)
            {
                var fechaInsert = causantesSIAGFBySolicitudList.Where(o => o.RUT_CAUSANTE == causante.RUT_CAUSANTE).Select(o => o.FECHA_INSERT).OrderBy(o => o).FirstOrDefault();
                causantesFechaInsertDictionary.Add(causante.RUT_CAUSANTE, fechaInsert);
            }

            return
                causantesFechaInsertDictionary.OrderBy(g => g.Value).Select(o => o.Key).ToList();
        }

        public List<Int32> ApplyRule_CausantesRutMenor(List<Model.VAP_VERIFPAGO_CAUSANTES> causantesSIAGFBySolicitudList)
        {
            #region //Variables
            //Dictionary<Rut_causante, Rut_causante>
            List<Int32> causantesRutMenortDictionary = new List<Int32>();
            #endregion

            //Causantes SIAGF
            var causantesList = (from causante in causantesSIAGFBySolicitudList
                                 group causante by causante.RUT_CAUSANTE into g
                                 select g.Last()).ToList();

            foreach (var causante in causantesList)
                causantesRutMenortDictionary.Add(causante.RUT_CAUSANTE);

            return
                causantesRutMenortDictionary.OrderBy(g => g).ToList();
        }
        public List<Int32> ApplyRule_CausantesRutMenor(List<Model.VAP_INGRESO_PROMEDIO> causantesSIAGFBySolicitudList)
        {
            #region //Variables
            //Dictionary<Rut_causante, Rut_causante>
            List<Int32> causantesRutMenortDictionary = new List<Int32>();
            #endregion

            //Causantes SIAGF
            var causantesList = (from causante in causantesSIAGFBySolicitudList
                                 group causante by causante.RUT_CAUSANTE into g
                                 select g.Last()).ToList();

            foreach (var causante in causantesList)
                causantesRutMenortDictionary.Add(causante.RUT_CAUSANTE);

            return
                causantesRutMenortDictionary.OrderBy(g => g).ToList();
        }


        public Common.Config.IPS_Config.RulePriorizationToUse GetRulePriorizationToUse(List<int> _causantesMayorPermanencia, List<int> _causantesFechaInsert, List<int> _causantesRutMenor)
        {
            var hasDuplicated_MayorPermanencia = _causantesMayorPermanencia.GroupBy(n => n).Any(c => c.Count() > 1);

            var hasDuplicated_FechaInsert = _causantesFechaInsert.GroupBy(n => n).Any(c => c.Count() > 1);

            return
                hasDuplicated_MayorPermanencia == false
                ? Common.Config.IPS_Config.RulePriorizationToUse.MAYOR_PERMANENCIA
                : (hasDuplicated_FechaInsert == false ? Common.Config.IPS_Config.RulePriorizationToUse.FECHA_INSERT : Common.Config.IPS_Config.RulePriorizationToUse.MENOR_RUT);
        }

        #endregion

        #region //Dias: SIAGF, Solicitados y Trabajados

        public int GetDiasEnSolicitud(Model.VAP_VERIFPAGO_CAUSANTES causante, Model.VAP_SOLICITUDES_BASE solicitud)
        {
            #region //Variables
            int diasToReturn = default(int);
            var solicitudIniPeriodo = int.Parse(solicitud.FECHA_INICIO_COMPENSACION.ToString("yyyyMM"));
            var solicitudFinPeriodo = int.Parse(solicitud.FECHA_FIN_COMPENSACION.ToString("yyyyMM"));

            var fecRecCausante = int.Parse(causante.CAUSANTE_FEC_REC.ToString("yyyyMM"));
            var fecExtCausante = int.Parse(causante.CAUSANTE_FEC_EXT_CALC.ToString("yyyyMM"));
            #endregion

            if (solicitudIniPeriodo == fecRecCausante)
            {
                if (solicitud.FECHA_INICIO_COMPENSACION > causante.CAUSANTE_FEC_REC)
                    diasToReturn = 30 - (solicitud.FECHA_INICIO_COMPENSACION.Day - 1);
                else
                    diasToReturn = 30 - (causante.CAUSANTE_FEC_REC.Day - 1);
            }
            else if (solicitudFinPeriodo == fecExtCausante)
            {
                if (solicitud.FECHA_FIN_COMPENSACION < causante.CAUSANTE_FEC_EXT_CALC)
                    diasToReturn = solicitud.FECHA_FIN_COMPENSACION.Day <= 30 ? solicitud.FECHA_FIN_COMPENSACION.Day : 30;
                else
                    diasToReturn = causante.CAUSANTE_FEC_EXT_CALC.Day <= 30 ? causante.CAUSANTE_FEC_EXT_CALC.Day : 30;
            }
            else
                diasToReturn = 30;

            return
                diasToReturn;
        }

        public int GetDiasSIAGF(Model.VAP_CAUSANTES_MES causante)
        {
            #region //Variables
            int diasSIAGF = default(int);
            var fecRec = int.Parse(causante.CAUSANTE_FEC_REC.ToString("yyyyMM"));
            var fecExt = int.Parse(causante.CAUSANTE_FEC_EXT_CALC.ToString("yyyyMM"));
            #endregion

            if (fecRec == causante.CAUSANTE_PERIODO)
                diasSIAGF = 30 - (causante.CAUSANTE_FEC_REC.Day - 1);

            else if (fecExt == causante.CAUSANTE_PERIODO)
                diasSIAGF = causante.CAUSANTE_FEC_EXT_CALC.Day <= 30 ? causante.CAUSANTE_FEC_EXT_CALC.Day : 30;
            //30 - (causante.CAUSANTE_FEC_EXT_CALC.Day - 1);

            else
                diasSIAGF = 30;

            return
                diasSIAGF;
        }
        public int GetDiasSIAGF(Model.VAP_INGRESO_PROMEDIO causante)
        {
            #region //Variables
            int diasSIAGF = default(int);
            var fecRec = int.Parse(causante.CAUSANTE_FEC_REC.ToString("yyyyMM"));
            var fecExt = int.Parse(causante.CAUSANTE_FEC_EXT_CALC.ToString("yyyyMM"));
            #endregion

            if (fecRec == causante.CAUSANTE_PERIODO)
                diasSIAGF = 30 - (causante.CAUSANTE_FEC_REC.Day - 1);

            else if (fecExt == causante.CAUSANTE_PERIODO)
                diasSIAGF = causante.CAUSANTE_FEC_EXT_CALC.Day <= 30 ? causante.CAUSANTE_FEC_EXT_CALC.Day : 30;
            //30 - (causante.CAUSANTE_FEC_EXT_CALC.Day - 1);
            else
                diasSIAGF = 30;

            return
                diasSIAGF;
        }

        public int GetDiasSolicitados(Model.VAP_SOLICITUDES_BASE solicitud, Model.VAP_CAUSANTES_MES causante)
        {
            #region //Variables
            int diasSolicitados = default(int);
            var fecSolicitudDesde = int.Parse(solicitud.FECHA_INICIO_COMPENSACION.ToString("yyyyMM"));
            var fecSolicitudHasta = int.Parse(solicitud.FECHA_FIN_COMPENSACION.ToString("yyyyMM"));
            #endregion

            if (fecSolicitudDesde == causante.CAUSANTE_PERIODO)
                diasSolicitados = 30 - (solicitud.FECHA_INICIO_COMPENSACION.Day - 1);

            else if (fecSolicitudHasta == causante.CAUSANTE_PERIODO)
                diasSolicitados = solicitud.FECHA_FIN_COMPENSACION.Day <= 30 ? solicitud.FECHA_FIN_COMPENSACION.Day : 30;
            //30 - (solicitud.FECHA_FIN_COMPENSACION.Day - 1);

            else
                diasSolicitados = 30;

            return
                diasSolicitados;
        }
        public int GetDiasSolicitados(Model.VAP_SOLICITUDES_BASE solicitud, Model.VAP_INGRESO_PROMEDIO causante)
        {
            #region //Variables
            int diasSolicitados = default(int);
            var fecSolicitudDesde = int.Parse(solicitud.FECHA_INICIO_COMPENSACION.ToString("yyyyMM"));
            var fecSolicitudHasta = int.Parse(solicitud.FECHA_FIN_COMPENSACION.ToString("yyyyMM"));
            #endregion

            if (fecSolicitudDesde == causante.CAUSANTE_PERIODO)
                diasSolicitados = 30 - (solicitud.FECHA_INICIO_COMPENSACION.Day - 1);

            else if (fecSolicitudHasta == causante.CAUSANTE_PERIODO)
                diasSolicitados = solicitud.FECHA_FIN_COMPENSACION.Day <= 30 ? solicitud.FECHA_FIN_COMPENSACION.Day : 30;
            //30 - (solicitud.FECHA_FIN_COMPENSACION.Day - 1);

            else
                diasSolicitados = 30;

            return
                diasSolicitados;
        }

        public int GetDiasTrabajados(Model.VAP_CAUSANTES_MES causante)
        {
            #region //Variables
            var diasTrabajados = default(int);
            #endregion

            var _PERIODO_TRABAJADO_PREVIRED = _repositoryBLL_INFPROCESO_PREVIRED.GET(causante);

            if (_PERIODO_TRABAJADO_PREVIRED.DIAS_TRABAJADOS != default(int))
                diasTrabajados = _PERIODO_TRABAJADO_PREVIRED.DIAS_TRABAJADOS;

            else
            {
                var _PERIODO_TRABAJADO_PLANILLA = _repositoryBLL_PLANILLA_EMPLEADORES.GET(causante);

                diasTrabajados = _PERIODO_TRABAJADO_PLANILLA != default(Model.PERIODO_TRABAJADO) ? _PERIODO_TRABAJADO_PLANILLA.DIAS_TRABAJADOS : default(int);
            }

            return
                diasTrabajados;
        }
        public int GetDiasTrabajados(Model.VAP_INGRESO_PROMEDIO causante)
        {
            #region //Variables
            var diasTrabajados = default(int);
            #endregion

            var _PERIODO_TRABAJADO_PREVIRED = _repositoryBLL_INFPROCESO_PREVIRED.GET(causante);

            if (_PERIODO_TRABAJADO_PREVIRED.DIAS_TRABAJADOS != default(int))
                diasTrabajados = _PERIODO_TRABAJADO_PREVIRED.DIAS_TRABAJADOS;

            else
            {
                var _PERIODO_TRABAJADO_PLANILLA = _repositoryBLL_PLANILLA_EMPLEADORES.GET(causante);

                diasTrabajados = _PERIODO_TRABAJADO_PLANILLA != default(Model.PERIODO_TRABAJADO) ? _PERIODO_TRABAJADO_PLANILLA.DIAS_TRABAJADOS : default(int);
            }

            return
                diasTrabajados;
        }

        public string GetMenorDia(int DIAS_SIAGF, int DIAS_SOLICITADOS, int DIAS_TRABAJADOS)
        {
            string diasGlosa = string.Empty;
            Dictionary<string, int> diasDictionary = new Dictionary<string, int>()
            {
                {"D.SIAGF", DIAS_SIAGF}
                ,{"D.SOLICITADOS",DIAS_SOLICITADOS}
                ,{"D.TRABAJADOS", DIAS_TRABAJADOS}
            };


            var menorDia = diasDictionary.OrderBy(o => o.Value).FirstOrDefault();

            var isDuplicated = diasDictionary.Where(o => o.Value == menorDia.Value).Count() > 1;

            if (isDuplicated)
                foreach (var item in diasDictionary)
                    diasGlosa += menorDia.Value == item.Value ? string.Format("|{0}| ", item.Key) : string.Empty;
            else
                diasGlosa = menorDia.Key;

            return
                diasGlosa;
        }

        public int GetDiasAProcesar(Model.VAP_INGRESO_PROMEDIO ingPromedio, Model.VAP_SOLICITUDES_BASE solicitud)
        {
            var diasSolicitados = GetDiasSolicitados(solicitud, ingPromedio);

            var diasTrabajados = GetDiasTrabajados(ingPromedio);

            var diasSiagf = GetDiasSIAGF(ingPromedio);

            int[] diasArray = new int[] { diasSiagf, diasSolicitados, diasTrabajados };

            return
                diasArray.OrderBy(o => o).First();
        }

        public int GetDiasAProcesarPlanilla(Model.VAP_INGRESO_PROMEDIO ingPromedio)
        {
            var diasTrabajados = GetDiasTrabajados(ingPromedio);

            var diasSiagf = GetDiasSIAGF(ingPromedio);

            int[] diasArray = new int[] { diasSiagf, diasTrabajados };

            return
                diasArray.OrderBy(o => o).First();
        }
        #endregion

        #endregion

        #region //Ingreso promedio
        public Model.VAP_INGRESO_PROMEDIO GetIngresoPromedioToAdd(Model.VAP_CAUSANTES_MES causante, long valAsigIngPromedio, int _tramoToUse)
        {
            var diasSiagf = GetDiasSIAGF(causante);

            var valAsigIngPromedioCalulado = valAsigIngPromedio < 0 ? 0 : (long)((double.Parse(valAsigIngPromedio.ToString()) / 30) * diasSiagf);

            var causanteToReturn = new Model.VAP_INGRESO_PROMEDIO()
            {
                RUT_BENEFICIARIO = causante.RUT_BENEFICIARIO
                ,
                DV_BENEFICIARIO = causante.DV_BENEFICIARIO
                ,
                RUT_CAUSANTE = causante.RUT_CAUSANTE
                ,
                DV_CAUSANTE = causante.DV_CAUSANTE
                ,
                FECHA_INSERT = causante.FECHA_INSERT
                ,
                CAUSANTE_FEC_REC = causante.CAUSANTE_FEC_REC
                ,
                CAUSANTE_FEC_EXT_CALC = causante.CAUSANTE_FEC_EXT_CALC
                ,
                SISTEMA_FEC_PROCESO = _fechaProceso                
                ,
                PERIODO_PROCESO = causante.PERIODO_PROCESO
                ,
                CAUSANTE_PERIODO = causante.CAUSANTE_PERIODO
                ,
                CAUSANTE_INGPROMEDIO = valAsigIngPromedioCalulado
                ,
                CAUSANTE_TIENE_INGPROMEDIO = valAsigIngPromedioCalulado < 0 ? "N" : "S"
                ,
                TRAMO = _tramoToUse
            };

            return
                causanteToReturn;
        }

        #endregion


    }
}

//public List<Int32> ApplyRule_CausantesMayorPermanencia(List<Model.VAP_INGRESO_PROMEDIO> _causantesSIAGFBySolicitudList)
//{
//    //Dictionary<Rut_causante, cantidad>
//    Dictionary<Int32, int> causantesRepetidosolicitudDictionary = new Dictionary<Int32, int>();

//    //Causantes SIAGF
//    var causantesList = (from causante in _causantesSIAGFBySolicitudList
//                         group causante by causante.RUT_CAUSANTE into g
//                         select g.Last()).ToList();

//    foreach (var causante in causantesList)
//    {
//        var cantRepetido = _causantesSIAGFBySolicitudList.Where(o => o.RUT_CAUSANTE == causante.RUT_CAUSANTE).Count();
//        causantesRepetidosolicitudDictionary.Add(causante.RUT_CAUSANTE, cantRepetido);
//    }

//    return
//        causantesRepetidosolicitudDictionary.OrderByDescending(g => g.Value).Select(o => o.Key).ToList();
//}
